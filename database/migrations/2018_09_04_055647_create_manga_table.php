<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMangaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mangas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('user_id');
            $table->string('artist')->nullable();
            $table->string('category_id');
            $table->string('cover');
            $table->integer('release_year');
            $table->enum('release_status',['Ongoing','Completed']);
            $table->text('description')->nullable();
            $table->enum('status',['New','Published','Rejected']);
            $table->datetime('status_change');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mangas');
    }
}
