<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFollowArtistsCol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('follow_artists', function (Blueprint $table) {
            $table->enum('type',['User','Artist'])->default('User');           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('follow_artists', function (Blueprint $table) {
           $table->dropColumn(['type']);
        });
    }
}