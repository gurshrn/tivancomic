<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMangaViewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manga_views', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('manga_id');
            $table->integer('user_id')->nullable();
            $table->string("slug");
            $table->string("url");
            $table->string("session_id");
            $table->string("ip");
            $table->text("agent");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manga_views');
    }
}
