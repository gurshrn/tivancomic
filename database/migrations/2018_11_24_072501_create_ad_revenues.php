<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdRevenues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_revenues', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->integer('ad_clicks');
            $table->float('total_revenue');
            $table->float('admin_part');
            $table->float('user_part');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ad_revenues');
    }
}
