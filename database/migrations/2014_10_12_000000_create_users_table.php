<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->enum('userrole',['Artist','User','Admin'])->default('User');
            $table->string('password');
            $table->date('dob')->nullable();
            $table->enum('gender',['Male','Female'])->nullable();
            $table->text('bio')->nullable();
            $table->string('pic')->nullable();
            $table->enum('status',['New','Verified','Blocked'])->default('New');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
