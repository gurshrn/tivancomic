<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name' => 'manga_admin',
            'email' => 'neetu.rani@imarkinfotech.com',
            'password' => bcrypt('im@rk123#@'),
            'role' => 'Admin',
            'dob'=>date('1988-08-07'),
            'gender'=>'Female',
            'status'=>'Verified'            
        ]);
    }
}
