<?php

use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('menu_items')->truncate();
        \DB::table('menus')->truncate();
        
        \DB::table('menus')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
             ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Useful Links',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Header Menu',
            ),
        ));
        
        
    }
}