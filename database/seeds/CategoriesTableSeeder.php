<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('categories')->insert([
            ['name' => 'Action'],[
            'name' => 'Adult'],[
            'name' => 'Adventure'],[
            'name' => 'Comedy'],[
            'name' => 'Cooking'],[
            'name' => 'Doujinshi'],[
            'name' => 'Drama'],[
            'name' => 'Ecchi'],[
            'name' => 'Fantasy'],[
            'name' => 'Gender bender'],[
            'name' => 'Harem'],[
            'name' => 'Historical'],[
            'name' => 'Horror'],[
            'name' => 'Josei'],[
            'name' => 'Manhua'],[
            'name' => 'Manhwa'],[
            'name' => 'Martial arts'],[
            'name' => 'Mature'],[
            'name' => 'Mecha'],[
            'name' => 'Medical'],[
            'name' => 'Mystery'],[
            'name' => 'One shot'],[
            'name' => 'Psychological'],[
            'name' => 'Romance'],[
            'name' => 'School life'],[
            'name' => 'Sci fi'],[
            'name' => 'Seinen'],[
            'name' => 'Shoujo'],[
            'name' => 'Shoujo ai'],[
            'name' => 'Shounen ai'],[
            'name' => 'Slice of life'],[
            'name' => 'Smut'],[
            'name' => 'Sports'],[
            'name' => 'Supernatural'],[
            'name' => 'Tragedy'],[
            'name' => 'Webtoons'],[
            'name' => 'Yaoi'],[
            'name' => 'Yuri']
        ]);
    }
}
