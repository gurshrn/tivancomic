<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/comic-directory/{slug}', 'MangaController@detail')->name('cms_manga_detail');
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});
Route::get('/comic-directory', 'MangaController@index')->name('manga-directory');
Route::get('/comic-reading/{slug}', 'MangaController@reading')->name('manga-reading');
Route::get('/latest-chapters', 'HomeController@latestChapters')->name('latest-chapters');
Route::get('/comic/artist/{slug}', 'HomeController@artistDetail')->name('cms_artist-details');
Route::get('/artist-directory', 'HomeController@artists')->name('artist-directory');
Route::get('/artist/{slug}', 'HomeController@artistProfile')->name('artist-profile');
Route::get('/blog', 'HomeController@blogs')->name('blog');

Route::get('/blog/{slug}', 'HomeController@blogDetail')->name('blog-detail');

Route::post('/contact', 'HomeController@contact')->name('contact-post');

/**
 * Let's get some search going
 */
Route::get('/search', "SearchController@index")
    ->middleware(['web'])
    ->name('search');



Auth::routes(['verify' => true]);
Route::get('/login', 'Auth\LoginController@index')->name('login');
Route::get('/register', 'Auth\RegisterController@index')->name('register');


Route::middleware(['verified', 'auth'])->group(function() {

	//profile edit
	Route::get('/dashboard', 'UserController@index')->name('dashboard');
	Route::get('/edit-profile', 'UserController@editProfile')->name('edit-profile');
	Route::post('/update-profile', 'UserController@updateProfile')->name('update-profile');

	Route::get('/settings', 'UserController@settings')->name('settings');
    Route::post('change-password', ['uses' =>'UserController@passwordUpdate', 'as' => 'change-password']);

	// Upload Avatar
	Route::post('upload/avatar','UserController@upload_avatar')->name('upload-avatar');

	// artist Menu
	//upload manga
	Route::get('/upload-comic/{slug?}', 'MangaController@uploadManga')->name('upload-manga');
	Route::post('/upload-comic', 'MangaController@saveManga')->name('save-manga');
	Route::get('/delete-comic/{slug}', 'MangaController@deleteManga')->name('delete-manga');

	Route::get('/mass-upload', 'MangaController@massUpload')->name('mass-upload');
	Route::post('/mass-upload', 'MangaController@massUploadSave')->name('mass-upload-save');


	Route::get('/comic-volume/{action}', 'MangaController@mangaVolume')->name('manga-volume');
	Route::post('/comic-volume/{action}', 'MangaController@mangaVolumeUpdate')->name('manga-volume-post');
	Route::get('/comic-chapter/{action}', 'MangaController@mangaChapter')->name('manga-chapter');
	Route::post('/comic-chapter/{action}', 'MangaController@mangaChapterUpdate')->name('manga-chapter-post');
	Route::get('/comic-page', 'MangaController@mangaPage')->name('manga-page');
	Route::post('/comic-page', 'MangaController@mangaPageUpdate')->name('manga-page-post');
	Route::get('/manag-page-delete/{page_id}', 'MangaController@mangaPageDelete')->name('manag-page-delete');

	//manga Listing
	Route::get('/comic-listing', 'MangaController@artistMangaList')->name('artist-manga-listing');
	Route::get('/comic-requests', 'MangaController@artistMangaRequests')->name('artist-manga-requests');
	Route::get('/comic-request/{volume_id}', 'MangaController@artistMangaRequestDetail')->name('artist-manga-request-detail');
	
	//earnings
	Route::get('/earnings', 'UserController@earnings')->name('earnings');

	//dropdowns
	Route::get('/comic-volume-list/{manga_id}', 'MangaController@mangaVolumeList')->name('manga-volume-list');

	Route::get('/comic-chapter-list/{manga_id}/{manga_volume_id?}', 'MangaController@mangaChapterList')->name('manga-chapter-list');
	Route::get('/comic-chapter-detail/{manga_chapter_id}', 'MangaController@mangaChapterDetail')->name('manga-chapter-detail');

	Route::get('/comic-page-list/{manga_id}/{manga_chapter_id}', 'MangaController@mangaPageList')->name('manga-page-list');


	//User Menu
	Route::post('/request-comic', 'MangaController@requestManga')->name('request-manga');
	Route::post('/rate-comic', 'UserController@rateManga')->name('rate-manga');

	Route::post('/follow-comic', 'UserController@followManga')->name('follow-manga');
	Route::post('/unfollow-comic', 'UserController@unfollowManga')->name('unfollow-manga');
	Route::get('/follow-comic-list', 'UserController@userMangaFollowList')->name('follow-manga-list');
	Route::post('/follow-artist', 'UserController@followArtist')->name('follow-artist');
	Route::post('/unfollow-artist', 'UserController@unfollowArtist')->name('unfollow-artist');
	Route::get('/follow-artist-list', 'UserController@userArtistFollowList')->name('follow-artist-list');

	Route::get('/order-history', 'MangaController@userMangaRequests')->name('order-history');
	Route::get('/series-updates', 'MangaController@seriesUpdates')->name('series-updates');
	Route::post('/mark-notification', 'UserController@markUpdateRead')->name('mark-notification');

	//Publisher Menu
	Route::get('/publisher-artists', 'PublisherController@listArtists')->name('publisher-artists');
	Route::get('/publisher-artist-add/{slug?}', 'PublisherController@addArtist')->name('publisher-artist-add');
	Route::post('/publisher-artist-add', 'PublisherController@saveArtist')->name('publisher-artist-add-post');
	Route::get('/delete-publisher-artist/{slug}', 'PublisherController@deleteArtist')->name('delete-publisher-artist');

});



Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    Route::get('profile', ['uses' =>'Voyager\VoyagerController@profile', 'as' => 'voyager.profile']);
    Route::get('profile/edit', ['uses' =>'Voyager\VoyagerController@profileEdit', 'as' => 'voyager.profile.edit']);
    Route::post('profile/edit', ['uses' =>'Voyager\VoyagerController@profileUpdate', 'as' => 'voyager.profile.update']);

    Route::get('users', ['uses' =>'Voyager\VoyagerBaseController@users_index', 'as' => 'voyager.users.index']);

    Route::post('mangas/featured', ['uses' =>'Voyager\VoyagerController@mangasFeatured', 'as' => 'voyager.mangas.featured']);
  
    Route::post('manga/featured', ['uses' =>'Voyager\VoyagerController@mangaFeatured', 'as' => 'voyager.manga.featured']);
    Route::post('manga/unfeatured', ['uses' =>'Voyager\VoyagerController@mangaUnFeatured', 'as' => 'voyager.manga.unfeatured']);

  	
    Route::post('manga/reject', ['uses' =>'Voyager\VoyagerController@mangaReject', 'as' => 'voyager.manga.reject']);
  	Route::post('mangas/publish', ['uses' =>'Voyager\VoyagerController@mangasPublish', 'as' => 'voyager.mangas.publish']);
  
    Route::post('manga/import', ['uses' =>'MangaController@importNew', 'as' => 'voyager.manga.import']);
    Route::post('manga/publish', ['uses' =>'Voyager\VoyagerController@mangaPublish', 'as' => 'voyager.manga.publish']);
    Route::post('manga/reject', ['uses' =>'Voyager\VoyagerController@mangaReject', 'as' => 'voyager.manga.reject']);


    Route::group([
	    'as' => 'voyager.page-blocks.',
	    'prefix' => 'admin/page-blocks/',
	    'middleware' => ['web', 'admin.user']
	], function () {
	    Route::post('sort', ['uses' => "PageBlockController@sort", 'as' => 'sort']);
	    Route::post('minimize', ['uses' => "PageBlockController@minimize", 'as' => 'minimize']);
	});

    Route::group([
    'as' => 'voyager-frontend.pages.',
	    'prefix' => 'admin/pages/',
	    'middleware' => ['web', 'admin.user'],
	], function () {
	    Route::post('layout/{id?}', ['uses' => "PageController@changeLayout", 'as' => 'layout']);
	});

});

Route::get('homepagecontent','HomeController@homeContent')->name('homepagecontent');
Route::get('/adrevenue','AdRevenueController@add');

Route::get('/testmail','HomeController@mailTest');

Route::get('/{slug?}', 'PageController@getPage')->name('home');



//Route::get('/', 'HomeController@index')->name('home');



