var FormFileUpload = function () {
    return {
        //main function to initiate the module
        init: function () {

             // Initialize the jQuery File Upload widget:
            $('#fileupload').fileupload({
                sequentialUploads:true,
                disableImageResize: false,
                dropZone: $('#dropzone'),
                maxNumberOfFiles:9999,
                autoUpload: false,
                disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
                maxFileSize: 5000000,
                acceptFileTypes: /^image\/(png|jpg|jpeg)$/i,
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},                
                // change: function (e, data) {
                //     $.each(data.files, function (index, file) {
                //         var filewidth=0;
                //         var fileheight=0;
                //         var filetypes=/^image\/(png|svg\+xml)$/i;
                //         if(filetypes.test(file.type) ||
                //                 filetypes.test(file.name))
                //         {
                //             var reader = new FileReader();

                //             reader.onload = function (e) {
                //                 var image = new Image();
                //                 image.src = reader.result;
                //                 image.onload = function() {
                //                     filewidth=image.width;
                //                     fileheight=image.height;
                //                     if(filewidth < 512 || fileheight < 512){
                //                          file.error = settings.i18n('imageSize');
                //                     }
                //                 };
                //             }

                //             reader.readAsDataURL(file);
                //         }

                //     });
                // }
            });
            // Enable iframe cross-domain access via redirect option:
            $('#fileupload').fileupload(
                'option',
                'redirect',
                window.location.href.replace(
                    /\/[^\/]*$/,
                    '/cors/result.html?%s'
                )
            );

            // Upload server status check for browsers with CORS support:
            if ($.support.cors) {
                $.ajax({
                    type: 'HEAD'
                }).fail(function () {
                    $('<div class="alert alert-danger"/>')
                        .text('Upload server currently unavailable - ' +
                                new Date())
                        .appendTo('#fileupload');
                });
            }

            // Load & display existing files:
            $('#fileupload').addClass('fileupload-processing');
            $.ajax({
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},
                type: "POST",
                url: $('#fileupload').attr("data-action"),
                dataType: 'json',
                context: $('#fileupload')[0]

            }).always(function () {
                $(this).removeClass('fileupload-processing');
            }).done(function (result) {
                $(this).fileupload('option', 'done')
                .call(this, $.Event('done'), {result: result});
            });
        var counter = 0;
        
        // $('#fileupload').bind('fileuploaddone', function (e, data) {
        // $('a.buynow').removeAttr('disabled');
        //         console.log(counter + "neetu / " + number_of_files);
                
        //     });

            var number_of_files = 0; // or sessionStorage.getItem('number_of_files')
            $('#fileupload')
                .bind('fileuploadadd', function (e, data) {
                    $.each(data.files, function (index, file) {
                        console.log('Added file in queue: ' + file.name);
                        number_of_files = number_of_files + 1;
                        sessionStorage.setItem('number_of_files', number_of_files);
                    });
                })
                .bind('fileuploadprocessdone', function (e, data) {
                    console.log(counter + " / " + number_of_files);
                });
        }

    };

}();

jQuery(document).ready(function() {
    FormFileUpload.init();
});


