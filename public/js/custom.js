jQuery(window).on('load', function () {
	jQuery("#preloader").fadeOut();
});
jQuery(function (jQuery) {
	//Preloader
	// Wow 
	wow = new WOW({
		boxClass: 'wow', // default
		animateClass: 'animated', // default
		offset: 0, // default
		mobile: false, // default
		live: true // default
	})
	wow.init();
	// Bootstrap Slider 
	jQuery('.carousel').carousel({
		interval: 3000
		, cycle: true
	});
	/**** Team Slider ****/
	jQuery('#latest_slide').owlCarousel({
		autoplay: 3000
		, loop: true
		, margin: 10
		, responsiveClass: true
		, nav: true
		, responsive: {
			0: {
				items: 1
				, nav: true
			}
			, 600: {
				items: 2
				, nav: true
			}
			, 992: {
				items: 4
				, nav: true
			, }
		}
	})
	jQuery('#manga_slide').owlCarousel({
		autoplay: 3000
		, loop: true
		, margin: 10
		, responsiveClass: true
		, nav: true
		, responsive: {
			0: {
				items: 1
				, nav: true
			}
			, 600: {
				items: 2
				, nav: true
			}
			, 992: {
				items: 5
				, nav: true
			, }
		}
	})
	jQuery("nav a.nav-link").prepend("<span></span>");
	/**** Select Placeholder ****/
	jQuery('.contact_form select').change(function () {
		if (jQuery(this).children('option:first-child').is(':selected')) {
			jQuery(this).addClass('placeholder1');
		}
		else {
			jQuery(this).removeClass('placeholder1');
		}
	});
	try {
		var menu = $('.js-item-menu');
		var sub_menu_is_showed = -1;
		for (var i = 0; i < menu.length; i++) {
			$(menu[i]).on('click', function (e) {
				e.preventDefault();
				$('.js-right-sidebar').removeClass("show-sidebar");
				if (jQuery.inArray(this, menu) == sub_menu_is_showed) {
					$(this).toggleClass('show-dropdown');
					sub_menu_is_showed = -1;
				}
				else {
					for (var i = 0; i < menu.length; i++) {
						$(menu[i]).removeClass("show-dropdown");
					}
					$(this).toggleClass('show-dropdown');
					sub_menu_is_showed = jQuery.inArray(this, menu);
				}
			});
		}
		$(".js-item-menu, .js-dropdown").click(function (event) {
			event.stopPropagation();
		});
		$("body,html").on("click", function () {
			for (var i = 0; i < menu.length; i++) {
				menu[i].classList.remove("show-dropdown");
			}
			sub_menu_is_showed = -1;
		});
	}
	catch (error) {
		console.log(error);
	}
	/**** dashboard menu *****/
	jQuery(".drop-down").click(function () {
		jQuery(this).next().toggle();
		jQuery(this).toggleClass("open");
	});
	/**** Textarea First Letter Capital ****/
	jQuery('textarea.form_control').on('keypress', function (event) {
		var $this = jQuery(this)
			, thisVal = $this.val()
			, FLC = thisVal.slice(0, 1).toUpperCase();
		con = thisVal.slice(1, thisVal.length);
		jQuery(this).val(FLC + con);
	});
});
/**** Custom Browse ****/
/**** Date picker *****/
jQuery(function () {
	jQuery("#datepicker1").datepicker();
	jQuery("#dob").datepicker({ maxDate: 0 });
});
$(document).on('click', '.browse', function () {
	var file = $(this).parent().parent().parent().find('.file');
	file.trigger('click');
});
$(document).on('change', '.file', function () {
	$(this).parent().find('.form_control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
});


// RajPut
// Jquery-Added-For-Radio-label
jQuery('.brwse_list label').on('click', function () {
	jQuery(this).addClass('year').siblings().removeClass('year');
});
/* -=- Bootstrap-Modal-Multiple =-=- */
jQuery('.modal').on("hidden.bs.modal", function (e) {
		if ($('.modal:visible').length) {
			jQuery('.modal-backdrop').first().css('z-index', parseInt($('.modal:visible').last().css('z-index')) - 10);
			jQuery('body').addClass('modal-open');
		}
	})
	.on("show.bs.modal", function (e) {
		if ($('.modal:visible').length) {
			jQuery('.modal-backdrop.in').first().css('z-index', parseInt($('.modal:visible').last().css('z-index')) + 10);
			jQuery(this).css('z-index', parseInt($('.modal-backdrop.in').first().css('z-index')) + 10);
		}
	});
/*-== //Bootstrap-Modal-Multiple =---*/
// ToolTip
jQuery(document).ready(function () {
	jQuery('[data-toggle="tooltip"]').tooltip();
});

// Upload-Image
function readURL(input) {
if (input.files && input.files[0]) {
	var reader = new FileReader();
	reader.onload = function (e) {
		$('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
		$('#imagePreview').hide();
		$('#imagePreview').fadeIn(650);
	}
	reader.readAsDataURL(input.files[0]);
}
}
jQuery("#imageUpload,#artist-avatar").change(function () {
	readURL(this);
});

// use-This-Only-Dashboard-Menu
// (function () {
// 	$('.toggle-wrap').on('click', function () {
// 		$(this).toggleClass('active');
// 		$('#dashboard_sctn').animate({ width: 'toggle' }, 200);
// 	});
// })();
jQuery(window).on('load', function () {
	var width = jQuery(window).width();

	if (width < 991) {
		jQuery('body').find('.search_sec').detach().appendTo('.navbar');
	} else {
		jQuery('body').find('.search_sec').detach().insertAfter('.navbar-nav');
	}
});

// Mobile-View-Toggle-For-Dashboard
jQuery("#burger").click(function () {
	jQuery(".dashboard_main .col-sm-3").toggleClass("Left");
});
jQuery(document).on("click", ".burgerbutton", function (e) {
	e.preventDefault();
	jQuery(this).toggleClass('active');
});

jQuery(document).ready(function () {
	jQuery("#categories").click(function () {
		jQuery("#mobile_categories").slideToggle("slow");
	});
});

window.addEventListener('load', function(){
    var allimages= document.getElementsByTagName('img');
    for (var i=0; i<allimages.length; i++) {
        if (allimages[i].getAttribute('data-src')) {
            allimages[i].setAttribute('src', allimages[i].getAttribute('data-src'));
        }
    }
}, false);
