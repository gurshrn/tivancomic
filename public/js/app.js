
 $(document).ready(function() {
 	// body...
 $("form").validationEngine({promptPosition : "topRight", scroll: false});
 
$('#signup_modal #myTab a[data-toggle="tab"]').on('click', function (e) {
  $('#register_role').val($(e.target).attr("data-value"));
});

$(".timeAgo").timeago();
 });

 function slugify(string) {
  return string
    .toString()
    .trim()
    .toLowerCase()
    .replace(/\s+/g, "-")
    .replace(/[^\w\-]+/g, "")
    .replace(/\-\-+/g, "-")
    .replace(/^-+/, "")
    .replace(/-+$/, "");
}

$('.filters.cat_all').on('change', function(e){
    if($(this).prop('checked')){
        $('.filters.cat').prop('checked',true);
    }
    else{
         $('.filters.cat').prop('checked',false);
    }
    e.preventDefault();
}); 
$('.filters').on('change', function(e){
    $('#sort').val($('.sort').val());
    $.ajax({
           type: "GET",
           url: $('form.filters').attr('action'),
           data: $('form.filters').serialize(), // serializes the form's elements.
           success: function(data)
           {
              if(data.success){
                $('#action_form').modal('toggle');
                $('.chapter_sc').html(data.results);
              }             
           }
    });
    e.preventDefault();
}); 
$.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});

$(".ajax-post").submit(function(e) {
    if ($(this).validationEngine('validate')) {
      showBlockUi('please wait...');
      var form = $(this);
      var url = form.attr('action');
      $.ajax({
             type: "POST",
             url: url,
             data: form.serialize(), // serializes the form's elements.
             success: function(data)
             {
                $.unblockUI();
                if(data.success){
                  if(typeof data.reload !== 'undefined' && data.reload){
                    window.location.reload();
                  }
                  $.toast({
                      heading: 'Success',
                      text: data.message,
                      showHideTransition: 'slide',
                      position: 'top-right',
                      icon: 'success',
                  });
                }else{
                    var errors="";
                    if($.isArray(data.errors) || $.type(data.errors)=='object'){
                      $.each(data.errors, function(key, error){
                        errors+='<div>'+error+'</div>';
                      });
                    }
                    else
                    {
                      errors+='<div>'+data.errors+'</div>';
                    }
                  $.toast({
                      heading: 'Error',
                      text: errors,
                      showHideTransition: 'slide',
                      position: 'top-right',
                      icon: 'error',
                  });
                }              
             },
             error: function(data){
                  $.unblockUI();
                  if(data.statusText=='Unauthenticated.' || data.statusText=="Unauthorized")
                  {
                    data.statusText="Only verified users are authorized to perform this task."
                  }
                  $.toast({
                      heading: 'Error',
                      text: data.statusText,
                      showHideTransition: 'slide',
                      position: 'top-right',
                      icon: 'error',
                  });
             }
      });
    }

    e.preventDefault(); // avoid to execute the actual submit of the form.
});


function showBlockUi(heading) {
    try {
        $.blockUI({message: '<h2>' + heading + '</h2>', css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
    } catch (err) {
        console.log(err);
    }
}

//<<<<<<<=================== * UPLOAD AVATAR  * ===============>>>>>>>//
$(document).on('change', '#imageUpload', function(){
   (function(){
   $("#formAvatar").ajaxForm({
   dataType : 'json', 
   success:  function(e){
   if( e ){
     if( e.success == false ){
        var error = '';
        for($key in e.errors){
          error += '' + e.errors[$key] + '';
        }
        $.toast({
            heading: 'Error',
            text: ""+ error +"",
            showHideTransition: 'slide',
            position: 'top-right',
            icon: 'error',
        });
        $('#imageUpload').val('');

      } else {      
        $('#imageUpload').val('');
        $('.profile_img').attr('src',e.avatar);
      }    
    }//<-- e
      else {
           $.toast({
                heading: 'Error',
                text: "Error occurred",
                showHideTransition: 'slide',
                position: 'top-right',
                icon: 'error',
            });
          $('#imageUpload').val('');
      }
       }//<----- SUCCESS
    }).submit();
    })(); //<--- FUNCTION %
});//<<<<<<<--- * ON * --->>>>>>>>>>>
//<<<<<<<=================== * UPLOAD AVATAR  * ===============>>>>>>>//

$(document).ready(function () {
    //Disable cut copy paste on body
    // $('body').bind('cut copy paste', function (e) {
    //     e.preventDefault();
    // });
   
    //Disable mouse right click on body
    $("body").on("contextmenu",function(e){
        return false;
    });
});