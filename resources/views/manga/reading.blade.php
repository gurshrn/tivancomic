@extends('layouts.app')
@section('page_title', $manga->title)
@section('meta_title',$manga->title)
@section('dfp-ad', true)
@section('content')
@php 
$firstChapter=$manga->chapterRead[0];
$lastChapter=$manga->latestchapters[0];
$ch=(!isset($_GET['ch'])?$manga->chapterRead[0]->id:$_GET['ch']); 
$page_no=(!isset($_GET['page'])?'1':$_GET['page']);
$currentPage=getMangaPage($manga->id,$ch,$page_no);
$totalPages=getPagesCount($ch);
@endphp
<style>
@media only screen and (min-width: 767px) {
        /* styles for browsers larger than 960px; */
				#div-gpt-ad-1552576149890-0,#div-gpt-ad-1552576246788-0{
					display:none!important;
				}
}
@media only screen and (min-width: 320px) and  (max-width: 768px){
        /* styles for browsers larger than 960px; */
				#div-gpt-ad-1552576122929-0,#div-gpt-ad-1552576222370-0{
					display:none!important;
				}
}
</style>
<!-- manga_slider end -->
	<section class="manga_reading">
		<div class="container">
			<div class="row">
				<div class="col-sm-12"> 
				 <div class="manga_ad_cntr ads">
					 <figure>
						<!-- Home Page Bottom Ad -->
						     <!-- Responsive Top Reading -->
								<div id='div-gpt-ad-1552576122929-0'>
								</div>
								<div id='div-gpt-ad-1552576149890-0'>
								</div>	
						     </figure>
					</div>
					<a href="{{route('cms_manga_detail',$manga->slug)}}" title="{{$manga->title}}" ><h5 class="txt_cntr">{{$manga->title}}</h5></a>
					
					@include('manga.partials.readingPageNav')
					<div class="manga_reading_full">
						<div class="row">
							<div class="col-lg-2 col-md-2" style="width:200px;height:200px">
							<figure class="ads"><!-- Home Recent Ad Sidebar Right -->
								<div id='div-gpt-ad-1552576047376-0'>
								</div>
						     </figure>
							</div>
							<div class="col-lg-8 col-md-8">
							<div class="pdf_section"><figure>
							@if(Agent::isDesktop())@endif<a href="javascript:;" onclick="doReload({{$page_no==$totalPages?$lastChapter->id:$ch}},{{$page_no==$totalPages?($ch==$lastChapter->id?$page_no:0):$page_no}},'Next');"><img src="{{asset('storage/mangas/').'/'.$currentPage->manga_id.'/'.$currentPage->chapter()->manga_volume_id.'/'.$currentPage->manga_chapter_id.'/'.$currentPage->page_image}}" alt="pdf_img"></a>@if(Agent::isDesktop())@endif</figure></div>
						
							</div>
							<div class="col-lg-2 col-md-2"  style="width:200px;height:200px">
							<figure class="ads"><!-- Home Recent Ad Sidebar Right -->
						     	<div id='div-gpt-ad-1552576075995-0'>
								</div>
						     </figure>
							</div>
						</div>
						<!-- row -->
					</div>
					@include('manga.partials.readingPageNav')
					
					<div class="manga_reading_nav">
				
						<div class="btm_ad">
						<div class="manga_ad_cntr ads"> 
							<!-- /21770139633/Comics_Desktop_970x250_BTF -->
							<div id='div-gpt-ad-1552576222370-0'>
							</div>
							<div id='div-gpt-ad-1552576246788-0'>
							</div>
							</div></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- manga_reading end -->
@endsection

@section('page_script')
$(document).ready(function(){
	if({{$page_no}}==1 && {{$firstChapter->id}}=={{$ch}})
	{
		$('.previous').removeClass('active');
	}
	else if(!$('.previous').hasClass('active')){	
		$('.previous').addClass('active');
	}
	if({{$page_no}}=={{$totalPages}} && {{$lastChapter->id}}=={{$ch}})
	{
		$('.next').removeClass('active');
	}
	else if(!$('.next').hasClass('active')){	
		$('.next').addClass('active');
	}
});

function doReload(ch,page,type=""){
	if(type=='Previous'){
		page=page-1;
		if({{$firstChapter->id}}=={{$ch}} && page==0){
			page=1;
		}		
	}
	if(type=='Next' && page<{{$totalPages}}){
		page=page+1;
	}
	document.location = '{{Route(Route::currentRouteName(),Route::current()->parameters)}}?ch=' 
		+ ch+"&page="+page;
}

$(document).keydown(
    function(e)
    {   
     	if (e.keyCode == 39) {      
            $('a.next')[0].click()
        }
        else if (e.keyCode == 37) {   
            $('a.previous')[0].click()
        }
    }
);
//$('html, body').animate({
 //   scrollTop: $(".manga_reading_full").offset().top-10
//}, 5000);
var width = $(window).width();
	//Comics_Desktop_970x250_ATF:
  googletag.cmd.push(function() {
    googletag.defineSlot('/21770139633/Comics_Desktop_970x250_ATF', [[468, 60], [728, 90], [970, 250], [970, 90], [728, 20]], 'div-gpt-ad-1552576122929-0').addService(googletag.pubads());
		googletag.pubads().enableSingleRequest();
		googletag.enableServices();
  });
  //Comics_Mobile_320x100_ATF:
   googletag.cmd.push(function() {
        googletag.defineSlot('/21770139633/Comics_Mobile_320x100_ATF', [[300, 250], [320, 50], [180, 150], [320, 100]], 'div-gpt-ad-1552576149890-0').addService(googletag.pubads());
		googletag.pubads().enableSingleRequest();
		googletag.enableServices();
   });
//Comics_ Desktop_160x600_Left:
  googletag.cmd.push(function() {
    googletag.defineSlot('/21770139633/Comics_Desktop_160x600_Left', [[160, 600], [120, 600], [120, 240], [300, 600], [240, 400]], 'div-gpt-ad-1552576047376-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });
//Comics_ Desktop_160x600_Right:
  googletag.cmd.push(function() {
      googletag.defineSlot('/21770139633/Comics_Desktop_160x600_Right', [[160, 600], [240, 400], [300, 600], [120, 240], [120, 600]], 'div-gpt-ad-1552576075995-0').addService(googletag.pubads());
		googletag.pubads().enableSingleRequest();
		googletag.enableServices();
  });
//Comics_Desktop_970x250_BTF:
  googletag.cmd.push(function() {
    googletag.defineSlot('/21770139633/Comics_Desktop_970x250_BTF', [[728, 90], [728, 20], [468, 60], [970, 90], [970, 250], [600, 120]], 'div-gpt-ad-1552576222370-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });
//Comics_Mobile_320x100_BTF:
   googletag.cmd.push(function() {
       googletag.defineSlot('/21770139633/Comics_Mobile_320x100_BTF', [[180, 250], [320, 50], [300, 250], [320, 100]], 'div-gpt-ad-1552576246788-0').addService(googletag.pubads());
		googletag.pubads().enableSingleRequest();
		googletag.enableServices();
   });

  googletag.cmd.push(function() { 
	  if(width >= 768){
		googletag.display('div-gpt-ad-1552576122929-0'); 
		googletag.display('div-gpt-ad-1552576222370-0');
		googletag.display('div-gpt-ad-1552576047376-0');
		googletag.display('div-gpt-ad-1552576075995-0');
	  } 
	  if(width >= 320 && width <= 767){	
		googletag.display('div-gpt-ad-1552576149890-0');
		googletag.display('div-gpt-ad-1552576246788-0');
		
	  }
});
@endsection

