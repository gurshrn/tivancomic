@extends('layouts.app')
@section('page_title')
Publish Comic
@endsection

@section('content')
	<section class="about_banner" style="background:url({{asset('images/about_banner.jpg')}}) no-repeat;">
		<div class="container">
			<div class="about_caption wow fadeInDown" data-wow-delay="0.2s" data-wow-duration="1000ms">
				<h2>Dashboard</h2> </div>
		</div>
		<div class="banner_scroll">
         <a href="#dashboard_sctn" title=""><i class="fa fa-angle-down" aria-hidden="true"></i></a>     
        </div>
	</section>
	<!--about_banner end -->
	<section class="dashboard_main">
		<div class="container">
			<div class="row">
				@include('layouts/partials/sidebar')
				<div class="col-sm-9 dashboard_rght">
					<h3 class="title_sctn">Upload Comic</h3>
					<div class="dashboard_side_cntnt">
						<div class="myprofile_sctn">
							<form class="edit_manga" method="post" action="{{route('save-manga')}}" enctype="multipart/form-data">
								 @csrf
								 @if($manga)
								 	<input type="hidden" name="id" value="{{$manga->id}}">
								 @endif
								<div class="file_sctn">
									<input type="file" name="cover" class="file" id="filePhoto">
									<div class="cstm_brws">
										<input type="text" class="form_control" disabled="" placeholder="Upload Comic Cover"> <span class="input-group-btn">
        								<button class="browse browse_btn" type="button"><i class="fa fa-paperclip" aria-hidden="true"></i> Upload Image</button>
      									</span> </div>
								</div>
								<div class="{{((!isArtist() && isPublisher()) || isAdmin())?'full_inpt1':'med_inpt1'}}">
									<p>
										<input type="text" name="title" placeholder="Title" class="form_control validate[required]" maxlength="100" value="{{$manga?$manga->title:''}}"> </p>
										@if(isArtist() && !isPublisher() && !isAdmin())
										<p class="slct drop-slct">
											<select class="form_control placeholder1 validate[required] category" name="category_id[]" multiple data-live-search="true">
												<option value="">Category</option>
												@foreach(getCategories() as $cat)
												<option value="{{$cat->id}}" {{ checkSelected($cat->id , 'category_id', $manga ? explode(',',$manga->category_id) : null) }} >{{$cat->name}}</option>
												@endforeach
											</select>
										</p>
										@endif
									
								</div>
								@if((!isArtist() && isPublisher()) || isAdmin())
								<div class="med_inpt1">
									<p class="slct drop-slct">
										<select class="form_control placeholder1 validate[required] category" name="category_id[]" multiple data-live-search="true">
											<option value="">Category</option>
											@foreach(getCategories() as $cat)
											<option value="{{$cat->id}}" {{ checkSelected($cat->id , 'category_id', $manga ? explode(',',$manga->category_id) : null) }} >{{$cat->name}}</option>
											@endforeach
										</select>
									</p>
									<p class="slct">
										<select class="form_control placeholder1 form_control validate[required]" name="artist_id" id="artist_id">
											<option value="">Artist</option>
											@foreach(getArtists() as $artist)
											<option value="{{$artist->id}}" {{ checkSelected($artist->id , 'artist_id', $manga ? $manga->artist_id : null) }} >{{$artist->name}}</option>
											@endforeach
										</select>
									</p>
								</div>
								@endif
								<div class="med_inpt1">
									<p class="slct">
										<select class="form_control placeholder1 validate[required]" name="release_status">
											<option value="">Status</option>
											@foreach(getManagReleaseStatus() as $status)
											<option value="{{$status}}"  {{ checkSelected($status , 'release_status', $manga ? $manga->release_status : null) }}>{{$status}}</option>
											@endforeach
										</select>
									</p>
									<p>
										<input type="number" class="form_control placeholder1 validate[required,min[1970],max[{{date('Y')}}]]" name="release_year" min="1970" max="{{date('Y')}}" placeholder="Release Year eg: {{date('Y')}}"  value="{{$manga?$manga->release_year:''}}">
									</p>
								</div>
								<div class="full_inpt1">
									<textarea placeholder="Description" class="form_control" name="description"> {{$manga?$manga->description:''}}</textarea>
								</div>
								 <input type="hidden" class="form-control" name="slug" placeholder="Slug" data-slug-origin="title" data-slug-forceupdate="true"       value="{{$manga?$manga->slug:''}}">
								@if(isAdmin())
								<div class="remembr_sctn">
									<input type="checkbox" id="is_featured" name="is_featured"
									{{$manga?($manga->is_featured?'checked':''):''}}>
									<label for="is_featured">Featured</label>
								</div>
								@endif 
								<div class="remembr_sctn">
									<input type="checkbox" id="rd1" name="agreement" class=" validate[required]">
									<label for="rd1">I agree to Terms & Conditions</label>
								</div>
								<div class="login_loader save_btn_bx">
									<input type="submit" value="Upload" class="view_btn"> </div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
@section('page_script_links')
  <script src="{{ asset('js/slugify.js') }}"></script>

@endsection
@section('page_script')
	$('select.category').selectpicker();
	$('#user_id').change(function(){
		if($(this).val()=='0')
			$('#artist').removeAttr('disabled');
		else
			$('#artist').attr('disabled','disabled');
		
	});	
	$('input[data-slug-origin]').each(function(i, el) {
        $(el).slugify();
    });	


$("#filePhoto").on('change', function(){

	var loaded = true;
	if(window.File && window.FileReader && window.FileList && window.Blob){
		if($(this).val()){ //check empty input filed

    <?php $dimensions = explode('x','214x322'); ?>
    oFReader = new FileReader();
			
	oFReader.onload = function (e) {

		var image = new Image();
	    image.src = oFReader.result;

		image.onload = function() {

				if( image.width < {{ $dimensions[0] }}) {
		    		$.toast({
			                heading: 'Error',
			                text: "File size should be min '214px * 322px'",
			                showHideTransition: 'slide',
			                position: 'top-right',
			                icon: 'error',
			            });
			        $('#filePhoto').val('');
		    		return false;
		    	}

		    	if( image.height < {{ $dimensions[1] }} ) {
		    		$.toast({
			                heading: 'Error',
			                text: "File size should be min '214px * 322px'",
			                showHideTransition: 'slide',
			                position: 'top-right',
			                icon: 'error',
			            });
			        $('#filePhoto').val('');
		    		return false;
		    	}
		    };// <<--- image.onload

		   }
		   oFReader.readAsDataURL($(this)[0].files[0]);
		}
   }	
 });						
@endsection