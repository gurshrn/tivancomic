@extends('layouts.app')
@section('page_title')
Publish Comic
@endsection

@section('content')
<section class="about_banner" style="background:url({{asset('images/about_banner.jpg')}}) no-repeat;">
	<div class="container">
		<div class="about_caption wow fadeInDown" data-wow-delay="0.2s" data-wow-duration="1000ms">
			<h2>Dashboard</h2> </div>
	</div>
	<div class="banner_scroll">
     <a href="#dashboard_sctn" title=""><i class="fa fa-angle-down" aria-hidden="true"></i></a>     
    </div>
</section>
<section class="dashboard_main">
	<div class="container">
		<div class="row">
			@include('layouts/partials/sidebar')
			<div class="col-sm-9 dashboard_rght">
				<h3 class="title_sctn">{{$action=='edit'?'Update':($action=='add'?'Add New':'Delete')}} Volume</h3>
				<div class="dashboard_side_cntnt">
					<div class="myprofile_sctn">
						<form class="edit_manga" action="" method="post">
							 @csrf
							<div class="med_inpt1">
								<p class="slct">
									<select class="form_control placeholder1 validate[required]" name="manga_id" id="manga_id">
										<option value="">Select Comic</option>
										@foreach(getMangaByRole() as $manga)
										<option value="{{ $manga->id}}">{{ $manga->title }}</option>
										@endforeach
									</select>
								</p>
								@if($action!='add')
								<p class="slct">
									<select class="form_control placeholder1 validate[required]" name="manga_volume_id" id="manga_volume_id">
										<option value="">Select Volume</option>
									</select>
								</p>
								@else
								<p>
									<input type="text" placeholder="Enter Volume Number" class="form_control validate[required]" maxlength="20" name="name"> </p>
								@endif
							</div>
							@if($action!='delete')
							<div class="med_inpt1">
							@if($action=='edit')
								<p class="pos_rel">
									<input type="text" placeholder="Rename" class="form_control validate[required]" maxlength="20" name="name" id="name"> </p>
							@endif
								<p>
										<input type="number" placeholder="Total Copies" class="form_control validate[required]" min="1" step="1" max="1000000" name="total_copies" id="total_copies"> </p>
							</div>
							@endif
							<div class="login_loader save_btn_bx">
								<input type="submit" value="{{$action=='edit'?'Update':($action=='add'?'Save':'Delete')}}" class="view_btn"> </div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection

@section('page_script')
@if($action!='add')
	$('#manga_id').on('change', function(e){
        console.log(e);
        var manga_id = e.target.value;
        var url='{{ route('manga-volume-list','manga_id') }}'.replace('manga_id', manga_id);
    	$.get(url, function(data) {
		    console.log(data);
		    var manga_volume_id=$('#manga_volume_id');
		    manga_volume_id.empty();
		    manga_volume_id.append('<option value="">Select Volume</option>');
		     $.each(data, function(value, display){
		         manga_volume_id.append('<option value="' + display.id + '" data-value="' + display.total_copies + '">' + display.name + '</option>');
		    });
		});
	});	
@if($action!='delete')
	$('#manga_volume_id').on('change', function(e){
        console.log(e);
       	$('#name').val($(this).find('option:selected').text());
       	$('#total_copies').val($(this).find('option:selected').attr('data-value'));
	});									
@endif
@endif
@endsection