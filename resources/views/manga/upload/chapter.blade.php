@extends('layouts.app')
@section('page_title')
Publish Comic
@endsection


@section('css')
  <!-- Sweet Alert -->
<link href="{{ asset('plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/jQuery-File-Upload/css/jquery.fileupload.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/jQuery-File-Upload/css/jquery.fileupload-ui.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<section class="about_banner" style="background:url({{asset('images/about_banner.jpg')}}) no-repeat;">
	<div class="container">
		<div class="about_caption wow fadeInDown" data-wow-delay="0.2s" data-wow-duration="1000ms">
			<h2>Dashboard</h2> </div>
	</div>
	<div class="banner_scroll">
     <a href="#dashboard_sctn" title=""><i class="fa fa-angle-down" aria-hidden="true"></i></a>     
    </div>
</section>
<section class="dashboard_main">
	<div class="container">
		<div class="row">
			@include('layouts/partials/sidebar')
			<div class="col-sm-9 dashboard_rght">
				<h3 class="title_sctn">{{$action=='edit'?'Update':($action=='add'?'Add New':'Delete')}} Chapter</h3>
				<div class="dashboard_side_cntnt">
					<div class="myprofile_sctn">
						<form class="edit_manga" action="" method="POST" enctype="multipart/form-data">
							 @csrf
							<div class="med_inpt1">
								<p class="slct">
									<select class="form_control placeholder1 validate[required]" name="manga_id" id="manga_id">
										<option value="">Select Comic</option>
										@foreach(getMangaByRole() as $manga)
										<option value="{{ $manga->id}}">{{ $manga->title }}</option>
										@endforeach
									</select>
								</p>
								<p class="slct">
									<select class="form_control placeholder1 validate[required]" name="manga_volume_id" id="manga_volume_id">
										<option value="">Select Volume</option>
									</select>
								</p>
							</div>
							<div class="med_inpt1">
								@if($action!='add')
								<p class="slct">
									<select class="form_control placeholder1 validate[required]" name="manga_chapter_id" id="manga_chapter_id">
										<option value="">Select Chapter</option>
									</select>
								</p>
								@endif
								@if($action!='delete')
								<p class="pos_rel">
									<input type="text" placeholder="Release Date" class="form_control  validate[required,past[now]]" id="datepicker1" name="release_date" readonly> <i class="fa fa-calendar" aria-hidden="true"></i></p>
								@endif
								@if($action=='add')
								<p>
										<input type="number" min="1" step="1" placeholder="Chapter Number" class="form_control validate[required]" maxlength="100" name="number"> </p>
								@endif
								
							</div>
							@if($action!='delete')
							<div class="med_inpt1">
								@if($action=='edit')
								<p>
										<input type="text" placeholder="Chapter Number" class="form_control validate[required]" maxlength="100" name="number" id="number"> </p>
								@endif
								<p>
									<input type="text" placeholder="Chapter Name" class="form_control validate[required]" maxlength="100" name="name" id="name"> </p>
							</div>
							
				            @endif
							@if($action!='add')
							<table role="presentation" class="table table-striped clearfix">
								<thead>
									<tr>
					                    <th>Page</th>
					                    <th>Page No.</th>
					                    <th>Actions</th>
					                </tr>
				                
								</thead>
				                <tbody class="files">
				                	<tr class="template-download">
									    <td colspan="3">
									       No pages yet
									      </td>
									</tr>
				                 </tbody>
				            </table>
				            @endif
							<div class="login_loader save_btn_bx">
								<input type="submit" value="{{$action=='edit'?'Update':($action=='add'?'Save':'Delete')}}" class="view_btn"> </div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@section('page_script_links')
    <script src="{{ asset('plugins/sweetalert/sweetalert.min.js') }}"></script>
	 
@endsection
@section('page_script')
	$('#manga_id').on('change', function(e){
        console.log(e);
        var manga_id = e.target.value;
        var url='{{ route('manga-volume-list','manga_id') }}'.replace('manga_id', manga_id);
    	$.get(url, function(data) {
		    var manga_volume_id=$('#manga_volume_id');
		    manga_volume_id.empty();
		    manga_volume_id.append('<option value="">Select Volume</option>');
		    $.each(data, function(value, display){
		         manga_volume_id.append('<option value="' + display.id + '">' + display.name + '</option>');
		    });
		    var manga_chapter_id=$('#manga_chapter_id');
		    manga_chapter_id.empty();		    
		    manga_chapter_id.append('<option value="">Select Chapter</option>');
		    @if($action!='add')
			 getPages(0);
			@endif
		});
	});		
	$('#manga_volume_id').on('change', function(e){
        console.log(e);
        var manga_id = $('#manga_id').val();
        var manga_volume_id = e.target.value;
        var url='{{ route('manga-chapter-list',['manga_id'=>'manga_id','manga_volume_id'=>'manga_volume_id']) }}'.replace('manga_id', manga_id).replace('manga_volume_id', manga_volume_id);
    	$.get(url, function(data) {
		    var manga_chapter_id=$('#manga_chapter_id');
		    manga_chapter_id.empty();		    
		    manga_chapter_id.append('<option value="">Select Chapter</option>');
			$.each(data, function(value, display){
		         manga_chapter_id.append('<option value="' + display.id + '">' +display.number+" - " +display.name + '</option>');
		    });

		    @if($action!='add')
			 getPages(0);
			@endif
		});
	});	
@if($action!='add')
		
    $('#manga_chapter_id').on('change', function(e){
        console.log(e);
        var manga_chapter_id = e.target.value;
        getPages(manga_chapter_id);
	});	

 	$('body').on('click','table tr .delete', function (e) {
	 var delete_url = $(this).attr('data-url');
       
     swal({
         title: "Are you sure, you want to delete page?",  
         type: "warning", 
         showLoaderOnConfirm: true,
         showCancelButton: true,   
         confirmButtonColor: "#DD6B55",  
         confirmButtonText: "Yes sure!",   
         cancelButtonText: "Cancel",  
         closeOnConfirm: false, 
       }, 
       function(isConfirm){  
         if (isConfirm) {   
          $.get(delete_url, function(data) {
				getPages($('#manga_chapter_id').val());
		  });
          swal.close();
       }
    });
  return false;
});

function getPages(manga_chapter_id){
		@if($action=='edit')
		var chapter_url='{{route('manga-chapter-detail','manga_chapter_id')}}'.replace('manga_chapter_id', manga_chapter_id);
		$.get(chapter_url, function(data) {
			if(data==""){
				$('#number').val('');    
				$('#name').val('');  
				$('#datepicker1').val('');
			}
			else{
				$('#number').val(data.number);    
				$('#name').val(data.name);  
				var date=data.release_date.split('-');
				if(date.length>1){
					date=date[1]+'/'+date[2]+'/'+date[0];
				}			
				$('#datepicker1').val(date);
			}    
		});
		@endif

		var manga_id = $('#manga_id').val();
        var url='{{ route('manga-page-list',['manga_id'=>'manga_id','manga_chapter_id'=>'manga_chapter_id']) }}'.replace('manga_id', manga_id).replace('manga_chapter_id', manga_chapter_id);
    	$.get(url+"?view=manga.upload.partials.pages", function(data) {
		    var pages=$('table .files');
		    pages.empty();		    
		    pages.html(data);
		});
	}
@endif
								
@endsection
