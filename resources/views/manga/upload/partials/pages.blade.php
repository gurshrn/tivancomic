@if(count($pages)>0)
@foreach($pages as $page)
<tr class="template-download">
     <td>
        <span class="preview">
            <img src="{{asset('storage/mangas/').'/'.$page->manga_id.'/'.$page->chapter()->manga_volume_id.'/'.$page->manga_chapter_id.'/'.$page->page_image}}" width="90px" >
         </span>
    </td>
    <td>
        <p class="name">
         <span><input type="number" value="{{$page->page_no}}" name="page_no[]" class="form_control validate[required]"></span>    </p>
    </td>
    <td align="center">
        <input type="hidden" name="page_id[]" value="{{$page->id}}">
        <a class="btn red delete btn-sm" data-url="{{route('manag-page-delete',$page->id)}}" href="javascript:;">
            <i class="fa fa-trash-o"></i>
            <span>Delete</span>
        </a>
      </td>
</tr>
@endforeach
@else
<tr class="template-download">
    <td colspan="3">
       No pages yet
      </td>
</tr>
@endif