@if($pages)
@foreach($pages as $page)
    <tr class="template-download">
        <td>
            <span class="preview">
                <img src="{{asset('storage/mangas/').'/'.$page->manga_id.'/'.$page->chapter()->manga_volume_id.'/'.$page->manga_chapter_id.'/'.$page->page_image}}" width="90px" >
             </span>
        </td>
        <td>
            <p class="name">
             <span>{{$page->page_no}}</span>    </p>
        </td>
        <td>
            <span class="size">{{round(filesize(storage_path('app/public/mangas/').$page->manga_id.'/'.$page->chapter()->manga_volume_id.'/'.$page->manga_chapter_id.'/'.$page->page_image)/1024,2)}} KB</span>
       </td>
        <td align="center">
            <input type="hidden" name="id" value="{{$page->id}}">
            <button class="btn red delete btn-sm" data-type="get" data-url="{{route('manag-page-delete',$page->id)}}">
                <i class="fa fa-trash-o"></i>
                <span>Delete</span>
            </button>
          </td>
    </tr>
@endforeach
@endif