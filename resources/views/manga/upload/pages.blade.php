@extends('layouts.app')
@section('page_title')
Publish Comic
@endsection


@section('css')
  <!-- Sweet Alert -->
<link href="{{ asset('plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/jQuery-File-Upload/css/jquery.fileupload.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/jQuery-File-Upload/css/jquery.fileupload-ui.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<section class="about_banner" style="background:url({{asset('images/about_banner.jpg')}}) no-repeat;">
	<div class="container">
		<div class="about_caption wow fadeInDown" data-wow-delay="0.2s" data-wow-duration="1000ms">
			<h2>Dashboard</h2> </div>
	</div>
	<div class="banner_scroll">
     <a href="#dashboard_sctn" title=""><i class="fa fa-angle-down" aria-hidden="true"></i></a>     
    </div>
</section>
<section class="dashboard_main">
	<div class="container">
		<div class="row">
			@include('layouts/partials/sidebar')
			<div class="col-sm-9 dashboard_rght">
				<h3 class="title_sctn">Update Chapter Pages</h3>
				<div class="dashboard_side_cntnt">
					<div class="myprofile_sctn">
						<form class="edit_manga" action="{{route('manga-page-post')}}" data-action="{{route('manga-page-post')}}" id="fileupload" method="POST" enctype="multipart/form-data">
							 @csrf
							<div class="med_inpt1">
								<p class="slct">
									<select class="form_control placeholder1 validate[required]" name="manga_id" id="manga_id">
										<option value="">Select Comic</option>
										@foreach(getMangaByRole() as $manga)
										<option value="{{ $manga->id}}">{{ $manga->title }}</option>
										@endforeach
									</select>
								</p>
								<p class="slct">
									<select class="form_control placeholder1 validate[required]" name="manga_volume_id" id="manga_volume_id">
										<option value="">Select Volume</option>
									</select>
								</p>
							</div>
							<div class="med_inpt1">
								<p class="slct">
									<select class="form_control placeholder1 validate[required]" name="manga_chapter_id" id="manga_chapter_id">
										<option value="">Select Chapter</option>
									</select>
								</p>
								
								
							</div>
							
							 <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
				            <div class="row fileupload-buttonbar">
				                <div class="col-lg-7">
				                    <!-- The fileinput-button span is used to style the file input field as button -->
				                    <span class="btn btn-success fileinput-button">
				                        <i class="fa fa-plus"></i>
				                        <span> Add files... </span>
				                        <input type="file" name="files[]" multiple=""> </span>
				                    <button type="submit" class="btn blue start">
				                        <i class="fa fa-upload"></i>
				                        <span> Start upload </span>
				                    </button>
				                    <button type="reset" class="btn btn-warning cancel">
				                        <i class="fa fa-ban-circle"></i>
				                        <span> Cancel upload </span>
				                    </button>
				                   <!--  <button type="button" class="btn red delete">
				                        <i class="fa fa-trash"></i>
				                        <span> Delete </span>
				                    </button>
				                      <label class="padding-zero">
				                        <input type="checkbox" class="toggle"></label> -->
				                    <!-- The global file processing state -->
				                    <span class="fileupload-process"> </span>
				                </div>
				                <!-- The global progress information -->
				                <div class="col-lg-5 fileupload-progress fade">
				                    <!-- The global progress bar -->
				                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
				                        <div class="progress-bar progress-bar-success" style="width:0%;"> </div>
				                    </div>
				                    <!-- The extended global progress information -->
				                    <div class="progress-extended"> &nbsp; </div>
				                </div>
				            </div>
				            <div id="dropzone" class="dropzone-file-area">
				            <div class="col-md-12 col-sm-12">
				                <i class="fa fa-cloud-upload "></i>
				            <h3 class="sbold">Drop files here or click to Add files</h3>
				            <p>Any .PNG, .JPG or drag pages here</p></div>
				            <div class="clearfix"></div>
				            </div>
				            <!-- The table listing the files available for upload/download -->
				            <table role="presentation" class="table table-striped clearfix">
				            	<thead>
									<tr>
					                    <th>Page</th>
					                    <th>Page No.</th>
					                    <th>Size</th>
					                    <th>Actions</th>
					                </tr>
				                
								</thead>
				                <tbody class="files">
				               
				                 </tbody>
				            </table>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<script id="template-upload" type="text/x-tmpl">
 {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error label label-danger"></strong>
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                <div class="progress-bar progress-bar-success" style="width:0%;"></div>
            </div>
        </td>
        <td  align="center"> {% if (!i && !o.options.autoUpload) { %}
            <button class="btn blue start" disabled>
                <i class="fa fa-upload"></i>
                <span>Start</span>
            </button> {% } %} {% if (!i) { %}
            <button class="btn red cancel">
                <i class="fa fa-ban"></i>
                <span>Cancel</span>
            </button> {% } %} </td>
    </tr> {% } %} </script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
 {% for (var i=0, file; file=o.files[i]; i++) { %}
 {% if (file.files) { %}
    {% for (var j=0; j < file.files.length; j++) { var file1=file.files[j]; %}
    <tr class="template-download fade">
        <td>
            <span class="preview"> {% if (file1.url) { %}
                <!-- <a href="{%=file1.url%}" title="{%=file1.name%}" download="{%=file1.name%}" data-gallery> -->
                    <img src="{%=file1.url%}" width="90px">
                <!-- </a> --> {% } %} </span>
        </td>
        <td>
            <p class="name"> {% if (file1.url) { %}
                <!-- <a href="{%=file1.url%}" title="{%=file1.name%}" download="{%=file1.name%}" {%=file1.thumbnailUrl? 'data-gallery': ''%}> -->{%=file1.name%}<!-- </a> --> {% } else { %}
                <span>{%=file1.name%}</span> {% } %} </p> {% if (file1.error) { %}
            <div>
                <span class="label label-danger">Error</span> {%=file1.error%}</div> {% } %} </td>
        <td>
            <span class="size">{%=o.formatFileSize(file1.size)%}</span>
        </td>
        <td align="center"> {% if (file1.deleteUrl) { %}
            <input type="hidden" name="id" value="{%=file1.id%}">
            <button class="btn red delete btn-sm" data-type="{%=file1.deleteType%}" data-url="{%=file1.deleteUrl%}" {% if (file1.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}' {% } %}>
                <i class="fa fa-trash-o"></i>
                <span>Delete</span>
            </button>
            <!--   <label class="padding-zero">
                      <input type="checkbox" name="delete" value="1" class="toggle"></label> --> {% } else { %}
             <button class="btn yellow cancel btn-sm">
                <i class="fa fa-ban"></i>
                <span>Cancel</span>
            </button>  {% } %} </td>
    </tr>
  {% } } else { %}
    <tr class="template-download fade">
        <td>
            <span class="preview"> {% if (file.url) { %}
                <!-- <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery> -->
                    <img src="{%=file.url%}" width="90px">
                <!-- </a> --> {% } %} </span>
        </td>
        <td>
            <p class="name"> {% if (file.url) { %}
                <!-- <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl? 'data-gallery': ''%}> -->{%=file.name%}<!-- </a> --> {% } else { %}
                <span>{%=file.name%}</span> {% } %} </p> {% if (file.error) { %}
            <div>
                <span class="label label-danger">Error</span> {%=file.error%}</div> {% } %} </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td align="center"> {% if (file.deleteUrl) { %}
            <input type="hidden" name="id" value="{%=file.id%}">
            <button class="btn red delete btn-sm" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}" {% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}' {% } %}>
                <i class="fa fa-trash-o"></i>
                <span>Delete</span>
            </button>
            <!--   <label class="padding-zero">
                      <input type="checkbox" name="delete" value="1" class="toggle"></label> --> {% } else { %}
             <button class="btn yellow cancel btn-sm">
                <i class="fa fa-ban"></i>
                <span>Cancel</span>
            </button>  {% } %} </td>
    </tr> {% } } %} </script><!-- container -->
@endsection
@section('page_script_links')
    <script src="{{ asset('plugins/sweetalert/sweetalert.min.js') }}"></script>
	  <script src="{{asset('plugins/jQuery-File-Upload/js/vendor/jquery.ui.widget.js')}}" type="text/javascript"></script>
        <script src="{{asset('plugins/jQuery-File-Upload/js/vendor/tmpl.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('plugins/jQuery-File-Upload/js/vendor/load-image.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('plugins/jQuery-File-Upload/js/vendor/canvas-to-blob.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('plugins/jQuery-File-Upload/js/jquery.iframe-transport.js')}}" type="text/javascript"></script>
        <script src="{{asset('plugins/jQuery-File-Upload/js/jquery.fileupload.js')}}" type="text/javascript"></script>
        <script src="{{asset('plugins/jQuery-File-Upload/js/jquery.fileupload-process.js')}}" type="text/javascript"></script>
        <script src="{{asset('plugins/jQuery-File-Upload/js/jquery.fileupload-image.js')}}" type="text/javascript"></script>
        <script src="{{asset('plugins/jQuery-File-Upload/js/jquery.fileupload-validate.js')}}" type="text/javascript"></script>
        <script src="{{asset('plugins/jQuery-File-Upload/js/jquery.fileupload-ui.js')}}" type="text/javascript"></script>
       
   <script src="{{ asset('js/fileinput.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/form-fileupload.js')}}" type="text/javascript"></script>

@endsection
@section('page_script')
	$('#manga_id').on('change', function(e){
        console.log(e);
        var manga_id = e.target.value;
        var url='{{ route('manga-volume-list','manga_id') }}'.replace('manga_id', manga_id);
    	$.get(url, function(data) {
		    var manga_volume_id=$('#manga_volume_id');
		    manga_volume_id.empty();
		    manga_volume_id.append('<option value="">Select Volume</option>');
		    $.each(data, function(value, display){
		         manga_volume_id.append('<option value="' + display.id + '">' + display.name + '</option>');
		    });
		    var manga_chapter_id=$('#manga_chapter_id');
		    manga_chapter_id.empty();		    
		    manga_chapter_id.append('<option value="">Select Chapter</option>');
		    getPages(0);
		   
		});
	});		
	$('#manga_volume_id').on('change', function(e){
        console.log(e);
        var manga_id = $('#manga_id').val();
        var manga_volume_id = e.target.value;
        var url='{{ route('manga-chapter-list',['manga_id'=>'manga_id','manga_volume_id'=>'manga_volume_id']) }}'.replace('manga_id', manga_id).replace('manga_volume_id', manga_volume_id);
    	$.get(url, function(data) {
		    var manga_chapter_id=$('#manga_chapter_id');
		    manga_chapter_id.empty();		    
		    manga_chapter_id.append('<option value="">Select Chapter</option>');
			$.each(data, function(value, display){
		         manga_chapter_id.append('<option value="' + display.id + '">' +display.number+" - " +display.name + '</option>');
		    });
		    getPages(0);
		});
	});	

$('#manga_chapter_id').on('change', function(e){
        console.log(e);
        var manga_chapter_id = e.target.value;
        getPages(manga_chapter_id);
	});	


  
$('#fileupload').bind('fileuploaddestroy', function (e, data) {
     swal({
         title: "Are you sure, you want to delete page?",  
         type: "warning", 
         showLoaderOnConfirm: true,
         showCancelButton: true,   
         confirmButtonColor: "#DD6B55",  
         confirmButtonText: "Yes sure!",   
         cancelButtonText: "Cancel",  
         closeOnConfirm: false, 
       }, 
       function(isConfirm){  
         if (isConfirm) {   
           $('#fileupload').fileupload('option', 'destroy').call($('#fileupload'), $.Event('destroy'), data);
          swal.close();
       }
    });
  return false;
});

function getPages(manga_chapter_id){
    var manga_id = $('#manga_id').val();
    var url='{{ route('manga-page-list',['manga_id'=>'manga_id','manga_chapter_id'=>'manga_chapter_id']) }}'.replace('manga_id', manga_id).replace('manga_chapter_id', manga_chapter_id);
	$.get(url+"?view=manga.upload.partials.addPages", function(data) {
	    var pages=$('table .files');
	    pages.empty();		    
	    pages.html(data);
	});
}
								
@endsection
