@extends('layouts.app')
@section('page_title')
Publish Comic
@endsection


@section('css')
  <!-- Sweet Alert -->
<link href="{{ asset('plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/jQuery-File-Upload/css/jquery.fileupload.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/jQuery-File-Upload/css/jquery.fileupload-ui.css')}}" rel="stylesheet" type="text/css" />
<style type="text/css">
	.in{
		opacity: 1 !important;
	}
</style>
@endsection
@section('content')
<section class="about_banner" style="background:url({{asset('images/about_banner.jpg')}}) no-repeat;">
	<div class="container">
		<div class="about_caption wow fadeInDown" data-wow-delay="0.2s" data-wow-duration="1000ms">
			<h2>Dashboard</h2> </div>
	</div>
	<div class="banner_scroll">
     <a href="#dashboard_sctn" title=""><i class="fa fa-angle-down" aria-hidden="true"></i></a>     
    </div>
</section>
<section class="dashboard_main">
	<div class="container">
		<div class="row">
			@include('layouts/partials/sidebar')
			<div class="col-sm-9 dashboard_rght">
				<h3 class="title_sctn">Mass Upload</h3>
				<div class="dashboard_side_cntnt">
					<div class="myprofile_sctn">
						<form class="edit_manga" action="{{route('mass-upload-save')}}" data-action="{{route('mass-upload-save')}}" id="fileupload" method="POST" enctype="multipart/form-data">
							 @csrf
							
							 <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
				            <div class="row fileupload-buttonbar">
				                <div class="col-lg-7">
				                    <!-- The fileinput-button span is used to style the file input field as button -->
				                    <span class="btn btn-success fileinput-button">
				                        <i class="fa fa-plus"></i>
				                        <span> Add files... </span>
				                        <input type="file" name="files[]" multiple=""> </span>
				                    <button type="submit" class="btn blue start">
				                        <i class="fa fa-upload"></i>
				                        <span> Start upload </span>
				                    </button>
				                    <button type="reset" class="btn btn-warning cancel">
				                        <i class="fa fa-ban-circle"></i>
				                        <span> Cancel upload </span>
				                    </button>
				                   <!--  <button type="button" class="btn red delete">
				                        <i class="fa fa-trash"></i>
				                        <span> Delete </span>
				                    </button>
				                      <label class="padding-zero">
				                        <input type="checkbox" class="toggle"></label> -->
				                    <!-- The global file processing state -->
				                    <span class="fileupload-process"> </span>
				                </div>
				                <!-- The global progress information -->
				                <div class="col-lg-5 fileupload-progress fade">
				                    <!-- The global progress bar -->
				                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
				                        <div class="progress-bar progress-bar-success" style="width:0%;"> </div>
				                    </div>
				                    <!-- The extended global progress information -->
				                    <div class="progress-extended"> &nbsp; </div>
				                </div>
				            </div>
				            <div id="dropzone" class="dropzone-file-area">
				            <div class="col-md-12 col-sm-12">
				                <i class="fa fa-cloud-upload "></i>
				            <h3 class="sbold">Drop files here or click to Add files</h3>
				            <p>Any .PNG, .JPG or drag pages here</p></div>
				            <div class="clearfix"></div>
				            </div>
				            <!-- The table listing the files available for upload/download -->
				            <table role="presentation" class="table table-striped clearfix">
				            	<thead>
									<tr>
					                    <th>Page</th>
					                    <th>Page Name</th>
					                    <th>Size</th>
					                    <th>Actions</th>
					                </tr>
				                
								</thead>
				                <tbody class="files">
				               
				                 </tbody>
				            </table>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<script id="template-upload" type="text/x-tmpl">
 {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error label label-danger"></strong>
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                <div class="progress-bar progress-bar-success" style="width:0%;"></div>
            </div>
        </td>
        <td  align="center"> {% if (!i && !o.options.autoUpload) { %}
            <button class="btn blue start" disabled>
                <i class="fa fa-upload"></i>
                <span>Start</span>
            </button> {% } %} {% if (!i) { %}
            <button class="btn red cancel">
                <i class="fa fa-ban"></i>
                <span>Cancel</span>
            </button> {% } %} </td>
    </tr> {% } %} </script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
 {% for (var i=0, file; file=o.files[i]; i++) { %}
 {% if (file.files) { %}
    {% for (var j=0; j < file.files.length; j++) { var file1=file.files[j]; %}
    <tr class="template-download fade">
        <td>
            <span class="preview"> {% if (file1.url) { %}
                <!-- <a href="{%=file1.url%}" title="{%=file1.name%}" download="{%=file1.name%}" data-gallery> -->
                    <img src="{%=file1.url%}" width="90px">
                <!-- </a> --> {% } %} </span>
        </td>
        <td>
            <p class="name"> {% if (file1.url) { %}
                <!-- <a href="{%=file1.url%}" title="{%=file1.name%}" download="{%=file1.name%}" {%=file1.thumbnailUrl? 'data-gallery': ''%}> -->{%=file1.name%}<!-- </a> --> {% } else { %}
                <span>{%=file1.name%}</span> {% } %} </p> {% if (file1.error) { %}
            <div>
                <span class="label label-danger">Error</span> {%=file1.error%}</div> {% } %} </td>
        <td>
            <span class="size">{%=o.formatFileSize(file1.size)%}</span>
        </td>
        <td align="center"> {% if (file1.deleteUrl) { %}
            <input type="hidden" name="id" value="{%=file1.id%}">
            <button class="btn red delete btn-sm" data-type="{%=file1.deleteType%}" data-url="{%=file1.deleteUrl%}" {% if (file1.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}' {% } %}>
                <i class="fa fa-trash-o"></i>
                <span>Delete</span>
            </button>
            <!--   <label class="padding-zero">
                      <input type="checkbox" name="delete" value="1" class="toggle"></label> --> {% } else { %}
             <button class="btn yellow cancel btn-sm">
                <i class="fa fa-ban"></i>
                <span>Cancel</span>
            </button>  {% } %} </td>
    </tr>
  {% } } else { %}
    <tr class="template-download fade">
        <td>
            <span class="preview"> {% if (file.url) { %}
                <!-- <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery> -->
                    <img src="{%=file.url%}" width="90px">
                <!-- </a> --> {% } %} </span>
        </td>
        <td>
            <p class="name"> {% if (file.url) { %}
                <!-- <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl? 'data-gallery': ''%}> -->{%=file.name%}<!-- </a> --> {% } else { %}
                <span>{%=file.name%}</span> {% } %} </p> {% if (file.error) { %}
            <div>
                <span class="label label-danger">Error</span> {%=file.error%}</div> {% } %} </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td align="center"> {% if (file.deleteUrl) { %}
            <input type="hidden" name="id" value="{%=file.id%}">
            <button class="btn red delete btn-sm" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}" {% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}' {% } %}>
                <i class="fa fa-trash-o"></i>
                <span>Delete</span>
            </button>
            <!--   <label class="padding-zero">
                      <input type="checkbox" name="delete" value="1" class="toggle"></label> --> {% } else { %}
             <button class="btn yellow cancel btn-sm">
                <i class="fa fa-ban"></i>
                <span>Cancel</span>
            </button>  {% } %} </td>
    </tr> {% } } %} </script><!-- container -->
@endsection
@section('page_script_links')
    <script src="{{ asset('plugins/sweetalert/sweetalert.min.js') }}"></script>
	  <script src="{{asset('plugins/jQuery-File-Upload/js/vendor/jquery.ui.widget.js')}}" type="text/javascript"></script>
        <script src="{{asset('plugins/jQuery-File-Upload/js/vendor/tmpl.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('plugins/jQuery-File-Upload/js/vendor/load-image.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('plugins/jQuery-File-Upload/js/vendor/canvas-to-blob.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('plugins/jQuery-File-Upload/js/jquery.iframe-transport.js')}}" type="text/javascript"></script>
        <script src="{{asset('plugins/jQuery-File-Upload/js/jquery.fileupload.js')}}" type="text/javascript"></script>
        <script src="{{asset('plugins/jQuery-File-Upload/js/jquery.fileupload-process.js')}}" type="text/javascript"></script>
        <script src="{{asset('plugins/jQuery-File-Upload/js/jquery.fileupload-image.js')}}" type="text/javascript"></script>
        <script src="{{asset('plugins/jQuery-File-Upload/js/jquery.fileupload-validate.js')}}" type="text/javascript"></script>
        <script src="{{asset('plugins/jQuery-File-Upload/js/jquery.fileupload-ui.js')}}" type="text/javascript"></script>
       
   <script src="{{ asset('js/fileinput.min.js') }}" type="text/javascript"></script>

@endsection
@section('page_script')
  
$('#fileupload').bind('fileuploaddestroy', function (e, data) {
     swal({
         title: "Are you sure, you want to delete page?",  
         type: "warning", 
         showLoaderOnConfirm: true,
         showCancelButton: true,   
         confirmButtonColor: "#DD6B55",  
         confirmButtonText: "Yes sure!",   
         cancelButtonText: "Cancel",  
         closeOnConfirm: false, 
       }, 
       function(isConfirm){  
         if (isConfirm) {   
           $('#fileupload').fileupload('option', 'destroy').call($('#fileupload'), $.Event('destroy'), data);
          swal.close();
       }
    });
  return false;
});


var FormFileUpload = function () {
    return {
        //main function to initiate the module
        init: function () {

             // Initialize the jQuery File Upload widget:
            $('#fileupload').fileupload({
                sequentialUploads:true,
                disableImageResize: false,
                dropZone: $('#dropzone'),
                maxNumberOfFiles:9999,
                autoUpload: false,
                disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
                maxFileSize: 5000000,
                acceptFileTypes: /^image\/(png|jpg|jpeg)$/i,
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},                
                // change: function (e, data) {
                //     $.each(data.files, function (index, file) {
                //         var filewidth=0;
                //         var fileheight=0;
                //         var filetypes=/^image\/(png|svg\+xml)$/i;
                //         if(filetypes.test(file.type) ||
                //                 filetypes.test(file.name))
                //         {
                //             var reader = new FileReader();

                //             reader.onload = function (e) {
                //                 var image = new Image();
                //                 image.src = reader.result;
                //                 image.onload = function() {
                //                     filewidth=image.width;
                //                     fileheight=image.height;
                //                     if(filewidth < 512 || fileheight < 512){
                //                          file.error = settings.i18n('imageSize');
                //                     }
                //                 };
                //             }

                //             reader.readAsDataURL(file);
                //         }

                //     });
                // }
            });
            // Enable iframe cross-domain access via redirect option:
            $('#fileupload').fileupload(
                'option',
                'redirect',
                window.location.href.replace(
                    /\/[^\/]*$/,
                    '/cors/result.html?%s'
                )
            );

            // Upload server status check for browsers with CORS support:
            if ($.support.cors) {
                $.ajax({
                    type: 'HEAD'
                }).fail(function () {
                    $('<div class="alert alert-danger"/>')
                        .text('Upload server currently unavailable - ' +
                                new Date())
                        .appendTo('#fileupload');
                });
            }

          
        var counter = 0;
        
            var number_of_files = 0; // or sessionStorage.getItem('number_of_files')
            $('#fileupload')
                .bind('fileuploadadd', function (e, data) {
                    $.each(data.files, function (index, file) {
                        console.log('Added file in queue: ' + file.name);
                        number_of_files = number_of_files + 1;
                        sessionStorage.setItem('number_of_files', number_of_files);
                    });
                })
                .bind('fileuploadprocessdone', function (e, data) {
                    console.log(counter + " / " + number_of_files);
                });
        }

    };

}();

jQuery(document).ready(function() {
    FormFileUpload.init();
});


					
@endsection
