@extends('layouts.app')
@section('page_title')
Request Copy
@endsection

@section('content')
<section class="about_banner" style="background:url({{asset('images/about_banner.jpg')}}) no-repeat;">
		<div class="container">
			<div class="about_caption wow fadeInDown" data-wow-delay="0.2s" data-wow-duration="1000ms">
				<h2>Dashboard</h2> </div>
		</div>
		<div class="banner_scroll"> <a href="#dashboard_sctn" title=""><i class="fa fa-angle-down" aria-hidden="true"></i></a> </div>
	</section>
	<!--about_banner end -->
	<section class="dashboard_main">
		<div class="container">
			<div class="row">
				@include('layouts/partials/sidebar')
				<div class="col-sm-9 dashboard_rght">
					<div class="dashboard_side_cntnt">
						<div class="myprofile_sctn">
							<div class="table-responsive">
								<table class="tbl_srs">
									<thead>
										<tr>
											<th style="width:214px;">
												<label>Cover Image</label>
											</th>
											<th style="width:200px;">
												<label>Title</label>
											</th>
											<th style="width:234px;">
												<label>Artist name</label>
											</th>
											<th style="width:190px;">
												<label>Volumes</label>
											</th>
											<th style="width:148px;">
												<label>Ordered Date</label>
											</th>
										</tr>
									</thead>
									<tbody>
										@forelse($mangaRequests as $resquest)
										<tr>
											<td>
												<figure><img src="{{empty($resquest->manga->cover)?'images/logo.png':asset('storage/covers/').'/'.$resquest->manga->cover}}" alt="{{$resquest->manga->title}}"></figure>
											</td>
											<td>
												<p>{{$resquest->manga->title}}</p>
											</td>
											<td>
												<p>{{$resquest->manga->artist}}</p>
											</td>
											<td>
												<p>Volume {{$resquest->volume->name}}</p>
											</td>
											<td>
												<p>{{date('F d, Y',strtotime($resquest->created_at))}}</p>
											</td>
										</tr>
										@empty
										<tr>
											<td colspan="5" class="warning">
												No Data found
											</td>
										</tr>
										@endforelse
									</tbody>
								</table>
							</div>
							<div class="tbl_pgination">{!! $mangaRequests->links() !!}</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
