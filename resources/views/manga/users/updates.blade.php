@extends('layouts.app')
@section('page_title')
Series Updates
@endsection

@section('content')
<section class="about_banner" style="background:url({{asset('images/about_banner.jpg')}}) no-repeat;">
		<div class="container">
			<div class="about_caption wow fadeInDown" data-wow-delay="0.2s" data-wow-duration="1000ms">
				<h2>Dashboard</h2> </div>
		</div>
		<div class="banner_scroll"> <a href="#dashboard_sctn" title=""><i class="fa fa-angle-down" aria-hidden="true"></i></a> </div>
	</section>
	<!--about_banner end -->
	<section class="dashboard_main">
		<div class="container">
			<div class="row">
				@include('layouts/partials/sidebar')
				<div class="col-sm-9 dashboard_rght">
					<div class="dashboard_side_cntnt">
						<div class="myprofile_sctn">
							<div class="table-responsive">
								<table class="tbl_srs">
									<thead>
										<tr>
											<th style="width:247px;">
												<label>Cover Image</label>
											</th>
											<th style="width:293px;">
												<label>Title</label>
											</th>
											<th style="width:293px;">
												<label>Chapter Number</label>
											</th>
											<th style="width:156px;">
												<label>Released Date</label>
											</th>
										</tr>
									</thead>
									<tbody>
										@forelse($seriesUpdates as $update)
										<tr>
											<td>
												<figure><img src="{{empty($update->manga()->cover)?'images/logo.png':asset('storage/covers/').'/'.$update->manga()->cover}}" alt="captain_img">
												@if(!$update->is_seen)	<a data-id="{{$update->id}}" href="{{route('manga-reading',$update->manga()->slug)}}?ch={{$update->chapter()->id}}" title="" class="unread"></a>@endif
												</figure>
											</td>
											<td>
												<p>{{$update->manga()->title}}</p>
											</td>
											<td>
												<p>{{$update->chapter()->number .' - '.$update->chapter()->name}}</p>
											</td>
											<td>
												<p><small class="timeAgo" data="{{ date('c', strtotime( $update->created_at )) }}">20 min ago</small></p>
											</td>
										</tr>										
										@empty
										<tr><td colspan="4" class="warning">No Data Found.</td></tr>
										@endforelse
									</tbody>
								</table>
							</div>
							<div class="tbl_pgination">{!! $seriesUpdates->links() !!}</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
@endsection
@section('page_script')

$('.unread').click(function (e) {
	e.preventDefault();
	var url=$(this).attr('href');
   	var id=$(this).data('id');
    $.ajax({
        type: "POST",
        url: "{{route('mark-notification')}}",
        data: 'id='+id,
        success: function (response) {
            window.location = url;
        },
        failure: function (response) {           
            window.location = url;
       }
    });
});
@endsection