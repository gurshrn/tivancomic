@extends('layouts.app')
@section('page_title')
Directory
@endsection
@section('dfp-ad', true)
@section('page_script')
var width = $(window).width();
	//Directory_Desktop_728x90_BTF:
	  googletag.cmd.push(function() {
		googletag.defineSlot('/21770139633/Directory_Desktop_970x250_ATF', [[728, 90], [728, 20], [970, 90]], 'div-gpt-ad-1552576358809-0').addService(googletag.pubads());
		googletag.pubads().enableSingleRequest();
		googletag.enableServices();
  });


	//Directory_Mobile_300x250_Right:
  googletag.cmd.push(function() {
    googletag.defineSlot('/21770139633/Directory_Desktop_300x250_Right', [[300, 600], [250, 250], [300, 250], [336, 280]], 'div-gpt-ad-1552576507555-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });

  googletag.cmd.push(function() {
    googletag.defineSlot('/21770139633/Directory_Mobile_320x100_ATF', [[180, 150], [300, 250], [320, 50], [320, 100]], 'div-gpt-ad-1552576386056-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });
  googletag.cmd.push(function() { 
  if(width >= 768){
		googletag.display('div-gpt-ad-1552576358809-0'); 
	} 
	if(width >= 320 && width <= 767){	
		googletag.display('div-gpt-ad-1552576386056-0'); 
	}
	googletag.display('div-gpt-ad-1552576507555-0'); 
  });

@endsection
@section('content')
@include('widgets/popularMangaBanner')
<style>
@media only screen and (min-width: 767px) {
        /* styles for browsers larger than 960px; */
				#div-gpt-ad-1552576386056-0{
					display:none!important;
				}
}
@media only screen and (min-width: 320px) and  (max-width: 768px){
        /* styles for browsers larger than 960px; */
				#div-gpt-ad-1552576358809-0{
					display:none!important;
				}
}
</style>
	<section class="chapter_sec directory_page">
		<div class="container">
			<div class="row">	
				<div class="col-sm-8 chapter_sec_lft">
					<figure class="ads"><!-- Home Page Bottom Ad -->
						<!-- /21770139633/Directory_Desktop_728x90_BTF -->
						<div id='div-gpt-ad-1552576358809-0'>
						</div>
						<div id='div-gpt-ad-1552576386056-0'>
						</div>
					</figure>
					<div class="chapter_sec_main">
						<div class="cstm_heading">
							<h3>Comics Directory</h3></div>
						<div class="slct_bx">
							<label>Sort By</label>
							<div class="slct">
								<select class="sort filters">
									<option value="views" selected>Views</option>
									<option value="review">Reviews</option>
									<option value="date" >Date</option>
									<option value="title">Title</option>
								</select>
							</div>
						</div>
						<div class="chapter_sc">
							@include('manga/partials/list')
						</div>
					</div>
				</div>
				<div class="col-sm-4 chapter_sec_rght">
					<div class="drctry_ad ads">
						<figure><!-- comic page side bar -->
							<!-- /21770139633/Directory_Mobile_300x250_Right -->
							<div id='div-gpt-ad-1552576507555-0'>
							</div>
						</figure>
					</div>
					@include('manga.partials.filters')
					
				</div>
			</div>
		</div>
	</section>
@include('widgets/latestBlog')
@endsection
