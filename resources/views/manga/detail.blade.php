@extends('layouts.app')
@section('page_title', $manga->title)
@section('meta_title',$manga->title)
@section('dfp-ad', true)
@section('page_script')
var width = $(window).width();

	//Directory_Desktop_728x90_BTF:
	  googletag.cmd.push(function() {
	    googletag.defineSlot('/21770139633/Directory_Desktop_728x90_BTF', [[728, 20], [728, 90]], 'div-gpt-ad-1552576578929-0').addService(googletag.pubads());
			googletag.pubads().enableSingleRequest();
			googletag.enableServices();
  });
 
  //Directory_Mobile_320x100_BTF:
  googletag.cmd.push(function() {  
    googletag.defineSlot('/21770139633/Directory_Mobile_320x100_BTF', [[320, 50], [180, 150], [320, 100], [300, 250]], 'div-gpt-ad-1552576613450-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });
  
  
	//Directory_DESKTOP_300x250_Right:
	  googletag.cmd.push(function() {  
	    googletag.defineSlot('/21770139633/Directory_Desktop_300x250_Right', [[300, 600], [250, 250], [300, 250], [336, 280]], 'div-gpt-ad-1552576507555-0').addService(googletag.pubads());
		googletag.pubads().enableSingleRequest();
		googletag.enableServices();
  });

	//Directory_Mobile_300x250_Right:
	  googletag.cmd.push(function() {
       googletag.defineSlot('/21770139633/Directory_Mobile_300x250_Right', [[320, 100], [300, 250], [320, 50]], 'div-gpt-ad-1552576546245-0').addService(googletag.pubads());
		googletag.pubads().enableSingleRequest();
		googletag.enableServices();
  });

	//Directory_Desktop_970x250_ATF:
    googletag.cmd.push(function() { 
		googletag.defineSlot('/21770139633/Directory_Desktop_970x250_ATF', [[728, 90], [728, 20], [970, 90]], 'div-gpt-ad-1552576358809-0').addService(googletag.pubads());
		googletag.pubads().enableSingleRequest();
		googletag.enableServices();
	});
  	//Directory_Mobile_320x100_ATF:
    googletag.cmd.push(function() {
		googletag.defineSlot('/21770139633/Directory_Mobile_320x100_ATF', [[180, 150], [300, 250], [320, 50], [320, 100]], 'div-gpt-ad-1552576386056-0').addService(googletag.pubads());
		googletag.pubads().enableSingleRequest();
		googletag.enableServices();
	});



  googletag.cmd.push(function() {
	  if(width >= 768){
		googletag.display('div-gpt-ad-1552576358809-0'); 
		googletag.display('div-gpt-ad-1552576578929-0');
		googletag.display('div-gpt-ad-1552576507555-0');
	  } 
	  if(width >= 320 && width <= 767){	
		googletag.display('div-gpt-ad-1552576386056-0');
		googletag.display('div-gpt-ad-1552576613450-0');
		//googletag.display('div-gpt-ad-1552576546245-0');
	  }
  });

@endsection
@section('content')
@include('widgets/popularMangaBanner')
<style>
@media only screen and (min-width: 767px) {
        /* styles for browsers larger than 960px; */
				#div-gpt-ad-1552576386056-0,#div-gpt-ad-1552576613450-0,#div-gpt-ad-1552576546245-0{
					display:none!important;
				}
}
@media only screen and (min-width: 320px) and  (max-width: 768px){
        /* styles for browsers larger than 960px; */
				#div-gpt-ad-1552576358809-0,#div-gpt-ad-1552576578929-0,#div-gpt-ad-1552576507555-0{
					display:none!important;
				}
}
</style>
	<section class="chapter_sec directory_page artist_detail">
		<div class="container">
			<div class="row">
				<div class="col-sm-8 chapter_sec_lft">
					<figure class="ads"><!-- Home Page Bottom Ad -->
						<!-- /21770139633/Directory_Desktop_728x90_BTF -->
						<div id='div-gpt-ad-1552576358809-0'>
						</div>
						<!-- /21770139633/Directory_Mobile_320x100_BTF -->
						<div id='div-gpt-ad-1552576386056-0'>
						</div>

					</figure>
					<div class="chapter_sec_main">
						<div class="artist_breadcrumb">
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="{{route('manga-directory')}}">Comics Directory</a></li>
								<li class="breadcrumb-item active" aria-current="page">{{$manga->title}}</li>
							</ol>
						</div>
						<div class="artist_profile manage_profile">
							<figure><img src="{{empty($manga->cover)?'images/logo.png':asset('storage/covers/').'/'.$manga->cover}}" alt="tate"><a href="#chptr_list" title="" class="view_btn">Chapter List</a></figure>
							<div class="artist_cntnt">
								<h3>{{$manga->title}}</h3>
								<div class="artist_form">
									<label>Artist Name:</label>
									<p><a href="{{route('artist-profile',($manga->artist_id>0)?$manga->artists()->slug:$manga->user()->slug)}}" title="">{{$manga->artist}}</a></p>
								</div>
								<div class="artist_form">
									<label>Genres:</label>
									<p>@foreach($manga->categories as $category){{$category->name}}<br>@endforeach</p>
								</div>
								<div class="artist_form">
									<label>Status:</label>
									<p>{{$manga->release_status}}</p>
								</div>
								<div class="artist_form">
									<label>Views:</label>
									<p>{{count($manga->mangaViews)}}</p>
								</div>
								<div class="artist_form">
									<label>Rate:</label>
									<p class="rating_sctn">
										<!-- <span class="stars">{{mangaRatings($manga->id)}}</span> -->
										@php $rating=mangaRatings($manga->id); @endphp
										@for($i=1;$i<=5;$i++)
										<i class="star fa fa-star" aria-hidden="true">
											<i class="star star-over fa fa-star" style="width: {{$i<=$rating?'100':($i-$rating)/100 }}%"></i>
										</i>
										@endfor
										<button class="rate" type="submit" data-toggle="modal" data-target="#ratings">Rating Us</button>
									</p>
									
								</div>
								<form action="{{route(checkFollowManga($manga->id)?'unfollow-manga':'follow-manga')}}" method="post" class="ajax-post">
									<input type="hidden" name="manga_id" value="{{$manga->id}}">
									<button type="submit" class="view_btn"><i class="fa {{checkFollowManga($manga->id)?'fa-thumbs-down':'fa-thumbs-up'}}" aria-hidden="true"></i>{{checkFollowManga($manga->id)?'Unfollow':'Follow'}}</button>
								</form>
								
								<div class="artist_summary">
									<h5>Summary</h5>
									<p>{!! $manga->description !!}</p>
								</div>
							</div>
						</div>
						<!-- artist_profile end -->
					</div>
					<!--chapter_sec_main end -->
					<div class="ad_sctn_cstm ads">
						<!-- /21770139633/Directory_Desktop_728x90_BTF -->
						<div id='div-gpt-ad-1552576578929-0'>
						</div>
						<!-- /21770139633/Directory_Mobile_320x100_BTF -->
						<div id='div-gpt-ad-1552576613450-0'>
						</div>		
					</div>
					@if($volumes)
					<div class="chapter_sec_main manage_chapter" id="chptr_list">
						<h6>{{$manga->title}}: Chapters</h6>
						<div class="manage_chptr_list">
							@foreach($volumes as $vol)
							<div class="mangae_lst_bx_main">
								<div class="mangae_lst_bx1">
									@foreach($vol->chapter_all() as $chapter)
									<div class="mangae_lst_bx">
										<h6>Chapter {{$chapter->number}}</h6>
										<h6><small class="timeAgo" data="{{ date('c', strtotime( $chapter->created_at )) }}">20 min ago</small></h6>
										<p><a href="{{route('manga-reading',$manga->slug)}}?ch={{$chapter->id}}" title="">Read</a></p>
									</div>
									@endforeach
								</div>
								<div class="rst">
									<h6><small>Vol. {{$vol->name}}</small></h6>
									<p><a href="#" data-toggle="modal" data-target="#order_modal" title="">Order Physical Copy</a></p>
								</div>
							</div>
							@endforeach
						</div>
						<div class="pagination_sctn">{!! $volumes->links('layouts.partials.pagination') !!}</div>
					</div>
					@endif
				</div>
				<div class="col-sm-4 chapter_sec_rght">
					<div class="drctry_ad ads">
						<figure><!-- comic page side bar -->
							<!-- /21770139633/Directory_Mobile_300x250_Right -->
							<!-- Responsive Chapter Sidebar -->
							<!-- /21770139633/Directory_Desktop_300x250_Right: -->
						<div id='div-gpt-ad-1552576507555-0'>
						</div>
						<!-- /21770139633/Directory_Mobile_300x250_Right: -->
						<div id='div-gpt-ad-1552576546245-0'>
						</div>		
										
						</figure>
					</div>
					@include('widgets/newMangas')
				</div>
			</div>
		</div>
	</section>

  <!-- Modal -->

  <div class="modal fade rateing" id="ratings" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
					<h1 class="modal-title" id="exampleModalLabel">Rating Us</h1>
					
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
               <div class="modal-body">
				   <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
				   <form class="ajax-post" method="post" action="{{route('rate-manga')}}">
						<input type="hidden" name="manga_id" value="{{$manga->id}}">
						<div class="form-group">
							<div class="col-md-12">
								<div class="star-rating">
								<fieldset>
									<input type="radio" id="star5" name="rating" value="5" /><label for="star5" title="Outstanding">5 stars</label>
									<input type="radio" id="star4" name="rating" value="4" /><label for="star4" title="Very Good">4 stars</label>
									<input type="radio" id="star3" name="rating" value="3" /><label for="star3" title="Good">3 stars</label>
									<input type="radio" id="star2" name="rating" value="2" /><label for="star2" title="Poor">2 stars</label>
									<input type="radio" id="star1" name="rating" value="1" /><label for="star1" title="Very Poor">1 star</label>
								</fieldset>
							</div>
						</div>
						</div>
					</form>
                </div>
            </div>
        </div>
	</div>
@include('widgets/latestBlog')
@include('widgets/requestManga')
@endsection
@section('page_script')
$('input[type=radio]').on('change', function() {
    $(this).closest("form").submit();
});
@endsection

