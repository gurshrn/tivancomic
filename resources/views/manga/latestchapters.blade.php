@extends('layouts.app')
@section('page_title')
Latest Updates
@endsection
@section('dfp-ad', true)
@section('page_script')
 var width = $(window).width();
	//Directory_Desktop_728x90_BTF:
	  googletag.cmd.push(function() {	
		googletag.defineSlot('/21770139633/Directory_Desktop_970x250_ATF', [[728, 90], [728, 20], [970, 90]], 'div-gpt-ad-1552576358809-0').addService(googletag.pubads());
		googletag.pubads().enableSingleRequest();
		googletag.enableServices();
	});

	//Directory_Mobile_300x250_Right:
	  googletag.cmd.push(function() {	
		googletag.defineSlot('/21770139633/Directory_Desktop_300x250_Right', [[300, 600], [250, 250], [300, 250], [336, 280]], 'div-gpt-ad-1552576507555-0').addService(googletag.pubads());
		googletag.pubads().enableSingleRequest();
		googletag.enableServices();
	});
	googletag.cmd.push(function() {
       googletag.defineSlot('/21770139633/Directory_Mobile_320x100_ATF', [[180, 150], [300, 250], [320, 50], [320, 100]], 'div-gpt-ad-1552576386056-0').addService(googletag.pubads());
		googletag.pubads().enableSingleRequest();
		googletag.enableServices();
  });
  googletag.cmd.push(function() { 
  if(width >= 768){
		googletag.display('div-gpt-ad-1552576358809-0'); 
	} 
	if(width >= 320 && width <= 767){	
		googletag.display('div-gpt-ad-1552576386056-0'); 
	}
	googletag.display('div-gpt-ad-1552576507555-0'); 
  });

@endsection
@section('content')
@include('widgets/popularMangaBanner')
<style>
@media only screen and (min-width: 767px) {
        /* styles for browsers larger than 960px; */
				#div-gpt-ad-1552576386056-0{
					display:none!important;
				}
}
@media only screen and (min-width: 320px) and  (max-width: 768px){
        /* styles for browsers larger than 960px; */
				#div-gpt-ad-1552576358809-0{
					display:none!important;
				}
}
</style>
	<section class="chapter_sec directory_page">
		<div class="container">
			<div class="row">
				<div class="col-sm-8 chapter_sec_lft">
					<figure class="ads"><!-- Home Page Bottom Ad -->
						<!-- Responsive Chapter Bar -->
						<!-- /21770139633/Directory_Desktop_728x90_BTF -->
						<div id='div-gpt-ad-1552576358809-0'>
						</div>
						<div id='div-gpt-ad-1552576386056-0'>
						</div>
					</figure>
					<div class="chapter_sec_main">
						<div class="cstm_heading">
							<h3>Latest Updates</h3></div>
						
						<div class="chapter_sc">
							@forelse($mangas as $manga)
							<div class="chapter_bx">
								<figure><a href="{{route('cms_manga_detail',$manga->slug)}}" title="{{$manga->title}}"><img src="{{empty($manga->cover)?'images/logo.png':asset('storage/covers/').'/'.$manga->cover}}" alt="{{$manga->title}}"></a></figure>
								<div class="chapter_bx_rght">
									<div class="chp_head">
										<h6><a href="{{route('cms_manga_detail',$manga->slug)}}" title="{{$manga->title}}">{{$manga->title}}</a></h6><small class="timeAgo" data="{{ date('c', strtotime( $manga->created )) }}">20 min ago</small></div>
									<ul>
										@php $date=date('Y-m-d'); $i=0;
											$man=\App\Models\Manga::find($manga->manga_id);
										@endphp
										@foreach($man->latestchapters as $chapter)
											@if($i==0)
												@php $date=date('Y-m-d',strtotime($chapter->latestpage->created_at)); $i++; @endphp
											@elseif($date!=date('Y-m-d',strtotime($chapter->latestpage->created_at)))
												@php $i=0; break; @endphp
											@endif
											@if($i < 3)
												<li><a href="{{route('manga-reading',$manga->slug)}}?ch={{$chapter->id}}">Chapter {{$chapter->number}}</a></li> <!--  .': '.$chapter->name -->
											@elseif($i==3)
												</ul>
											@endif
											@php $i++; @endphp											
										@endforeach
										
									@if($i-3>0)
									<p><small>{{$i-3}} more chapters...</small></p>
									@elseif($i-3 <= 0)
										</ul>
									@endif
								</div>
							</div>
							@empty

							<!-- chapter_bx end -->
							@endforelse
							<div class="pagination_sctn">{!! $mangas->appends($_GET)->links('layouts.partials.pagination') !!}</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4 chapter_sec_rght">
					<div class="drctry_ad ads">
						<figure><!-- comic page side bar -->
							<!-- /21770139633/Directory_Mobile_300x250_Right -->
							<div id='div-gpt-ad-1552576507555-0'>
							</div>
						</figure>
					</div>
					<div class="populars">
					@include('widgets/mostPopular')</div>
				</div>
			</div>
		</div>
	</section>
@include('widgets/latestBlog')
@endsection
