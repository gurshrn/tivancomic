@extends('layouts.app')
@section('page_title')
Series
@endsection

@section('content')
<section class="about_banner" style="background:url({{asset('images/about_banner.jpg')}}) no-repeat;">
		<div class="container">
			<div class="about_caption wow fadeInDown" data-wow-delay="0.2s" data-wow-duration="1000ms">
				<h2>Dashboard</h2> </div>
		</div>
		<div class="banner_scroll"> <a href="#dashboard_sctn" title=""><i class="fa fa-angle-down" aria-hidden="true"></i></a> </div>
	</section>
	<!--about_banner end -->
	<section class="dashboard_main">
		<div class="container">
			<div class="row">
				@include('layouts/partials/sidebar')
				<div class="col-sm-9 dashboard_rght">
						<div class="dashboard_side_cntnt">
						<div class="myprofile_sctn">
							<div class="series_btn"> <a href="{{route('upload-manga')}}" title="" class="view_btn">Upload New Comic</a></div>
							<div class="table-responsive">
							<table class="tbl_srs">
								<thead>
									<tr>
										<th style="width:160px;">
											<label>Cover Image</label>
										</th>
										<th style="width:160px;">
											<label>Title</label>
										</th>
										<th style="width:243px;">
											<label>Artist</label>
										</th>
										<th style="width:142px;">
											<label>No. of Volumes</label>
										</th>
										<th style="width:136px;">
											<label>No. of Chapters</label>
										</th>
										<th style="width:157px;">
											<label>Action</label>
										</th>
									</tr>
								</thead>
								<tbody>
									@forelse($mangas as $manga)
									<tr>
										<td>
											<figure><img src="{{empty($manga->cover)?'images/logo.png':asset('storage/covers/').'/'.$manga->cover}}" alt="{{$manga->title}}"></figure>
										</td>
										<td>
											<p>{{$manga->title}}
												@if($manga->status!='Published')<br><small>Status: {{$manga->status}}</small>@endif</p>
										</td>
										<td>
											<p>{{$manga->artist}}</p>
										</td>
										<td>
											<p>{{count($manga->volumes)}}</p>
										</td>
										<td>
											<p>{{count($manga->chapters)}}</p>
										</td>
										<td>@if($manga->status=='Published' && $manga->author())<a href="{{route('cms_manga_detail',$manga->slug)}}" title=""><i class="fa fa-eye" aria-hidden="true"></i></a>
											@endif
											<a href="{{route('upload-manga',$manga->slug)}}" title="Edit"> <i class="fa fa-pencil" aria-hidden="true"></i></a>
											<a href="{{route('delete-manga',$manga->slug)}}" title="Delete"> <i class="fa fa-times" aria-hidden="true"></i></a>
										</td>
									</tr>
										@empty
										<tr>
											<td colspan="6" class="warning">
												No Data found
											</td>
										</tr>
										@endforelse
									</tbody>
								</table>
							</div>
							<div class="tbl_pgination">{!! $mangas->links() !!}</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
