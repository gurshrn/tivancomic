@extends('layouts.app')
@section('page_title')
Request Detail
@endsection

@section('content')
<section class="about_banner" style="background:url({{asset('images/about_banner.jpg')}}) no-repeat;">
		<div class="container">
			<div class="about_caption wow fadeInDown" data-wow-delay="0.2s" data-wow-duration="1000ms">
				<h2>Dashboard</h2> </div>
		</div>
		<div class="banner_scroll"> <a href="#dashboard_sctn" title=""><i class="fa fa-angle-down" aria-hidden="true"></i></a> </div>
	</section>
	<!--about_banner end -->
	<section class="dashboard_main">
		<div class="container">
			<div class="row">
				@include('layouts/partials/sidebar')
				<div class="col-sm-9 dashboard_rght">
					<h3 class="title_sctn">User Detail</h3>
					<div class="dashboard_side_cntnt">
						<div class="myprofile_sctn">
							<div class="table-responsive">
								<table class="tbl_srs">
									<thead>
										<tr>
											<th style="width:401px;">
												<label>Name</label>
											</th>
											<th style="width:475px;">
												<label>Email</label>
											</th>
											<th style="width:100px;">
												<label>Message</label>
											</th>
										</tr>
									</thead>
									<tbody>
										@forelse($mangaRequests as $request)
										<tr>
											<td>
												<p>{{$request->user()->name}}</span>
												</p>
											</td>
											<td>
												<p>{{$request->user()->email}}</p>
											</td>
											<td><a href="#" title="" class="tbl_view_dtl" data-toggle="modal" data-request_id="{{$request->id}}" data-message="{{$request->message}}"  data-address="{{$request->address}}" data-target="#req_modal">Read</a> </td>
										</tr>
										@empty
										<tr>
											<td colspan="3" class="warning">
												No Data found
											</td>
										</tr>
										@endforelse
									</tbody>
								</table>
							</div>
							<div class="tbl_pgination">{!! $mangaRequests->links() !!}</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="modal fade" id="req_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title" id="exampleModalLabel">Request Detail</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
				</div>
				<div class="modal-body">
					<p><strong>Address: </strong><span id="address"></span></p>
					<p><strong>Message: </strong><span id="message"></span></p>
					
				</div>
			</div>
		</div>
	</div>
@endsection
@section('page_script')
	var $modal = $('#req_modal');

// Show loader & then get content when modal is shown
$modal.on('show.bs.modal', function(e) {
  $('#message').text($(e.relatedTarget).data('message'));
  $('#address').text($(e.relatedTarget).data('address'));       
});						
@endsection