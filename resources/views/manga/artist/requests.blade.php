@extends('layouts.app')
@section('page_title')
Request Copy
@endsection

@section('content')
<section class="about_banner" style="background:url({{asset('images/about_banner.jpg')}}) no-repeat;">
		<div class="container">
			<div class="about_caption wow fadeInDown" data-wow-delay="0.2s" data-wow-duration="1000ms">
				<h2>Dashboard</h2> </div>
		</div>
		<div class="banner_scroll"> <a href="#dashboard_sctn" title=""><i class="fa fa-angle-down" aria-hidden="true"></i></a> </div>
	</section>
	<!--about_banner end -->
	<section class="dashboard_main">
		<div class="container">
			<div class="row">
				@include('layouts/partials/sidebar')
				<div class="col-sm-9 dashboard_rght">
					<div class="dashboard_side_cntnt">
						<div class="myprofile_sctn">
							<div class="table-responsive">
								<table class="tbl_srs">
									<thead>
										<tr>
											<th style="width:430px;">
												<label>Comic Title</label>
											</th>
											<th style="width:430px;">
												<label>No. Of Requested Copy</label>
											</th>
											<th style="width:116px;">
												<label>&nbsp;</label>
											</th>
										</tr>
									</thead>
									<tbody>
										@forelse($mangaRequests as $request)
										<tr>
											<td>
												<p>{{$request->manga->title}}
													<span>Volume: {{$request->name}}</span>
												</p>
											</td>
											<td>
												<p>{{count($request->mangaRequests)}}/ {{$request->total_copies}}</p>
											</td>
											<td><a href="{{route('artist-manga-request-detail',$request->id)}}" title="" class="tbl_view_dtl">View Details</a> </td>
										</tr>
										@empty
										<tr>
											<td colspan="3" class="warning">No Data found
											</td>
										</tr>
										@endforelse
									</tbody>
								</table>
							</div>
							<div class="tbl_pgination">{!! $mangaRequests->links() !!}</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
