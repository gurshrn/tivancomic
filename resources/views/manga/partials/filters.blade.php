<form action="{{route('manga-directory')}}" method="get" class="filters">
   <input type="hidden" name="sort" value="date" id="sort">
   <div class="dirctory_srch">
        <div class="cstm_heading cstm_heading_sml">
            <h3>Search Comic by name</h3> </div>
        <div class="search_dirctory">
            <input type="text" placeholder="Enter Name" maxlength="100" class="form_control" name="keywords" value="{{ \Request::get('keywords') }}">
            <button class="srch_drct" type="submit"> <i class="fa fa-search"></i> </button>
        </div>
    </div>
    <div id="categories" class="view_btn_sctn">
        <div class="view_btn">All Filters</div>
    </div>
    <div id="mobile_categories">
        <div class="dirctory_srch genre_sctn">
            <div class="cstm_heading cstm_heading_sml">
                <h3>Genres/ Categories</h3> </div>
            <div class="grp_lst">
                <div class="remembr_sctn">
                    <input type="checkbox" id="0" value="0" name="category_id[]" class="filters cat_all">
                    <label for="0">All</label>
                </div>
                @foreach(getCategories() as $category)
                <div class="remembr_sctn">
                    <input type="checkbox" id="{{$category->id}}" value="{{$category->id}}" name="category_id[]" class="filters cat">
                    <label for="{{$category->id}}">{{$category->name}}</label>
                </div>
                @endforeach
            </div>
            <!-- <div class="apply_main">
                <input type="button" value="Apply" class="view_btn apply_btn"> </div> -->
        </div>
        <div class="dirctory_srch genre_sctn brwse_sctn">
            <div class="cstm_heading cstm_heading_sml">
                <h3>Browse Comic by Year of Released</h3> </div>
                <div class="brwse_list"> 
                    <input type="radio" name="release_year" id="year_0" value="" class="filters"><label for="year_0"><strong>All</strong></label><!-- <a href="javascript:;" title="All"><strong>All</strong></a>  -->
                    @for($i=0;$i<=19;$i++)
                    <input type="radio" name="release_year" value="{{date('Y')-$i}}" id="year_{{date('Y')-$i}}" class="filters"><label for="year_{{date('Y')-$i}}" class="{{ \Request::get('release_year')!=(date('Y')-$i)?:'year' }}">{{date('Y')-$i}}</label><!-- <a href="javascript:;" title="{{date('Y')-$i}}">{{date('Y')-$i}}</a> -->
                    @endfor
                    <input type="radio" name="release_year" id="year_{{date('Y')-20}}" value="older" class="filters"><label for="year_{{date('Y')-20}}">Older</label>
                <!-- <a href="javascript:;" title="Older">older</a> -->
                </div>
        </div>
        <!--brwse_sctn end -->
        <div class="dirctory_srch genre_sctn">
            <div class="cstm_heading cstm_heading_sml">
                <h3>Browse Comic by Status</h3> 
            </div>
            <div class="grp_lst">
                @foreach(getManagReleaseStatus() as $status)
                <div class="remembr_sctn">
                    <input type="checkbox" value="{{$status}}" id="release_{{$status}}" name="release_status[]" class="filters">
                    <label for="release_{{$status}}">{{$status}}</label>
                </div>
                @endforeach
                
            </div>
        </div>
    </div>
</form>

