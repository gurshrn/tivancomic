@php
$selectedCh=0;
@endphp
<div class="manga_reading_nav">
	<div class="chose_dtl "> <!-- back_btn -->
		<div class="slct">
		<!-- 	<a href="{{route('cms_manga_detail',$manga->slug)}}" title="{{$manga->title}}" class="active">Back</a> -->
			<select class="placeholder1 form_control" name="ch" onChange="doReload(this.value,'1');">
				@foreach($manga->chapterRead as $chapter)
					@if($selectedCh==0)
						@if($ch!=$chapter->id)
							@php
								$firstChapter=$chapter;
							@endphp
						@else
							@php
								$selectedCh=1;
							@endphp
						@endif
					@elseif($selectedCh==1)
						@php
						$lastChapter=$chapter;
						$selectedCh=2;
						@endphp
					@endif
				<option value="{{$chapter->id}}" {{$ch!=$chapter->id?:'selected'}}>Vol {{($chapter->volume()?$chapter->volume()->name:'').' - '. $chapter->name}}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="reading_pagination"><a href="javascript:;" class="previous" onclick="doReload({{$page_no==1?$firstChapter->id:$ch}},{{$page_no==1?($ch==$firstChapter->id?1:(getPagesCount($firstChapter->id)+1)):$page_no}},'Previous');">Previous</a>Page
		<select name="page" onChange="doReload({{$ch}},this.value);">
			@foreach(getMangaPages($manga->id,$ch) as $page)
			<option value="{{$page->page_no}}" {{$page_no!=$page->page_no?:'selected'}}>{{$page->page_no}}</option>
			@endforeach
		</select>
		of {{$totalPages}}
		<a href="javascript:;" onclick="doReload({{$page_no==$totalPages?$lastChapter->id:$ch}},{{$page_no==$totalPages?($ch==$lastChapter->id?$page_no:0):$page_no}},'Next');" class="next active">Next</a>
	</div>
</div>