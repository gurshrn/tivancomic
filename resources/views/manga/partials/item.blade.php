<div class="chapter_bx">
	<figure><a href="{{route('cms_manga_detail',$manga->slug)}}" title="{{$manga->title}}"><img src="{{empty($manga->cover)?'images/logo.png':asset('storage/covers/').'/'.$manga->cover}}" alt="{{$manga->title}}"></a></figure>
	<div class="chapter_bx_rght">
		<div class="chp_head">
			<h6><a href="{{route('cms_manga_detail',$manga->slug)}}" title="{{$manga->title}}">{{$manga->title}}</a></h6></div>
		<div class="rating_sctn">
			@php 
				$rating=count($manga->mangaRatings)>0?$manga->mangaRatings->sum('rating')/count($manga->mangaRatings):0;
			 @endphp
			@for($i=1;$i<=5;$i++)
			<i class="star fa fa-star" aria-hidden="true">
				<i class="star star-over fa fa-star" style="width: {{$i<=$rating?'100':($i-$rating)/100 }}%"></i>
			</i>
			@endfor</div>
		<div class="chpt_sctn">
			<p>Chapter {{count($manga->chapters)}}</p>
			<p>{{($manga->manga_views_count)}} views</p>
		</div>
	</div>
</div>