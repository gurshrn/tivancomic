@extends('layouts.app')
@section('page_title')
Edit Profile
@endsection

@section('content')
<section class="about_banner" style="background:url({{asset('images/about_banner.jpg')}}) no-repeat;">
		<div class="container">
			<div class="about_caption wow fadeInDown" data-wow-delay="0.2s" data-wow-duration="1000ms">
				<h2>Dashboard</h2> </div>
		</div>
		<div class="banner_scroll"> <a href="#dashboard_sctn" title=""><i class="fa fa-angle-down" aria-hidden="true"></i></a> </div>
	</section>
	<!--about_banner end -->
	<section class="dashboard_main">
		<div class="container">
			<div class="row">
				@include('layouts/partials/sidebar')
				<div class="col-sm-9 dashboard_rght">
					<h3 class="title_sctn">Edit Profile</h3>
					<div class="dashboard_side_cntnt">
						<div class="myprofile_sctn">
							<form action="{{route('update-profile')}}" method="post">
								@csrf
								<div class="med_inpt1">
									<p> 
										<input type="text" placeholder="Full Name" class="cptl form_control validate[required,custom[onlyLetterSp]]" value="{{old('name')?e(old('name')):Auth::user()->name}}" name="name" maxlength="50">
									</p>
									<p>
										<input type="email" placeholder="Email" class="form_control validate[required,custom[email]]" value="{{Auth::user()->email}}" name="email"  maxlength="50" readonly>
									</p>
								</div>
								<div class="med_inpt1">
									<p class="slct">
										<select class="form_control placeholder1" name="gender">
											<option value="">Gender</option>
											@foreach(getGender() as $gender)
											<option value="{{$gender}}" {{Auth::user()->gender?(Auth::user()->gender==$gender?'selected':''):''}}>{{$gender}}</option>
											@endforeach
										</select>
									</p>
									<p class="pos_rel">
										<input type="text" placeholder="Date of Birth" class="form_control validate[past[now]]" id="dob" value="{{old('dob')?e(old('dob')):(Auth::user()->dob?date('m/d/Y',strtotime(Auth::user()->dob)):'')}}" name="dob" readonly><i class="fa fa-calendar" aria-hidden="true"></i></p>
								</div>
								<div class="full_inpt1">
									<textarea placeholder="Description" class="form_control"  name="bio"> {{old('bio')?e(old('bio')):Auth::user()->bio}}</textarea>
								</div>
								<div class="login_loader save_btn_bx">
									<input type="submit" value="Save" class="view_btn"> </div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
