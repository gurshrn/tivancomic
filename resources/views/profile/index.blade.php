@extends('layouts.app')
@section('page_title')
My Profile
@endsection

@section('content')
<section class="about_banner" style="background:url({{ asset('images/about_banner.jpg')}}) no-repeat;">
		<div class="container">
			<div class="about_caption wow fadeInDown" data-wow-delay="0.2s" data-wow-duration="1000ms">
				<h2>Dashboard</h2> </div>
		</div>
		<div class="banner_scroll">
         <a href="#dashboard_sctn" title=""><i class="fa fa-angle-down" aria-hidden="true"></i></a>     
        </div>
	</section>
	<!--about_banner end -->
	<section class="dashboard_main">
		<div class="container">
			<div class="row">
				@include('layouts/partials/sidebar')
				<div class="col-sm-9 dashboard_rght">
					<div class="dashboard_side_cntnt">
						<div class="myprofile_sctn">
						 <div class="avatar-upload">
								<div class="avatar-edit">
								<form action="{{route('upload-avatar')}}" method="POST" id="formAvatar" accept-charset="UTF-8" enctype="multipart/form-data">
    									<input type="hidden" name="_token" value="{{ csrf_token() }}">
			
									<input type='file' name="photo"  id="imageUpload" accept=".png, .jpg, .jpeg" />
									<label for="imageUpload"></label>
								</form>
								</div>
								<div class="avatar-preview">
									<div id="imagePreview" style="background-image: url({{asset('storage/').'/'.(empty(Auth::user()->avatar)?Config::get('voyager.user.default_avatar'):Auth::user()->avatar)}});">
									</div>
								</div>
							</div>
							<div class="myprofile_info">
								<a href="{{route('edit-profile')}}" title="" class="edit_pro">Edit profile</a>
								<h5><label>Name:</label>
						          <p>{{Auth::user()->name}}</p>
							     </h5>
								<h5><label>DOB:</label>
						          <p>{{Auth::user()->dob?date('d/M/Y',strtotime(Auth::user()->dob)):''}}</p>
							     </h5>
								<h5><label>Email:</label>
						          <p>{{Auth::user()->email}}</p>
							     </h5>
								<h5><label>Gender:</label>
						          <p>{{Auth::user()->gender}}</p>
							     </h5>
								<h5><label>Description:</label>
						          <p>{!! Auth::user()->bio !!}</p>
							     </h5> </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
