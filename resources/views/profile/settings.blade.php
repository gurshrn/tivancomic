@extends('layouts.app')
@section('page_title')
Settings
@endsection

@section('content')
<section class="about_banner" style="background:url({{asset('images/about_banner.jpg')}}) no-repeat;">
		<div class="container">
			<div class="about_caption wow fadeInDown" data-wow-delay="0.2s" data-wow-duration="1000ms">
				<h2>Dashboard</h2> </div>
		</div>
		<div class="banner_scroll"> <a href="#dashboard_sctn" title=""><i class="fa fa-angle-down" aria-hidden="true"></i></a> </div>
	</section>
	<!--about_banner end -->
	<section class="dashboard_main">
		<div class="container">
			<div class="row">
				@include('layouts/partials/sidebar')
				<div class="col-sm-9 dashboard_rght">
					<h3 class="title_sctn">Settings</h3>
					<div class="dashboard_side_cntnt">
						<div class="myprofile_sctn">
							 <form action="{{route('change-password')}}" method="post">
                				@csrf
            					<div class="med_inpt1">
									<p> 
										<input type="password" placeholder="Old Password" class="cptl form_control validate[required]" name="old_password" maxlength="50">
									</p>
									<p>
										<input type="password" placeholder="New Password" class="form_control validate[required]" name="password" id="password"  maxlength="50" >
									</p>
									<p> 
										<input type="password" placeholder="Confirm Password" class="cptl form_control validate[required,equals[password]]"  name="password_confirmation" maxlength="50">
									</p>
								</div>
								<div class="login_loader save_btn_bx">
									<input type="submit" value="Update" class="view_btn"> </div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
