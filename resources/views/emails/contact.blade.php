<table>
	<tr>
		<td colspan="2"><h2>Contact Query</h2></td>
	</tr>
	<tr>
		<td>Name:</td>
		<td>{{$contact->name}}</td>
	</tr>
	<tr>
		<td>Phone:</td>
		<td>{{$contact->phone}}</td>
	</tr>
	<tr>
		<td>E-Mail:</td>
		<td>{{$contact->email}}</td>
	</tr>
	<tr>
		<td>Category:</td>
		<td>{{$contact->category}}</td>
	</tr>
	<tr>
		<td>Message:</td>
		<td>{!! $contact->message !!}</td>
	</tr>
</table>