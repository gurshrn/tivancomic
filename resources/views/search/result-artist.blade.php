@php $artist=$result; @endphp
@php $type=($artist instanceof App\User)?'User':'Artist' @endphp
@if($type=='User' || $artist->has('publisher'))
<div class="col-sm-6 blog_main">
<a href="{{route('artist-profile',$artist->slug)}}">
	<figure><img src="{{asset('storage/').'/'.(empty($artist->avatar)?Config::get('voyager.user.default_avatar'):$artist->avatar)}}" alt="{{$artist->name}}"></figure>
	<h6>{{$artist->name}}</h6>
	<p>Published Comics: {{count($artist->mangas)}}</p>
</a>
</div>
@endif