@extends('layouts.app')
@section('page_title', 'Search')
@section('google-ad', true)

@section('content')
   @include('widgets/popularMangaBanner')
    <section class="chapter_sec directory_page">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 chapter_sec_lft">
                    <figure><!-- Home Page Bottom Ad -->
                        <ins class="adsbygoogle"
                             style="display:inline-block;width:970px;height:90px"
                             data-ad-client="ca-pub-4954418714708367"
                             data-ad-slot="4537817178"></ins></figure>
                    <div class="chapter_sec_main">
                        <div class="cstm_heading">
                            <h3>Results</h3></div>
                        <div class="chapter_sc">
                            @forelse($resultCollections as $collectionName => $collection)
                                @if (count($collection) > 0)
                                   <!--  <div class="cell small-12">
                                        <h3>{{ ucfirst($collectionName) }}s</h3>
                                    </div>  --><!-- /.cell -->

                                    @foreach($collection as $result)
                                        @includeFirst([
                                            "search.result-$collectionName",
                                            "search.result"
                                        ])
                                    @endforeach

                                    <div class="cell small-12"><hr /></div>
                                @endif
                            @empty
                                <p>No result found</p>
                            @endforelse
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 chapter_sec_rght">
                    <div class="drctry_ad">
                        <figure><!-- comic page side bar -->
                            <ins class="adsbygoogle"
                                 style="display:inline-block;width:300px;height:250px"
                                 data-ad-client="ca-pub-4954418714708367"
                                 data-ad-slot="3783972851"></ins>
                        </figure>
                    </div>
                    @include('layouts.partials.searchSide')
                    @include('widgets/newMangas')
                </div>
            </div>
        </div>
    </section>
 
   @endsection
