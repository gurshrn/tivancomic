@extends('layouts.app')
@section('page_title')
Home
@endsection

@section('content')
@include('widgets/slider')
	<section class="home_recent_sec">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="ad_lft1"> <img src="{{asset('images/ad_vts1.jpg')}}" alt="ad_vts1"> </div>
					<div class="recent_mid_sc">
						<div class="ad_mid"> <img src="{{asset('images/ad_vt3.jpg')}}" alt="ad_vt3"></div>
						@if(count(getMangaRecent())>0)
						<div class="recent_slider">
							<div class="rcnt_bx">
								<div class="cstm_heading">
									<h3>Recently Added</h3></div>
								<div class="view_btn_sctn"><a href="{{route('manga-directory')}}" title="" class="view_btn">View All</a></div>
							</div>
							<div id="latest_slide" class="owl-carousel owl-theme">
								@foreach(getMangaRecent(12) as $manga)
								<div class="item">
									<div class="item_bx">
										<a href="{{route('cms_manga_detail',$manga->slug)}}" title="{{$manga->title}}">
											<figure><img src="{{empty($manga->cover)?'images/logo.png':asset('storage/covers/').'/'.$manga->cover}}" alt="pro1"></figure> <span>{{$manga->title}}</span> </a>
									</div>
								</div>
								@endforeach
							</div>
						</div>
						@endif
					</div>
					<div class="ad_rght"> <img src="{{asset('images/ad_vt2.jpg')}}" alt="ad_vt2"></div>
				</div>
			</div>
		</div>
	</section>
	<!-- home_recent_sec end -->
	<section class="chapter_sec">
		<div class="container">
			<div class="row">
				<div class="col-sm-9 chapter_sec_lft">
					<div class="chapter_sec_main">
						<div class="cstm_heading">
							<h3>Latest Chapters</h3></div>
						<div class="chapter_sc">
							@foreach(getMangaLatestChapter(12) as $manga)
							<div class="chapter_bx">
								<figure><img src="{{empty($manga->cover)?'images/logo.png':asset('storage/covers/').'/'.$manga->cover}}" alt="chapter_img1"></figure>
								<div class="chapter_bx_rght">
									<div class="chp_head">
										<h6><a href="{{route('cms_manga_detail',$manga->slug)}}" title="">{{$manga->title}}</a></h6><small class="timeAgo" data="{{ date('c', strtotime( $manga->latestpage->created_at )) }}">20 min ago</small></div>
									<ul>
										@php $date=date('Y-m-d'); $i=0; @endphp
										@foreach($manga->latestchapters as $chapter)
											@if($i==0)
												@php $date=date('Y-m-d',strtotime($chapter->latestpage->created_at)); $i++; @endphp
											@elseif($date!=date('Y-m-d',strtotime($chapter->latestpage->created_at)))
												@php $i=0; break; @endphp
											@endif
											@if($i < 3)
												<li><a href="{{route('manga-reading',$manga->slug)}}?ch={{$chapter->id}}">Chapter {{$chapter->number.': '.$chapter->name}}</a></li>
											@elseif($i==3)
												</ul>
											@endif
											@php $i++; @endphp											
										@endforeach
										
									@if($i-3>0)
									<p><small>{{$i-3}} more chapters...</small></p>
									@elseif($i-3 <= 0)
										</ul>
									@endif
								</div>
							</div>
							<!-- chapter_bx end -->
							@endforeach

							<div class="vw_sctn"><a href="{{route('manga-directory')}}" title="" class="view_btn">View All Comics</a></div>
						</div>
					</div>
					<div class="ad_sctn_cstm"><img src="{{asset('images/ad_vt4.jpg')}}" alt="ad_vt4"></div>
					<div class="feature_btm">
						 <div class="feature_btm_bx" style="background:url(images/feature1.jpg) no-repeat;"> <a href="#" title="">Featured Mangaka</a> </div>
						<div class="feature_btm_bx" style="background:url(images/meet_img.jpg) no-repeat;"> <a href="#" title="">Meet the mangakas</a> </div>
					</div>
				</div>
				<div class="col-sm-3 chapter_sec_rght">
					@include('widgets/mostPopular')
				</div>
			</div>
		</div>
		<div class="side_img_sctn" style="background:url(images/side_img.png) no-repeat;"></div>
	</section>
	<!-- chapter_sec end -->
	@include('widgets/latestBlog')
@endsection