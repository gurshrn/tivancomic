@extends('layouts.app')
@section('page_title')
Blog
@endsection

@section('content')
<!-- header end -->
	<section class="about_banner" style="background:url({{asset('images/about_banner.jpg')}}) no-repeat;">
		<div class="container">
			<div class="about_caption wow fadeInDown" data-wow-delay="0.2s" data-wow-duration="1000ms">
				<h2>Blog</h2> </div>
		</div>
	</section>
	<!--about_banner end -->
	<section class="blog_page">
		<div class="container">
			<div class="row">
				@php $i=0.1; @endphp
				@foreach($blogs as $post)
				@include('blog.partials.item')
				@php $i=$i+0.1; @endphp
				@endforeach
			</div>
			<div class="pagination_sctn">{!! $blogs->links('layouts.partials.pagination') !!}</div>
		</div>
	</section>
@endsection
