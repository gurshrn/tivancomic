<div class="col-sm-6 blog_main">
	<div class="blog_bx wow fadeInUp" data-wow-delay="{{isset($i)?$i:0}}s" data-wow-duration="1000ms">
	<figure style="background:url({{isset($post->image)?(filter_var($post->image, FILTER_VALIDATE_URL) ? $post->image : Voyager::image( $post->image )):asset('images/logo.png')}})"></figure>
	<h6><i class="fa fa-calendar" aria-hidden="true"></i> {{date('F d, Y',strtotime($post->created_at))}} ||  By {{$post->authorId->name}}</h6>
	<h4><a href="{{route('blog-detail',$post->slug)}}" title="{{$post->title}}">{{$post->title}}</a></h4>
	<p>{!! empty($post->excerpt)?'':$post->excerpt !!}</p> <a href="{{route('blog-detail',$post->slug)}}" title="{{$post->title}}" class="read_more">Read More...</a> </div>
</div>