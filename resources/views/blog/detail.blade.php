@extends('layouts.app')
@section('page_title')
{{$blog->title}}
@endsection
@section('meta_title', $blog->title)
@section('meta_description', $blog->meta_description)

@section('content')
 <!-- header end -->
	<section class="about_banner" style="background:url({{asset('images/about_banner.jpg')}}) no-repeat;">
		<div class="container">
			<div class="about_caption wow fadeInDown" data-wow-delay="0.2s" data-wow-duration="1000ms">
				<h2>Blog</h2> </div>
		</div>
	</section>
	<!--about_banner end -->
	<section class="blog_page blog_innr">
		<div class="container">
			<div class="row">
				<div class="col-sm-9 blog_bx">
					<figure>@if(isset($blog->image))
	                        <img src="{{ filter_var($blog->image, FILTER_VALIDATE_URL) ? $blog->image : Voyager::image( $blog->image ) }}" style="width:100%" alt="{{$blog->title}}" />
	                     @endif</figure>
					<div class="blog_cntnt">
						<h6><i class="fa fa-calendar" aria-hidden="true"></i> {{date('F d, Y',strtotime($blog->created_at))}} ||  By {{$blog->authorId->name}}</h6>
						<h4>{{$blog->title}}</h4>
						<p>{!! $blog->body !!}</p>
					</div>
				</div>
				<div class="col-sm-3 blog_rcnt_pst">
					
					@include('widgets/recentBlog')
					<div class="chapter_sec_rght blog-sidebar">
						@include('widgets/newMangas')
					</div>
					
				</div>
			</div>
		</div>
	</section>
	
@endsection
