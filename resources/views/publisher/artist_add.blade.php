@extends('layouts.app')
@section('page_title')
Artist Profile
@endsection

@section('content')
<section class="about_banner" style="background:url({{ asset('images/about_banner.jpg')}}) no-repeat;">
		<div class="container">
			<div class="about_caption wow fadeInDown" data-wow-delay="0.2s" data-wow-duration="1000ms">
				<h2>Dashboard</h2> </div>
		</div>
		<div class="banner_scroll">
         <a href="#dashboard_sctn" title=""><i class="fa fa-angle-down" aria-hidden="true"></i></a>     
        </div>
	</section>
	<!--about_banner end -->
	<section class="dashboard_main">
		<div class="container">
			<div class="row">
				@include('layouts/partials/sidebar')
				<div class="col-sm-9 dashboard_rght">
					<h3 class="title_sctn">{{$artist?'Update':'Add'}} Artist</h3>
					<div class="dashboard_side_cntnt">
						<div class="myprofile_sctn">
							<form action="{{route('publisher-artist-add-post')}}" method="post"  accept-charset="UTF-8" enctype="multipart/form-data">
							<div class="avatar-upload">
								<div class="avatar-edit">
									<input type='file' name="avatar"  id="artist-avatar" accept=".png, .jpg, .jpeg" />
									<label for="artist-avatar"></label>
								</div>
								<div class="avatar-preview">
									<div id="imagePreview" style="background-image: url({{asset('storage/').'/'.(empty($artist->avatar)?Config::get('voyager.user.default_avatar'):$artist->avatar)}});">
									</div>
								</div>
							</div>
							<div class="myprofile_info">
								@csrf
								@if($artist)
								 	<input type="hidden" name="id" value="{{$artist->id}}">
								@endif
								<div class="med_inpt1">
									<p> 
										<input type="text" placeholder="Full Name" class="cptl form_control validate[required,custom[onlyLetterSp]]" value="{{old('name')?e(old('name')):(!$artist?'':$artist->name)}}" name="name" maxlength="50">
									</p>
									<p>
										<input type="email" placeholder="Email" class="form_control validate[custom[email]]" value="{{old('email')?e(old('email')):(!$artist?'':$artist->email)}}" name="email"  maxlength="50">
									</p>
								</div>
								<div class="med_inpt1">
									<p class="slct">
										<select class="form_control placeholder1" name="gender">
											<option value="">Gender</option>
											@foreach(getGender() as $gender)
											<option value="{{$gender}}" {{(!$artist?false:$artist->gender)?($artist->gender==$gender?'selected':''):''}}>{{$gender}}</option>
											@endforeach
										</select>
									</p>
									<p class="pos_rel">
										<input type="text" placeholder="Date of Birth" class="form_control validate[past[now]]" id="dob" value="{{old('dob')?e(old('dob')):((!$artist?false:$artist->dob)?date('m/d/Y',strtotime($artist->dob)):'')}}" name="dob" readonly><i class="fa fa-calendar" aria-hidden="true"></i></p>
								</div>
								<div class="full_inpt1">
									<textarea placeholder="Description" class="form_control"  name="bio"> {{old('bio')?e(old('bio')):(!$artist?'':$artist->bio)}}</textarea>
								</div>
								<div class="login_loader save_btn_bx">
									<input type="submit" value="Save" class="view_btn"> </div>
							</div>
							</form> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
