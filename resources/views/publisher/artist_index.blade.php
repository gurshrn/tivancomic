@extends('layouts.app')
@section('page_title')
Artist List
@endsection

@section('content')
	<section class="about_banner" style="background:url({{asset('images/about_banner.jpg')}}) no-repeat;">
		<div class="container">
			<div class="about_caption wow fadeInDown" data-wow-delay="0.2s" data-wow-duration="1000ms">
				<h2>Dashboard</h2> </div>
		</div>
		<div class="banner_scroll"> <a href="#dashboard_sctn" title=""><i class="fa fa-angle-down" aria-hidden="true"></i></a> </div>
	</section>
	<!--about_banner end -->
	<section class="dashboard_main">
		<div class="container">
			<div class="row">
				@include('layouts/partials/sidebar')
				<div class="col-sm-9 dashboard_rght">
						<div class="dashboard_side_cntnt">
						<div class="myprofile_sctn">
							<div class="series_btn"> <a href="{{route('publisher-artist-add')}}" title="" class="view_btn">Add New Artist</a></div>
							<div class="table-responsive">
							<table class="tbl_srs">
								<thead>
									<tr>
										<th style="width:160px;">
											<label>Profile Pic</label>
										</th>
										<th style="width:160px;">
											<label>Name</label>
										</th>
										<th style="width:243px;">
											<label>E-Mail</label>
										</th>
										<th style="width:142px;">
											<label>No. of Comics</label>
										</th>
										<th style="width:157px;">
											<label>Action</label>
										</th>
									</tr>
								</thead>
								<tbody>
									@forelse($artists as $artist)
									<tr>
										<td>
											<figure><img src="{{empty($artist->avatar)?'images/logo.png':asset('storage/').'/'.$artist->avatar}}" alt="{{$artist->name}}"></figure>
										</td>
										<td>
											<p>{{$artist->name}}</p>
										</td>
										<td>
											<p>{{$artist->email}}</p>
										</td>
										<td>
											<p>{{count($artist->mangas)}}</p>
										</td>
										<td>
											<a href="{{route('artist-profile',$artist->slug)}}" title=""><i class="fa fa-eye" aria-hidden="true"></i></a>
											<a href="{{route('publisher-artist-add',$artist->slug)}}" title="Edit"> <i class="fa fa-pencil" aria-hidden="true"></i></a>
											<a href="{{route('delete-publisher-artist',$artist->slug)}}" title="Delete"> <i class="fa fa-times" aria-hidden="true"></i></a>
										</td>
									</tr>
										@empty
										<tr>
											<td colspan="5" class="warning">
												No Data found
											</td>
										</tr>
										@endforelse
									</tbody>
								</table>
							</div>
							<div class="tbl_pgination">{!! $artists->links() !!}</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
