@extends('layouts.app')
@section('page_title')
Request Detail
@endsection
@section('chart', true)

@section('content')
<section class="about_banner" style="background:url({{asset('images/about_banner.jpg')}}) no-repeat;">
		<div class="container">
			<div class="about_caption wow fadeInDown" data-wow-delay="0.2s" data-wow-duration="1000ms">
				<h2>Dashboard</h2> </div>
		</div>
		<div class="banner_scroll"> <a href="#dashboard_sctn" title=""><i class="fa fa-angle-down" aria-hidden="true"></i></a> </div>
	</section>
	<!--about_banner end -->
	<section class="dashboard_main">
		<div class="container">
			<div class="row">
				@include('layouts/partials/sidebar')
				<div class="col-sm-9 dashboard_rght">
					<div class="dashboard_side_cntnt btm_0">
						<div class="myprofile_sctn">
							<h3 class="title_sctn">Total Earning of this month: <strong>${{$currentMonth?$currentMonth->total_revenue:0}}</strong></h3>
							<div class="stat_sctn">
								<div class="chart" id="earnings"></div>
								<!-- <figure><img src="images/graph_img.jpg" alt="graph_img"></figure> -->
							</div>
						</div>
					</div>
					<div class="dashboard_side_cntnt">
						<div class="myprofile_sctn">
							<div class="earn_page">
								<h3 class="title_sctn">Details</h3>
								<!-- <form class="search_dirctory earn_srch">
									<input type="text" placeholder="Search" class="form_control">
									<button class="srch_drct" type="button"> <i class="fa fa-search"></i> </button>
								</form> -->
							</div>
							<div class="table-responsive">
								<table class="tbl_srs tbl_earn">
									<thead>
										<tr>
											<th style="width:267px;">
												<label>Comic Title</label>
											</th>
											<th style="width:270px;">
												<label>Month</label>
											</th>
											<th style="width:302px;">
												<label>Total Views</label>
											</th>
											<th style="width:138px;">
												<label>Earning</label>
											</th>
										</tr>
									</thead>
									<tbody>
										@forelse($mangaAdRevenue as $revenue)
											<tr>
												<td>
													<a href="{{$revenue->manga?route('cms_manga_detail',$revenue->manga->slug):'javascript:;'}}" title="{{$revenue->manga?$revenue->manga->title:'Comic Deleted'}}"> {{$revenue->manga?$revenue->manga->title:'Comic Deleted'}}</a>
												</td>
												<td>
													{{date('M',strtotime(date('Y').'-'.$revenue->month.'-01'))}}
												</td>
												<td >
													{{$revenue->total_ad_clicks}}
												</td>
												<td>
													{{$revenue->total_revenue}}
												</td>
											</tr>
										@empty
										<tr><td colspan="4" class="warning">No Data Found.</td></tr>
										@endforelse
									</tbody>
								</table>
							</div>
							<div class="tbl_pgination">{!! $mangaAdRevenue->links() !!}</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
@endsection
@section('page_script')

var IndexToMonth = [ 
"Jan", 
"Feb", 
"Mar", 
"Apr", 
"May", 
"Jun",
"Jul", 
"Aug", 
"Sept", 
"Oct", 
"Nov", 
"Dec" 
];

//** Charts
new Morris.Area({ 
  // ID of the element in which to draw the chart.
  element: 'earnings',
  // Chart data records -- each entry in this array corresponds to a point on
  // the chart.

  data: [
    @foreach($ad_revenue as $revenue)
    { months: '{{date('Y').'-'.$revenue['month'].'-1'}}', value: {{$revenue['total_revenue']}}, clicks: {{$revenue['total_ad_clicks']}} },
   	@endforeach
  ],
  // The name of the data record attribute that contains x-values.
  xkey: 'months',
  // A list of names of data record attributes that contain y-values.
  ykeys: ['value','clicks'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['Earnings','Clicks'],
  pointFillColors: ['#E55239','#ED7D31'],
  lineColors: ['#E55239','#ED7D31'],
  hideHover: 'auto',
  gridIntegers: true,
  resize: true,
  xLabelFormat: function (x) {
        var month = IndexToMonth[ x.getMonth() ];
        var year = x.getFullYear();
        var day = x.getDate();
        return  month;
        //return  year + ' '+ day +' ' + month;
    },
    dateFormat: function (x) {
        var month = IndexToMonth[ new Date(x).getMonth() ];
        var year = new Date(x).getFullYear();
        var day = new Date(x).getDate();
        return  month;
        //return year + ' '+ day +' ' + month;
    },
  
});// <------------ MORRIS

@endsection