<div class="chapter_sc">
							@forelse(getMangaLatestChapter(12) as $manga)
							<div class="chapter_bx">
								<figure><img src="{{asset('images/loader.gif')}}" data-src="{{empty($manga->cover)?'images/logo.png':asset('storage/covers/').'/'.$manga->cover}}" alt="{{$manga->title}}"></figure>
								<div class="chapter_bx_rght">
									<div class="chp_head">
										<h6><a href="{{route('cms_manga_detail',$manga->slug)}}" title="{{$manga->title}}">{{$manga->title}}</a></h6><small class="timeAgo" data="{{ date('c', strtotime( $manga->latestpage->created_at )) }}">20 min ago</small></div>
									<ul>
										@php $date=date('Y-m-d'); $i=0; @endphp
										@foreach($manga->latestchapters as $chapter)
											@if($i==0)
												@php $date=date('Y-m-d',strtotime($chapter->latestpage->created_at)); $i++; @endphp
											@elseif($date!=date('Y-m-d',strtotime($chapter->latestpage->created_at)))
												@php $i=0; break; @endphp
											@endif
											@if($i < 3)
												<li><a href="{{route('manga-reading',$manga->slug)}}?ch={{$chapter->id}}">Chapter {{$chapter->number.': '.$chapter->name}}</a></li>
											@elseif($i==3)
												</ul>
											@endif
											@php $i++; @endphp											
										@endforeach
										
									@if($i-3>0)
									<p><small>{{$i-3}} more chapters...</small></p>
									@elseif($i-3 <= 0)
										</ul>
									@endif
								</div>
							</div>
							@empty

							<!-- chapter_bx end -->
							@endforelse

							<div class="vw_sctn"><a href="{{route('manga-directory')}}" title="" class="view_btn">View All Comics</a></div>
						</div>