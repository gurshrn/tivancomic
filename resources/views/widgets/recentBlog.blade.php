@php $blogs=getBlogs(4);
@endphp

@if(count($blogs)>0)
<div class="blog_rcnt_post">
	<h4>Recent Posts</h4>
	@foreach($blogs as $post)
	<div class="blog_post_sc">
		<figure>@if(isset($post->image))
                    <img src="{{asset('images/loader.gif')}}" data-src="{{ filter_var($post->image, FILTER_VALIDATE_URL) ? $post->image : Voyager::image( $post->image ) }}" alt="{{$post->title}}" />
                @else
                	 <img src="{{asset('images/loader.gif')}}" data-src="{{asset('images/logo.png')}}" alt="{{$post->title}}" />
                @endif</figure>
		<div class="blog_rght_cntnt"> <a href="{{route('blog-detail',$post->slug)}}" title="{{$post->title}}">{{$blog->authorId->name}}</a>
			<p>{{$post->title}}</p>
		</div>
	</div>
	<!--blog_post_sc end -->
	@endforeach
</div>
@endif