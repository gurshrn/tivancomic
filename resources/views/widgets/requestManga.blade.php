<div class="modal fade" id="order_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h1 class="modal-title" id="exampleModalLabel">Order Comic</h1>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
			</div>
			<div class="modal-body">
				@if(Auth::check())
				<form action="{{route('request-manga')}}" method="post" class="ajax-post">
					<div class="med_inpt1">
						@if(isset($artist))
						<p class="slct">
							<select class="form_control placeholder1 validate[required]" name="manga_id" id="manga_id">
								<option value="">Select Comic</option>
								@foreach($artist->mangas as $manga)
									<option value="{{$manga->id}}">{{$manga->title}}</option>
								@endforeach
						
							</select>
						</p>
						@else
							<input type="hidden" value="{{$manga->id}}" name="manga_id">
						@endif
						<p class="slct">
							<select class="form_control placeholder1 validate[required]" name="manga_volume_id" id="manga_volume_id">
								<option value="">Select Volume</option>
							@if(!isset($artist))
								@foreach($manga->volumes as $vol)
									<option value="{{$vol->id}}" data-value="{{ $vol->total_copies}}">{{$vol->name}}</option>
								@endforeach
							@endif
							</select>
						</p>
					</div>
					<div class="full_inpt1">
						<p>
							<textarea placeholder="Address" class="form_control validate[required]" name="address"></textarea> </p>
						<p>
					</div>
					<div class="full_inpt1">
						<textarea placeholder="Message" class="form_control validate[required]" name="message"></textarea>
					</div>
					<div class="login_loader login_btn1">
						<input type="submit" value="Place Order" class="view_btn"> </div>
				</form>
				@else
				<p>Kindly Login for order Comic.</p>
				@endif
			</div>
		</div>
	</div>
</div>
