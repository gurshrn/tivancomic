@php $mangas=getMangaRecent(5);
@endphp
@if(count($mangas)>0)
<div class="publish_rght">
	<div class="cstm_heading cstm_heading_sml">
		<h3>New Comic</h3></div>
	<ul>
		@foreach($mangas as $manga)
		<li>
			<figure><a href="{{route('cms_manga_detail',$manga->slug)}}" title="{{$manga->title}}"><img src="{{asset('images/loader.gif')}}" data-src="{{empty($manga->cover)?'images/logo.png':asset('storage/covers/').'/'.$manga->cover}}" alt="{{$manga->title}}"></a></figure>
			<div class="popular_cntnt">
				<h6><a href="{{route('cms_manga_detail',$manga->slug)}}" title="{{$manga->title}}">{{$manga->title}}</a></h6>
				<p>{{$manga->release_status}}, {{count($manga->chapters)}} chapters</p>
			</div>
		</li>
		@endforeach
	</ul>
</div>
@endif