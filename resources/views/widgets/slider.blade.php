@php $slides=getSlides();
@endphp

@if(count($slides)>0)
<section class="banner">
	<div id="banner_slider" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">
			@php $i=0; @endphp
			@foreach($slides as $slide)
			<div class="carousel-item {{$i!=0?:'active'}}" style="background:url({{ filter_var($slide->image, FILTER_VALIDATE_URL) ? $slide->image : Voyager::image( $slide->image ) }}) no-repeat;">
				<div class="banner_caption">
					<div class="container">
						<div class="banner_main">
							<h3>{{ $slide->subheader }}</h3>
							<h1>{{ $slide->header }}</h1><a href="{{ $slide->link }}" title="" class="btn_medium">Learn More</a> </div>
					</div>
				</div>
			</div>
			@php $i=$i+1; @endphp
			
			@endforeach
		</div>
		<div class="slide_pagination">
			<a class="carousel-control-prev" href="#banner_slider" role="button" data-slide="prev"> <span class="n" aria-hidden="true"><i class="fa fa-angle-left" aria-hidden="true"></i></span> <span class="sr-only">Previous</span> </a>
			<a class="carousel-control-next" href="#banner_slider" role="button" data-slide="next"> <span class="" aria-hidden="true"><i class="fa fa-angle-right" aria-hidden="true"></i></span> <span class="sr-only">Next</span> </a>
		</div>
		<ol class="carousel-indicators">
			@php $i=0; @endphp
			@foreach($slides as $slide)
				<li data-target="#banner_slider" data-slide-to="{{ $i }}" class=" {{$i!=0?:'active'}}">{{ $slide->header }}</li>
			@php $i=$i+1; @endphp
			@endforeach
			
		</ol>
	</div>
</section>
<!--banner end -->
@endif