@php $mangas=getMangaPopular(12);
@endphp

@if(count($mangas)>0)
<section class="manga_slider">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h2>Popular comic this month</h2>
				<div id="manga_slide" class="owl-carousel owl-theme">
					@foreach($mangas as $manga)
					<div class="item">
						<a href="{{route('cms_manga_detail',$manga->slug)}}" title="{{$manga->title}}">
							<figure><img src="{{asset('images/loader.gif')}}" data-src="{{empty($manga->cover)?'images/logo.png':asset('storage/covers/').'/'.$manga->cover}}" alt="{{$manga->title}}">
								<div class="manga_ovrlay">
									<h6>{{$manga->title}}</h6><small>Chapter {{count($manga->chapters)}}</small></div>
							</figure>
						</a>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</section>
<!-- manga_slider end -->
@endif