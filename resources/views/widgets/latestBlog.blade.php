@php $blogs=getBlogs(2);
@endphp
@if(count($blogs)>0)
<section class="home_blog" style="background:url({{asset('images/blog_bg.jpg')}}) no-repeat;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h3>Our Latest Blog</h3></div>
		</div>
		<div class="row home_blg_info">
			@foreach($blogs as $post)
			<div class="col-sm-6">
				<figure style="background:url({{isset($post->image)?(filter_var($post->image, FILTER_VALIDATE_URL) ? $post->image : Voyager::image( $post->image )):asset('images/logo.png')}})">
                    </figure>
				<h6><i class="fa fa-calendar" aria-hidden="true"></i> {{date('F d, Y',strtotime($post->created_at))}}</h6>
				<h5><a href="{{route('blog-detail',$post->slug)}}" title="{{$post->title}}">{{$post->title}}</a></h5>
				<p>{!! empty($post->excerpt)?'':$post->excerpt !!} <a href="{{route('blog-detail',$post->slug)}}" title="{{$post->title}}" class="rd_more">Read More</a> </p>
			</div>
			@endforeach
			
		</div>
	</div>
</section>
<!-- home_blog end -->
@endif