<a class="btn btn-success" id="bulk_publish_btn"> <span>Bulk Publish</span></a>

{{-- Bulk publish modal --}}
<div class="modal modal-success fade" tabindex="-1" id="bulk_publish_modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                     Are you sure you want to publish <span id="bulk_publish_count"></span> <span id="bulk_publish_display_name"></span>?
                </h4>
            </div>
            <div class="modal-body" id="bulk_publish_modal_body">
            </div>
            <div class="modal-footer">
                <form action="{{ route('voyager.'.$dataType->slug.'.publish') }}" id="bulk_publish_form" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="ids" id="bulk_publish_input" value="">
                    <input type="submit" class="btn btn-success pull-right delete-confirm"
                             value="Publish {{ strtolower($dataType->display_name_plural) }}">
                </form>
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
                    {{ __('voyager::generic.cancel') }}
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
window.onload = function () {
    // Bulk delete selectors
    var $bulkPublishBtn = $('#bulk_publish_btn');
    var $bulkPublishModal = $('#bulk_publish_modal');
    var $bulkPublishCount = $('#bulk_publish_count');
    var $bulkPublishDisplayName = $('#bulk_publish_display_name');
    var $bulkPublishInput = $('#bulk_publish_input');
    // Reposition modal to prevent z-index issues
    $bulkPublishModal.appendTo('body');
    // Bulk delete listener
    $bulkPublishBtn.click(function () {
        var ids = [];
        var $checkedBoxes = $('#dataTable input[type=checkbox]:checked').not('.select_all');
        var count = $checkedBoxes.length;
        if (count) {
            // Reset input value
            $bulkPublishInput.val('');
            // Deletion info
            var displayName = count > 1 ? '{{ $dataType->display_name_plural }}' : '{{ $dataType->display_name_singular }}';
            displayName = displayName.toLowerCase();
            $bulkPublishCount.html(count);
            $bulkPublishDisplayName.html(displayName);
            // Gather IDs
            $.each($checkedBoxes, function () {
                var value = $(this).val();
                ids.push(value);
            })
            // Set input value
            $bulkPublishInput.val(ids);
            // Show modal
            $bulkPublishModal.modal('show');
        } else {
            // No row selected
            toastr.warning('You haven\'t selected anything to publish');
        }
    });
}
</script>
