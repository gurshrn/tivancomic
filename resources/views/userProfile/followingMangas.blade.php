@extends('layouts.app')
@section('page_title')
Request Detail
@endsection

@section('content')
<section class="about_banner" style="background:url({{asset('images/about_banner.jpg')}}) no-repeat;">
		<div class="container">
			<div class="about_caption wow fadeInDown" data-wow-delay="0.2s" data-wow-duration="1000ms">
				<h2>Dashboard</h2> </div>
		</div>
		<div class="banner_scroll"> <a href="#dashboard_sctn" title=""><i class="fa fa-angle-down" aria-hidden="true"></i></a> </div>
	</section>
	<!--about_banner end -->
	<section class="dashboard_main">
		<div class="container">
			<div class="row">
				@include('layouts/partials/sidebar')
				<div class="col-sm-9 dashboard_rght">
					<div class="dashboard_side_cntnt">
						<div class="myprofile_sctn">
							<div class="table-responsive">
								<table class="tbl_srs">
									<thead>
										<tr>
											<th style="width:255px;">
												<label>Cover Image</label>
											</th>
											<th style="width:296px;">
												<label>Title</label>
											</th>
											<th style="width:333px;">
												<label>Artist</label>
											</th>
											<th style="width:104px;">
												<label>Action</label>
											</th>
										</tr>
									</thead>
									<tbody>
										@forelse($followMangas as $manga)
										<tr>
											<td>
												<figure><img src="{{empty($manga->manga()->cover)?'images/logo.png':asset('storage/covers/').'/'.$manga->manga()->cover}}" alt="{{$manga->manga()->title}}"></figure>
											</td>
											<td>
												<p>{{$manga->manga()->title}}</p>
											</td>
											<td>
												<p>{{$manga->manga()->artist}}</p>
											</td>
											<td>
												<form action="{{route('unfollow-manga')}}" method="post" class="ajax-post">
													<input type="hidden" name="manga_id" value="{{$manga->manga()->id}}">
													<button type="submit" class="tbl_view_dtl brdr_none">Unfollow</button>
												</form>
											</td>
										</tr>
										@empty
										<tr><td colspan="4" class="warning">No Data Found.</td></tr>
										@endforelse
									</tbody>
								</table>
							</div>
							<div class="tbl_pgination">{!! $followMangas->links() !!}</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
@endsection
@section('page_script')
			
@endsection