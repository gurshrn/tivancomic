@extends('layouts.app')
@section('page_title')
Request Detail
@endsection

@section('content')
<section class="about_banner" style="background:url({{asset('images/about_banner.jpg')}}) no-repeat;">
		<div class="container">
			<div class="about_caption wow fadeInDown" data-wow-delay="0.2s" data-wow-duration="1000ms">
				<h2>Dashboard</h2> </div>
		</div>
		<div class="banner_scroll"> <a href="#dashboard_sctn" title=""><i class="fa fa-angle-down" aria-hidden="true"></i></a> </div>
	</section>
	<!--about_banner end -->
	<section class="dashboard_main">
		<div class="container">
			<div class="row">
				@include('layouts/partials/sidebar')
				<div class="col-sm-9 dashboard_rght">
					<div class="dashboard_side_cntnt">
						<div class="myprofile_sctn">
							<div class="table-responsive">
								<table class="tbl_srs">
									<thead>
										<tr>
											<th style="width:420px;">
												<label>Artist Image</label>
											</th>
											<th style="width:452px;">
												<label>Name</label>
											</th>
											<th style="width:104px;">
												<label>Action</label>
											</th>
										</tr>
									</thead>
									<tbody>
										@forelse($followArtists as $artist)
										<tr>
											<td>
												<figure><img src="{{asset('storage/').'/'.(empty($artist->artist->avatar)?Config::get('voyager.user.default_avatar'):$artist->artist->avatar)}}" alt="{{$artist->artist->name}}"></figure>
											</td>
											<td>
												<p>{{$artist->artist->name}}</p>
											</td>
											<td>
												<form action="{{route('unfollow-artist')}}" method="post" class="ajax-post">
													<input type="hidden" name="artist_id" value="{{$artist->id}}">
													<button type="submit" class="tbl_view_dtl brdr_none">Unfollow</button>
												</form>
											</td>
										</tr>
										@empty
										<tr><td colspan="3" class="warning">No Data Found.</td></tr>
										@endforelse
									</tbody>
								</table>
							</div>
							<div class="tbl_pgination">{!! $followArtists->links() !!}</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
@endsection
@section('page_script')
			
@endsection