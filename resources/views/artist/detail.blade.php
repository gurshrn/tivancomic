@extends('layouts.app')
@section('page_title')
Artist Detail
@endsection
@section('google-ad', true)
@section('content')
@include('widgets/popularMangaBanner')

	<!-- manga_slider end -->
	<section class="chapter_sec directory_page artist_detail">
		<div class="container">
			<div class="row">
				<div class="col-sm-8 chapter_sec_lft">
					<figure><!-- Home Page Bottom Ad -->
						<ins class="adsbygoogle"
						     style="display:inline-block;width:970px;height:90px"
						     data-ad-client="ca-pub-4954418714708367"
						     data-ad-slot="4537817178"></ins></figure>
					<div class="chapter_sec_main">
						<div class="artist_breadcrumb">
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="{{route('artist-directory')}}">Artist Directory</a></li>
								<li class="breadcrumb-item"><a href="{{route('artist-profile',$artist->slug)}}">Artist Profile</a></li>
								<li class="breadcrumb-item active" aria-current="page">{{$artist->name}}</li>
							</ol>
						</div>
						<div class="chapter_sc">
							@foreach($mangas as $manga)
								@include('manga/partials/item')
							@endforeach
							<!-- chapter_bx end -->
							<div class="pagination_sctn">{!! $mangas->links('layouts.partials.pagination') !!}</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4 chapter_sec_rght">
					@include('widgets/newMangas')
				</div>
			</div>
		</div>
	</section>
	@include('widgets/latestBlog')

@endsection
