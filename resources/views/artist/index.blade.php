@extends('layouts.app')
@section('page_title')
Artist Directory
@endsection

@section('content')
@include('widgets/popularMangaBanner')
<section class="chapter_sec artist_directory">
		<div class="container">
			<div class="row">
				<div class="col-sm-8 chapter_sec_lft">
					<div class="chapter_sec_main">
						<div class="cstm_heading">
							<h3>Artist Directory</h3></div>
						<div class="artist_drctry">
							<ul>
								@php $i=0.1; @endphp
								@forelse($artists as $artist)
								<li class="wow zoomIn" data-wow-delay="{{$i}}s" data-wow-duration="1000ms">
									<a href="{{route('artist-profile',$artist->slug)}}">
										<figure><img src="{{asset('storage/').'/'.(empty($artist->avatar)?Config::get('voyager.user.default_avatar'):$artist->avatar)}}" alt="{{$artist->name}}"></figure>
										<h6>{{$artist->name}}</h6>
										<p>Published Comics: {{count($artist->mangas)}}</p>
									</a>
								</li>
								@php $i=$i+0.1; @endphp
								@empty
									<li>No Data Found.</li>
								@endforelse
							</ul>
						</div>
						<div class="pagination_sctn">{!! $artists->links('layouts.partials.pagination') !!}</div>
					</div>
				</div>
				<div class="col-sm-4 chapter_sec_rght">
					@include('widgets/newMangas')
				</div>
			</div>
		</div>
	</section>

@include('widgets/latestBlog')
@endsection
