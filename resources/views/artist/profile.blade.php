@extends('layouts.app')
@section('page_title')
Artist Profile
@endsection
@section('google-ad', true)

@section('content')
@include('widgets/popularMangaBanner')

	<!-- manga_slider end -->
		<section class="chapter_sec directory_page artist_detail">
		<div class="container">
			<div class="row">
				<div class="col-sm-8 chapter_sec_lft">
					<figure><!-- Home Page Bottom Ad -->
						<ins class="adsbygoogle"
						     style="display:inline-block;width:970px;height:90px"
						     data-ad-client="ca-pub-4954418714708367"
						     data-ad-slot="4537817178"></ins></figure>
					<div class="chapter_sec_main">
						<div class="artist_profile">
							<figure><img src="{{asset('storage/').'/'.(empty($artist->avatar)?Config::get('voyager.user.default_avatar'):$artist->avatar)}}" alt="{{$artist->name}}"></figure>
							<div class="artist_cntnt">
								<div class="artist_form">
									<label>Name:</label>
									<p>{{$artist->name}}</p>
								</div>
								@if(!empty( $artist->bio))
								<div class="artist_form">
									<label>Description:</label>
									<p>{!! $artist->bio !!} </p>
								</div>
								@endif
								@php
								 $type=($artist instanceof App\User)?'User':'Artist' @endphp
								<form action="{{route(checkFollowArtist($artist->id,$type)?'unfollow-artist':'follow-artist')}}" method="post" class="ajax-post">
									<input type="hidden" name="artist_id" value="{{$artist->id}}">
									<input type="hidden" name="type" value="{{$type}}">
									
									<button type="submit" class="view_btn"><i class="fa {{checkFollowArtist($artist->id,$type)?'fa-thumbs-down':'fa-thumbs-up'}}" aria-hidden="true"></i>{{checkFollowArtist($artist->id,$type)?'Unfollow':'Follow'}}</button>
								</form>
							</div>
						</div>
						<!-- artist_profile end -->
						@if(count($artist->mangas)>0)
						<div class="cstm_heading cstm_heading_sml">
							<h3>Published Comics</h3></div>
						<div class="chapter_sc">
							@php $count=0; @endphp
							@foreach($artist->mangas as $manga)
								@include('manga/partials/item')
								@php $count=$count+1; @endphp
								@if($count==10)
									@php break; @endphp
								@endif
							@endforeach
							
							<!-- chapter_bx end -->
							
						</div>
							<div class="btn_sctn">
							@if(count($artist->mangas)>10)<a href="{{route('cms_artist-details',$artist->slug)}}" title="" class="view_btn">View All</a>
							@endif
							<a href="#" title="" class="view_btn" data-toggle="modal" data-target="#order_modal">Order Physical Copy</a></div>
						@endif
					</div>
				</div>
				<div class="col-sm-4 chapter_sec_rght">
					<div class="drctry_ad">
						<figure><!-- comic page side bar -->
							<ins class="adsbygoogle"
							     style="display:inline-block;width:300px;height:250px"
							     data-ad-client="ca-pub-4954418714708367"
							     data-ad-slot="3783972851"></ins></figure>
					</div>
					@include('widgets/newMangas')
				</div>
			</div>
		</div>
	</section>
@include('widgets/latestBlog')

@include('widgets/requestManga')
@endsection

@section('page_script')
	$('#manga_id').on('change', function(e){
	    console.log(e);
	    var manga_id = e.target.value;
	    var url='{{ route('manga-volume-list','manga_id') }}'.replace('manga_id', manga_id);
		$.get(url, function(data) {
		    console.log(data);
		    var manga_volume_id=$('#manga_volume_id');
		    manga_volume_id.empty();
		    manga_volume_id.append('<option value="">Select Volume</option>');
		     $.each(data, function(value, display){
		         manga_volume_id.append('<option value="' + display.id + '" data-value="' + display.total_copies + '">' + display.name + '</option>');
		    });
		});
	});	

@endsection