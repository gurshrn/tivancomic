<form id="search-form" action="{{route('search')}}" method="GET">
   	<div class="dirctory_srch">
        <div class="cstm_heading cstm_heading_sml">
            <h3>Search</h3> </div>
        <div class="search_dirctory">
            <input type="search" placeholder="Enter keywords" maxlength="100" class="form_control" name="keywords">
            <button class="srch_drct" type="submit"> <i class="fa fa-search"></i> </button>
        </div>
    </div>
</form>