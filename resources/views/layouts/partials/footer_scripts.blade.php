@yield('page_script_links')
<script type="text/javascript">
		@if(Session::has('message'))
			$.toast({
			    heading: 'Success',
			    text: '{{Session::get('message')}}',
			    showHideTransition: 'slide',
			    position: 'top-right',
			    icon: 'success',
			});
			@php Session::forget('message'); @endphp
		@endif

		@if(Session::has('error'))
			$.toast({
			    heading: 'Error',
			    text: '{{Session::get('error')}}',
			    showHideTransition: 'slide',
			    position: 'top-right',
			    icon: 'error',
			});
			@php Session::forget('error'); @endphp
		@endif
		@if(Session::has('login'))
			$('#login_modal').modal('show')
			@php Session::forget('login'); @endphp
		@endif
		@if(Session::has('signup'))
			$('#signup_modal').modal('show')
			@php Session::forget('signup'); @endphp
		@endif
		@yield('page_script')
</script>