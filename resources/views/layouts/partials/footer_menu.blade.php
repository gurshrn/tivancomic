<ul class="ftr_nav">
    @foreach($items as $menu_item)
        <li class=" {{url($menu_item->link()) != url()->current()?:'current-menu-item'}}"><a href="{{ $menu_item->link() }}" target="{{$menu_item->target}}">{{ $menu_item->title }}</a></li>
    @endforeach
</ul>