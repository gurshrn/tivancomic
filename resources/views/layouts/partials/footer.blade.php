	<footer>
		<div class="top_ftr">
			<div class="container">
				<div class="row">
					<div class="col-sm-3 ftr_bx ftr_bx1">
						<a href="{{url('/')}}" title="" class="ftr_logo"><img src="{{ asset('storage/'.setting('site.footer_logo'))}}" alt="ftr_logo"></a>
						<p>{{setting('site.footer_description')}}</p>
					</div>
					<div class="col-sm-4 ftr_bx ftr_navigation">
						<h6>Useful Links</h6>
						{!! menu('Useful Links','layouts.partials.footer_menu') !!}
					</div>
					<div class="col-sm-3 ftr_bx ftr_cntct">
						<h6>Contact Info</h6>
						<ul class="ftr_nav">
							@if(!empty(setting('site.address')))
							<li>
								<figure><img src="{{ asset('images/loc_icn.svg')}}" alt="loc_icn"></figure><span>{{setting('site.address')}}</span></li>
							@endif
							@if(!empty(setting('site.phone')))
							<li>
								<figure><img src="{{ asset('images/mob_icn.svg')}}" alt="mob_icn"></figure><a href="tel:{{setting('site.phone')}}" title="">{{setting('site.phone')}}</a></li>
							@endif
							@if(!empty(setting('site.email')))
							<li>
								<figure><img src="{{ asset('images/mail_icn.svg')}}" alt="mail_icn"></figure><a href="mailto:{{setting('site.email')}}" title="">{{setting('site.email')}}</a></li>
							@endif
						</ul>
					</div>
					<div class="col-sm-2 ftr_bx">
						<h6>Follow Us</h6>
						<ul class="ftr_social">
							@if(!empty(setting('site.fb_link')))
							<li><a href="{{setting('site.fb_link')}}" target="_blank" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							@endif
							@if(!empty(setting('site.twitter')))
							<li><a href="{{setting('site.twitter')}}"  target="_blank" title="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							@endif
							@if(!empty(setting('site.linkedin')))
							<li><a href="{{setting('site.linkedin')}}"  target="_blank" title="linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
							@endif
							@if(!empty(setting('site.instagram')))
							<li><a  href="{{setting('site.instagram')}}"  target="_blank" title="instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
							@endif
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!--top_ftr end -->
		<div class="copyright_sctn">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="copyright_inr">
							<p>Copyright © {{ date('Y') }} {{ setting('site.title') }}. All Rights Reserved.</p>
							<p>Powered By - <a href="http://imarkinfotech.com/" title="" target="_blank">iMark Infotech</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
@guest
   @include('auth/login')
   @include('auth/register')
   @include('auth.passwords.email')
	
@endguest

	<!-- jQuery (necessary for JavaScript plugins) -->
	<script src="{{ asset('js/jquery3.3.1.min.js')}}" type="text/javascript"></script>
	<script src="{{ asset('js/jquery.form.js')}}" type="text/javascript"></script>
	<script src="{{ asset('js/popper.min.js')}}" type="text/javascript"></script>
	<script src="{{ asset('js/bootstrap.min.js')}}" type="text/javascript"></script>
	<script src="{{ asset('js/wow.min.js')}}" type="text/javascript"></script>
	<script src="{{ asset('js/jquery-ui.min.js')}}" type="text/javascript"></script>
	<script src="{{ asset('js/navbar-fixed.js')}}" type="text/javascript"></script>
	<script src="{{ asset('js/owl.carousel.min.js')}}" type="text/javascript"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js" type="text/javascript"></script>

	<script src="{{ asset('js/jquery.BlockUI.js')}}" type="text/javascript"></script>

	<!-- Time display -->
    <script src="{{ asset('plugins/timeago/jqueryTimeago_'.Lang::locale().'.js') }}" type="text/javascript"></script>


	<!-- https://github.com/kamranahmedse/jquery-toast-plugin -->
	<script src="{{ asset('js/jquery.toast.js')}}" type="text/javascript"></script>

	<!-- validations -->
	<script src="{{ asset('js/jquery.validationEngine-en.js')}}" type="text/javascript" charset="utf-8"></script>
	<script src="{{ asset('js/jquery.validationEngine.js')}}" type="text/javascript" charset="utf-8"></script>

	<script src="{{ asset('js/custom.js')}}" type="text/javascript"></script>
	<script src="{{ asset('js/app.js')}}" type="text/javascript"></script>
	@hasSection('google-ad')
	<script>
		[].forEach.call(document.querySelectorAll('.adsbygoogle'), function(){
		    (adsbygoogle = window.adsbygoogle || []).push({});
		});
	</script>
	@endif
	@hasSection('chart')	
		<script src="{{asset('js/morris.min.js')}}" type="text/javascript"></script>	
		<script src="{{ asset('js/raphael-min.js')}}" type="text/javascript"></script>

	@endif
	
	@include('layouts/partials/footer_scripts')

	
</body>

</html>