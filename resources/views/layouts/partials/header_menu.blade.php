<ul class="navbar-nav">
    @foreach($items as $menu_item)
        <li class="nav-item {{url($menu_item->link()) != url()->current()?:'current-menu-item'}}"><a class="nav-link" href="{{ $menu_item->link() }}" target="{{$menu_item->target}}">{{ $menu_item->title }}</a></li>
    @endforeach
</ul>