

<div class="col-sm-3 dashboard_lft" id="dashboard_sctn">
	<div id="burger" class="burgerbutton">
		<span></span>
		<span></span>
		<span></span>
	</div>
	<div class="dashboard_sidebar">
		<div class="dash_img">
			<figure><img src="{{asset('storage/').'/'.(empty(Auth::user()->avatar)?Config::get('voyager.user.default_avatar'):Auth::user()->avatar)}}" alt="" class="profile_img"></figure>
			<h5>{{Auth::user()->name}}</h5> </div>
		<ul>
			<li class="{{route('dashboard') != url()->current()?:'dashboard_active'}}"><a href="{{route('dashboard')}}" title="">My Profile</a></li>
			@if(isPublisher() || isAdmin())
			<li class="{{route('publisher-artists') != url()->current()?:'dashboard_active'}}"><a href="{{route('publisher-artists')}}" title="">Artists</a></li>
			@endif
			@if(isArtist() || isPublisher() || isAdmin())
				<li class="{{(route('artist-manga-listing')!= url()->current() && 
				route('manga-volume','edit')!= url()->current() && 
				route('mass-upload')!= url()->current() && 
				route('manga-volume','delete')!= url()->current() &&
				route('manga-chapter','add')!= url()->current() &&
				route('manga-chapter','edit')!= url()->current() &&
				route('manga-chapter','delete')!= url()->current() &&
				route('manga-page')!= url()->current() &&
				 route('manga-volume','add')!= url()->current())?:'dashboard_active'}}"><a class="drop-down {{(route('artist-manga-listing')!= url()->current() && 
				route('manga-volume','edit')!= url()->current() && 
				route('mass-upload')!= url()->current() && 
				route('manga-volume','delete')!= url()->current() &&
				route('manga-chapter','add')!= url()->current() &&
				route('manga-chapter','edit')!= url()->current() &&
				route('manga-chapter','delete')!= url()->current() &&
				route('manga-page')!= url()->current() &&
				 route('manga-volume','add')!= url()->current())?:'open'}}" href="javascript:void(0)" title="">Comic Books <span><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
					<ul class="submenu" style="{{(route('artist-manga-listing')!= url()->current() && 
				route('manga-volume','edit')!= url()->current() && 
				route('mass-upload')!= url()->current() && 
				route('manga-volume','delete')!= url()->current() &&
				route('manga-chapter','add')!= url()->current() &&
				route('manga-chapter','edit')!= url()->current() &&
				route('manga-chapter','delete')!= url()->current() &&
				route('manga-page')!= url()->current() &&
				 route('manga-volume','add')!= url()->current())?:'display: block;'}}">
						<li class="{{route('artist-manga-listing') != url()->current()?:'current_sctn'}}"><a href="{{route('artist-manga-listing')}}" title=""><i class="fa fa-angle-right" aria-hidden="true"></i>Series</a></li>
						<li class="{{route('manga-volume','add') != url()->current()?:'current_sctn'}}"><a href="{{route('manga-volume','add')}}" title=""><i class="fa fa-angle-right" aria-hidden="true"></i>Add Volumes</a></li>
						<li class="{{route('manga-volume','edit') != url()->current()?:'current_sctn'}}"><a href="{{route('manga-volume','edit')}}" title=""><i class="fa fa-angle-right" aria-hidden="true"></i>Edit Volumes</a></li>
						<li class="{{route('manga-volume','delete') != url()->current()?:'current_sctn'}}"><a href="{{route('manga-volume','delete')}}" title=""><i class="fa fa-angle-right" aria-hidden="true"></i>Delete Volumes</a></li>
						
						<li class="{{route('manga-chapter','add') != url()->current()?:'current_sctn'}}"><a href="{{route('manga-chapter','add')}}" title=""><i class="fa fa-angle-right" aria-hidden="true"></i>Add Chapters</a></li>
						<li class="{{route('manga-chapter','edit') != url()->current()?:'current_sctn'}}"><a href="{{route('manga-chapter','edit')}}" title=""><i class="fa fa-angle-right" aria-hidden="true"></i>Edit Chapters</a></li>
						<li class="{{route('manga-chapter','delete') != url()->current()?:'current_sctn'}}"><a href="{{route('manga-chapter','delete')}}" title=""><i class="fa fa-angle-right" aria-hidden="true"></i>Delete Chapters</a></li>
						
						<li class="{{route('manga-page') != url()->current()?:'current_sctn'}}"><a href="{{route('manga-page')}}" title=""><i class="fa fa-angle-right" aria-hidden="true"></i>Pages</a></li>
						<li class="{{route('mass-upload') != url()->current()?:'current_sctn'}}"><a href="{{route('mass-upload')}}" title=""><i class="fa fa-angle-right" aria-hidden="true"></i>Mass Upload</a></li>

						
					</ul>
				</li>
				<li class="{{route('earnings') != url()->current()?:'dashboard_active'}}"><a href="{{route('earnings')}}" title="">View Earning</a></li>
				<li class="{{route('artist-manga-requests') != url()->current()?:'dashboard_active'}}"><a href="{{route('artist-manga-requests')}}" title="">Request Copy</a></li>

			@endif
				<li class="{{(route('follow-manga-list')!= url()->current() && route('follow-artist-list')!= url()->current())?:'dashboard_active'}}"><a class="drop-down {{(route('follow-manga-list')!= url()->current() && route('follow-artist-list')!= url()->current())?:'open'}}" href="javascript:void(0)" title="">Following<span ><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
					<ul class="submenu" style="{{(route('follow-manga-list')!= url()->current() && route('follow-artist-list')!= url()->current())?:'display: block;'}}">
						<li class="{{route('follow-manga-list') != url()->current()?:'current_sctn'}}"><a href="{{route('follow-manga-list')}}" title=""><i class="fa fa-angle-right" aria-hidden="true"></i>Comics</a></li>
						<li class="{{route('follow-artist-list') != url()->current()?:'current_sctn'}}"><a href="{{route('follow-artist-list')}}" title=""><i class="fa fa-angle-right" aria-hidden="true"></i>Artist</a></li>
					</ul>
				</li>
				<li class="{{route('order-history') != url()->current()?:'dashboard_active'}}"><a href="{{route('order-history')}}" title="">Order History</a></li>
				<li class="{{route('series-updates') != url()->current()?:'dashboard_active'}}"><a href="{{route('series-updates')}}" title="">Series Updates</a></li>
		</ul>
	</div>
</div>