<form id="search-form" action="{{route('search')}}" method="GET">
    <div class="input-group">
		<input class="form_control" name="keywords" type="search" value="{{ \Request::get('keywords') }}" placeholder="Enter Comic/ Artist Name">
		<div class="input-group-append">
			<button class="srch_btn" type="submit"> <i class="fa fa-search"></i> </button>
		</div>
	</div>
</form>