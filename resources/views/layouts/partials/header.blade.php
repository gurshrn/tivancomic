<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description"
          content="@yield('meta_description', setting('site.description')) - {{ setting('site.title') }}">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<title>:: {{ setting('site.title') }} - @yield('page_title') ::</title>
	<link rel="icon" href="{{ asset('storage/'.setting('site.site_icon'))}}" type="image/x-icon">
	<link href="{{ asset('css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
	<link href="{{ asset('css/animate.min.css')}}" rel="stylesheet">
	<link href="{{ asset('css/owl.carousel.css')}}" rel="stylesheet">
	<link href="{{ asset('css/jquery-ui.css')}}" rel="stylesheet">
	<link href="{{ asset('css/jquery.toast.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('css/validationEngine.jquery.css')}}" type="text/css"/>
	<link rel="stylesheet" href="{{ asset('css/template.css')}}" type="text/css"/>
	<link href="{{ asset('css/style.css')}}" rel="stylesheet">  
<!-- Open Graph -->
	<meta property="og:site_name" content="{{ setting('site.title') }}" />
	<meta property="og:title" content="@yield('meta_title')" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="{{ url('/') }}" />
	<meta property="og:image" content="@yield('meta_image', url('/') . '/images/apple-touch-icon.png')" />
	
	<meta property="og:description" content="@yield('meta_description', setting('site.description'))" />
	@if (setting('site.google_analytics_tracking_id'))
		<!-- Google Analytics (gtag.js) -->
		<script type="text/javascript" async src="https://www.googletagmanager.com/gtag/js?id={{ setting('site.google_analytics_tracking_id') }}"></script>
		<script>
				window.dataLayer = window.dataLayer || [];
				function gtag(){dataLayer.push(arguments);}
				gtag('js', new Date());
				gtag('config', '{{ setting('site.google_analytics_tracking_id') }}');
		</script>
	@endif

    @if (setting('admin.google_recaptcha_site_key'))
        <script type="text/javascript" src='https://www.google.com/recaptcha/api.js' async defer></script>
        <script>
            function setFormId(formId) {
                window.formId = formId;
            }
            function onSubmit(token) {
                document.getElementById(window.formId).submit();
            }
        </script>
     @endif
    @hasSection('dfp-ad')
	<script async='async' type="text/javascript" src='https://www.googletagservices.com/tag/js/gpt.js'></script>
	<script>
	  var googletag = googletag || {};
	  googletag.cmd = googletag.cmd || [];
	</script>
	@endif
	@hasSection('google-new')
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	@endif
	

@yield('css')
 <meta name="_token" content="{!! csrf_token() !!}"/>
	@hasSection('google-ad')
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<!-- <script>
	  (adsbygoogle = window.adsbygoogle || []).push({
	    google_ad_client: "ca-pub-4954418714708367",
	    enable_page_level_ads: true
	  });
	</script> -->
	@endif


</head>

<body class="{{\Route::current()->getName()}}">
	<div id="preloader">
		<div class="wrapper">
			<div class="cssload-loader"></div>
		</div>
	</div>
	<header>
		<div class="top_header">
			<div class="container">
				<div class="top_header_innr">
					<div class="tp_lft">
						<!-- <div class="tp_bx">
							<a href="tel:{{setting('site.phone')}}" title="">
								<figure><i class="fa fa-phone" aria-hidden="true"></i></figure><span>{{setting('site.phone')}}</span></a>
						</div>
						<div class="tp_bx">
							<a href="mailto:{{setting('site.email')}}" title="">
								<figure><i class="fa fa-envelope-o" aria-hidden="true"></i></figure><span>{{setting('site.email')}}</span></a>
						</div> -->
					</div>
					@guest
						<div class="tp_rght"> <a href="#" title="" class="login_btn" data-toggle="modal" data-target="#login_modal">Login</a> <a href="#" title="" class="login_btn" data-toggle="modal" data-target="#signup_modal">Sign Up</a> </div>
					@else
					<div class="account-wrap">
						<div class="account-item clearfix js-item-menu">
							<div class="image"> <img src="{{asset('storage/').'/'.(empty(Auth::user()->avatar)?Config::get('voyager.user.default_avatar'):Auth::user()->avatar)}}" alt="profile_img" class="profile_img"> </div>
							<div class="content">
								<a class="js-acc-btn" href="#"></a>
							</div>
							<div class="account-dropdown js-dropdown">
								<div class="info clearfix">
									<div class="image">
										<a href="#"> <img src="{{asset('storage/').'/'.(empty(Auth::user()->avatar)?Config::get('voyager.user.default_avatar'):Auth::user()->avatar)}}" class="profile_img" alt="profile_img"> </a>
									</div>
									<div class="content">
										<h5 class="name">
                                        <a href="{{route('dashboard')}}">{{ Auth::user()->name }}</a>
                                    </h5> <span class="email">{{ Auth::user()->email }}</span> </div>
								</div>
								<div class="account-dropdown__body">
									<div class="account-dropdown__item">
										<a href="{{route('dashboard')}}"> <i class="fa fa-user" aria-hidden="true"></i>Account</a>
									</div>
									@if(Auth::user()->role=='User')
									<div class="account-dropdown__item">
										<a href="{{route('series-updates')}}"> <i class="fa fa-bell" aria-hidden="true"></i>Series Updates</a>
									</div>
									@endif
									<div class="account-dropdown__item">
										<a href="{{route('settings')}}"> <i class="fa fa-cog" aria-hidden="true"></i>Setting</a>
									</div>
								</div>
								<div class="account-dropdown__footer">
									<a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"> <i class="fa fa-power-off" aria-hidden="true"></i>Logout</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
								</div>
							</div>
						</div>
					</div>
					@endguest
				</div>
			</div>
		</div>
		<!-- top_header end -->
		<div class="btm_header">
			<div class="container">
				<nav class="navbar navbar-expand-lg">
					<a class="navbar-brand" href="{{url('/')}}"><img src="{{ asset('storage/'.setting('site.logo'))}}" alt="logo"></a>
					<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#main-menu" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
					<div class="collapse navbar-collapse" id="main-menu">
						{!! menu('Header Menu','layouts.partials.header_menu') !!}
						<div class="search_sec">
							@include('layouts.partials.search')
						</div>
						<!-- search_sec end -->
					</div>
				</nav>
			</div>
		</div>
		<!-- btm_header end -->
	</header>
	