   <div class="modal fade" id="login_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title" id="exampleModalLabel">Login</h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ route('login') }}" class="ajax-post">
                         @csrf

                        <p>
                            <input type="email" placeholder="Email*" name="email" class="form_control{{ $errors->has('email') ? ' is-invalid' : '' }}  validate[required,custom[email]]" value="{{ old('email') }}" maxlength="50" >
                             @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif </p>
                        <p>
                            <input type="password" placeholder="Password" class="form_control{{ $errors->has('password') ? ' is-invalid' : '' }}  validate[required]" name="password" maxlength="30">
                              @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif </p>
                        <div class="frgt_pwd"><a href="javascript:;" data-dismiss="modal" title="" data-toggle="modal" data-target="#forgot_modal">Forgot Password</a></div>
                        <div class="login_loader login_btn1">
                            <input type="submit" value="login" class="view_btn"> </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


                       <!--  <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div> -->

                      