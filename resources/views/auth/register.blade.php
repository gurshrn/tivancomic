<div class="modal fade" id="signup_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title" id="exampleModalLabel1">Sign Up</h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                </div>
                <div class="modal-body">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class=""> <a class="nav-link active" id="usr-tab" data-toggle="tab" href="#" role="tab" aria-controls="usr" aria-selected="true" data-value="User">User</a> </li>
                        <li class=""> <a class="nav-link" id="artist-tab" data-toggle="tab" href="#" role="tab" aria-controls="artist" aria-selected="false" data-value="Artist">Artist</a> </li>
                        <li class=""> <a class="nav-link" id="publisher-tab" data-toggle="tab" href="#" role="tab" aria-controls="publisher" aria-selected="false" data-value="Publisher">Publisher</a> </li>
                    </ul>
                        <form method="POST" action="{{ route('register') }}">
                                 @csrf
                                 <input type="hidden" name="role" value="User" id="register_role">
                                <p>
                                    <input type="text" placeholder="Name*" class="form_control cptl{{ $errors->has('name') ? ' is-invalid' : '' }} validate[required,custom[onlyLetterSp]]" name="name" value="{{ old('name') }}" maxlength="50" autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif </p>
                                <p>
                                    <input type="email" placeholder="Email*" class="form_control{{ $errors->has('email') ? ' is-invalid' : '' }} validate[required,custom[email]]" name="email" maxlength="50" value="{{ old('email') }}" >

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif </p>
                                <p>
                                    <input type="password" placeholder="Password" maxlength="30" class="form_control{{ $errors->has('password') ? ' is-invalid' : '' }} validate[required]" name="password" id="password" >

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif </p>
                                <p>
                                    <input type="password" placeholder="Confirm Password" maxlength="30" class="form_control validate[required,equals[password]]" name="password_confirmation" > </p>
                                <div class="remembr_sctn">
                                    <input type="checkbox" id="rd1" name="agreement" class="validate[required]">
                                    <label for="rd1">I agree with Terms and Conditions</label>
                                </div>
                                <div class="login_loader login_btn1">
                                    <input type="submit" value="Create Account" class="view_btn"> </div>
                            </form>
                    <!-- <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="usr" role="tabpanel" aria-labelledby="usr-tab">
                        
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>


