@extends('layouts.app')
@section('page_title', $page->title)
@section('meta_title', $page->title)
@section('meta_description', $page->meta_description)
@section('content')
<section class="about_banner" style="background:url({{ filter_var($page->image, FILTER_VALIDATE_URL) ? $page->image : Voyager::image( $page->image ) }}) no-repeat;">
		<div class="container">
			<div class="about_caption wow fadeInDown" data-wow-delay="0.2s" data-wow-duration="1000ms">
				<h2>{{$page->title}}</h2> 
			</div>
		</div>
	
</section>
	<!--about_banner end -->
	@if(!empty($page->body))
		{!! $page->body !!}
				
	@endif
	{!! $page->block_content !!}
@endsection
