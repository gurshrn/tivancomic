@extends('layouts.app')
@section('page_title', 'Contact Us')
@section('meta_title', 'Contact Us')

@section('content')
<section class="about_banner" style="background:url({{ filter_var($page->image, FILTER_VALIDATE_URL) ? $page->image : Voyager::image( $page->image ) }}) no-repeat;">
		<div class="container">
			<div class="about_caption wow fadeInDown" data-wow-delay="0.2s" data-wow-duration="1000ms">
				<h2>{{$page->title}}</h2> </div>
		</div>
	
	</section>
	<!--about_banner end -->
	<section class="contact_sctn">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="cstm_heading">
						<h3>{{$page->title}}</h3></div>
				</div>
			</div>
			<div class="row contact_sctn_info">
				@if(!empty(setting('site.address')))
				<div class="col-sm-4">
					<div class="contact_bx">
						<figure><img src="{{asset('images/contact_loc.svg')}}" alt="contact_loc"></figure>
						<div class="adrs_cntct">
							<h5>Address</h5>
							<p>{{setting('site.address')}}</p>
						</div>
					</div>
				</div>
				@endif
				@if(!empty(setting('site.phone')))
				<div class="col-sm-4">
					<div class="contact_bx">
						<figure><img src="{{asset('images/contact_mob.svg')}}" alt="contact_mob" class="mob_icn"></figure>
						<div class="adrs_cntct">
							<h5>telephone</h5>
							<p><a href="tel:{{setting('site.phone')}}" title="">{{setting('site.phone')}}</a></p>
						</div>
					</div>
				</div>
				@endif
				@if(!empty(setting('site.email')))
				<div class="col-sm-4">
					<div class="contact_bx">
						<figure><img src="{{asset('images/contact_mail.svg')}}" alt="contact_mail" class="mail_icn"></figure>
						<div class="adrs_cntct">
							<h5>Email</h5>
							<p><a href="mailto:{{setting('site.email')}}" title="">{{setting('site.email')}}</a></p>
						</div>
					</div>
				</div>
				@endif
			</div>
		</div>
	</section>
	<!-- contact_sctn end -->
	<section class="contact_form">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="cstm_heading">
						<h3>Get in Touch</h3></div>
					<div class="contact_form_sec">
						<form action="{{route('contact-post')}}" method="post">
							@csrf
					 <div class="medium_inpt">
						 <p><label>Name<span class="req">*</span></label><input type="text" placeholder="Lorem" class="form_control cptl validate[required]" maxlength="40" name="name"></p>
						  <p><label>Email<span class="req">*</span></label><input type="eamil" placeholder="@gmail.com" class="form_control validate[required]" maxlength="50" name="email"></p>
						  <p><label>Phone Number<span class="req">*</span></label><input type="text" placeholder="123456789" class="form_control validate[required]" maxlength="25" name="phone"></p>
						</div>
						<div class="full_inpt">
							<label>Category<span class="req">*</span></label>
						 <p class="slct"><select class="placeholder1 form_control validate[required]" name="category"><option>General</option><option>General1</option><option>General2</option></select></p>
						</div>
						<div class="full_inpt">
							<label>Message</label>
						 <textarea placeholder="Type your message" class="form_control" name="message"></textarea>
						</div>
						<div class="sbmt_btn_sctn"><input type="submit" value="SUBMIT" class="view_btn"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- contact_form end -->
	{!! $page->block_content !!}

@endsection
