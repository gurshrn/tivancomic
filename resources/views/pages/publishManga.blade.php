@extends('layouts.app')
@section('page_title', $page->title)
@section('meta_title', $page->title)
@section('meta_description', $page->meta_description)

@section('content')
<section class="about_banner" style="background:url({{ filter_var($page->image, FILTER_VALIDATE_URL) ? $page->image : Voyager::image( $page->image ) }}) no-repeat;">
		<div class="container">
			<div class="about_caption wow fadeInDown" data-wow-delay="0.2s" data-wow-duration="1000ms">
				<h2>{{$page->title}}</h2> </div>
		</div>
	
	</section>
	<!--about_banner end -->
	<section class="chapter_sec publish_page">
		<div class="container">
			<div class="row">
				<div class="col-sm-8 chapter_sec_lft">
					<div class="chapter_sec_main">
						{!! $page->body !!} <a href="{{route('upload-manga')}}" title="" class="view_btn pblsh_btn">Publish Comics</a> </div>
				</div>
				<div class="col-sm-4 chapter_sec_rght">
					@include('widgets/newMangas')
				</div>
			</div>
		</div>
	</section>
	{!! $page->block_content !!}
@endsection
