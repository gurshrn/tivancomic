@extends('layouts.app')
@section('page_title')
{{$page->title}}
@endsection
@section('dfp-ad', true)
@section('page_script')
	//$.ajax({
          // type: "GET",
           //url: "{{route('homepagecontent')}}",
           //async:true,
           //success: function(data)
          // {
                //$('#latestchapters').html(data.latestChapter);
               // $('.chapter_sec_rght').html(data.mostPopular);
                //$('#latestchapters').html(data.recentMangas);
           //}
    //});
	$(document).ready(function(){
	 var width = $(window).width();
  	//Homepage_Desktop_728x90_BTF:
		googletag.cmd.push(function() {	   
			googletag.defineSlot('/21770139633/Homepage_Desktop_728x90_BTF', [ [970, 90], [728, 90], [320, 50]], 'div-gpt-ad-1552576732423-0').addService(googletag.pubads());
				googletag.pubads().enableSingleRequest();
				googletag.enableServices();
		  });
	//Homepage_Mobile_320x100_BTF:
		 googletag.cmd.push(function() {	  
			googletag.defineSlot('/21770139633/Homepage_Mobile_320x100_BTF', [[180, 150], [320, 100], [300, 250], [320, 50]], 'div-gpt-ad-1552576753484-0').addService(googletag.pubads());
			googletag.pubads().enableSingleRequest();
			googletag.enableServices();
		 });
	//Homepage_Desktop_300x600_Left:	
	  googletag.cmd.push(function() {
			googletag.defineSlot('/21770139633/Homepage_Desktop_300x600_Left', [[300, 600], [120, 600], [160, 600]], 'div-gpt-ad-1552576808869-0').addService(googletag.pubads());
			googletag.pubads().enableSingleRequest();
			googletag.enableServices();
	  });

		//Homepage_Desktop_160x600_Right
		  googletag.cmd.push(function() {	
			googletag.defineSlot('/21770139633/Homepage_Desktop_160x600_Right', [[120, 600], [160, 600]], 'div-gpt-ad-1552576850307-0').addService(googletag.pubads());
			googletag.pubads().enableSingleRequest();
			googletag.enableServices();
	  });
	//Homepage_Desktop_728x90_ATF:
		 googletag.cmd.push(function() {  
				googletag.defineSlot('/21770139633/Homepage_Desktop_728x90_ATF', [[320, 50], [970, 90], 'fluid', [728, 90]], 'div-gpt-ad-1552576683175-0').addService(googletag.pubads());
				googletag.pubads().enableSingleRequest();
				googletag.enableServices();
		  });
	//Homepage_Mobile_320x100_ATF:
		 googletag.cmd.push(function() {
			googletag.defineSlot('/21770139633/Homepage_Mobile_320x100_ATF', [[320, 50], [180, 150], [320, 100], [300, 250]], 'div-gpt-ad-1552576706334-0').addService(googletag.pubads());
			googletag.pubads().enableSingleRequest();
			googletag.enableServices();
		});
		googletag.cmd.push(function() {
		googletag.defineSlot('/21770139633/Comics_Desktop_970x250_BTF_2', [[728, 90], [600, 120], [970, 250], [970, 90], [468, 60]], 'div-gpt-ad-1552750835217-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
		});
		googletag.cmd.push(function() {
			googletag.defineSlot('/21770139633/Homepage_Mobile_320x100_BTF_2', [[180, 150], [300, 250], [320, 100], [320, 50]], 'div-gpt-ad-1552751518693-0').addService(googletag.pubads());
    	googletag.pubads().enableSingleRequest();
    	googletag.enableServices();
		});
googletag.cmd.push(function() { 
	if(width >= 768){
		googletag.display('div-gpt-ad-1552576683175-0'); 
		googletag.display('div-gpt-ad-1552576732423-0');
		googletag.display('div-gpt-ad-1552576808869-0');
	} 
	if(width >= 320 && width <= 767){	
		googletag.display('div-gpt-ad-1552576706334-0'); 
		googletag.display('div-gpt-ad-1545106665699-0');
		googletag.display('div-gpt-ad-1552751518693-0');
	}	
});
});
@endsection
@section('content')
@include('widgets/slider')
<style>
@media only screen and (min-width: 767px) {
        /* styles for browsers larger than 960px; */
				#div-gpt-ad-1552576706334-0,#div-gpt-ad-1552576753484-0,#div-gpt-ad-1552751518693-0{
					display:none!important;
				}
}
@media only screen and (min-width: 320px) and  (max-width: 768px){
        /* styles for browsers larger than 960px; */
				#div-gpt-ad-1552576683175-0,#div-gpt-ad-1552576732423-0,#div-gpt-ad-1552750835217-0{
					display:none!important;
				}
}
</style>
	<section class="home_recent_sec">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center" style="padding:20px 0px;">
							<!-- Home Page Recent Banner -->
							<!-- <ins class="adsbygoogle"
							     style="display:inline-block;width:728px;height:90px"
							     data-ad-client="ca-pub-4954418714708367"
							     data-ad-slot="5950013338"></ins> -->
							<!-- /21770139633/Homepage_Desktop_728x90_ATF -->
							<div id='div-gpt-ad-1552576683175-0'>
							</div>
							<div id='div-gpt-ad-1552576706334-0'>
							</div>
				</div>
				<div class="col-sm-12">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="recent_mid_sc">
								<div class="recent_slider">
									<div class="rcnt_bx">
										<div class="cstm_heading">
											<h3>Recently Added</h3></div>
										<div class="view_btn_sctn"><a href="{{route('manga-directory')}}" title="" class="view_btn">View All</a></div>
									</div>
									<div id="latest_slide" class="owl-carousel owl-theme">
										@forelse(getMangaRecent(12) as $manga)
										<div class="item">
											<div class="item_bx">
												<a href="{{route('cms_manga_detail',$manga->slug)}}" title="{{$manga->title}}">
													<figure><img src="{{empty($manga->cover)?'images/logo.png':asset('storage/covers/').'/'.$manga->cover}}" alt="pro1"></figure> <span>{{$manga->title}}</span> </a>
											</div>
										</div>
										@empty
										<div class="item">
											<div class="item_bx">
													<span>No Data Found</span> 
											</div>
										</div>	
										@endforelse
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12 text-center" style="padding:20px 0px;">
							<!-- Home Page Recent Banner -->
							<!-- <ins class="adsbygoogle"
							     style="display:inline-block;width:728px;height:90px"
							     data-ad-client="ca-pub-4954418714708367"
							     data-ad-slot="5950013338"></ins> -->
								 <!-- /21770139633/Homepage_Desktop_728x90_BTF -->
							 <div id='div-gpt-ad-1552576732423-0'>
							</div>
							
							<!--//Homepage_Mobile_320x100_BTF: -->
							<div id='div-gpt-ad-1552576753484-0'>
							</div>
				</div>
			</div>
		</div>
	</section>
	<!-- home_recent_sec end -->
	<section class="chapter_sec">
		<div class="container">
			<div class="row">
				<div class="col-sm-9 chapter_sec_lft">
					<div class="chapter_sec_main">
						<div class="cstm_heading">
							<h3>Featured Comics</h3></div>
							@include('widgets.featuredComics')
						<!-- <div id="latestchapters">
							<div class="mocka-container">
							  <span class="mocka-media"></span>
							  <span class="mocka-heading"></span>
							  <span class="mocka-text"></span>
							</div>
						</div> -->
					</div>
					<div class="ad_sctn_cstm"><!-- Home Page Bottom Ad -->
						<!-- <ins class="adsbygoogle"
						     style="display:inline-block;width:970px;height:90px"
						     data-ad-client="ca-pub-4954418714708367"
						     data-ad-slot="4537817178"></ins> -->
							 <!-- /21770139633/Homepage_Desktop_728x90_BTF -->
							<div id='div-gpt-ad-1552750835217-0'>
							</div>
							
							<!--//Homepage_Mobile_320x100_BTF: -->
							<div id='div-gpt-ad-1552751518693-0'>
							</div>
					</div>
					<div class="feature_btm">
						@foreach($page->block_content as $block)
					        @if (!empty($block->html))
					            @php echo (string)$block->html @endphp
					        @else
					            <div class="page-block">
					                <div class="callout alert">
					                    <div class="grid-container column text-center">
					                        <h2><< !! Warning: Missing Block !! >></h2>
					                    </div>
					                </div>
					            </div>
					        @endif
					    @endforeach
					</div>
				</div>
				<div class="col-sm-3 chapter_sec_rght">
					@include('widgets.mostPopular')
					<!-- <div class="mocka-container" style="margin-bottom:200px;">
						  <span class="mocka-media"></span>
						  <span class="mocka-heading"></span>
						  <span class="mocka-text"></span>
						</div> -->
				</div>
			</div>
		</div>
		<div class="side_img_sctn" style="background:url(images/side_img.png) no-repeat;"></div>
	</section>
	<!-- chapter_sec end -->
	@include('widgets/latestBlog')
@endsection
