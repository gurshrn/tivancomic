<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class MangaRequest extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'manga_id', 'user_id', 'manga_volume_id', 'address', 'message'
    ];

    public function user() {
        return $this->belongsTo('App\User','user_id')->first();
    }

    public function manga() {
        return $this->belongsTo('App\Models\Manga','manga_id');
    }
    
    public function volume() {
        return $this->belongsTo('App\Models\MangaVolume','manga_volume_id');
    }

    public static function artistCheck($user=null) {
        if($user)
            return  self::whereHas('manga', function($q)use($user) {
                        $q->where('user_id',$user->id);
                    });
        else
            return  self::whereHas('manga', function($q) {
                        $q->where('user_id',\Auth::user()->id);
                    });        
    }

    public static function mangaCheckBySlug($slug=null) {
        if($slug)
            return  self::whereHas('manga', function($q)use($slug) {
                        $q->where('slug',$slug);
                    });
        else
            return  self::has('manga');        
    }
    
}
