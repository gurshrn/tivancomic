<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\SlugHelper;
use Laravel\Scout\Searchable;
use TCG\Voyager\Facades\Voyager;
use Illuminate\Support\Facades\Auth;


class Manga extends Model
{
    use Searchable;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'title', 'user_id', 'artist', 'category_id', 'cover', 'release_year', 'release_status', 'description', 'status', 'status_change','slug','views','ratings','artist_id','is_featured'
    ];

    public function author() {
        if($this->user()->status=='Blocked')
            return $this->belongsTo('App\User','user_id')->where('status','Verified');
        if($this->artist_id>0)
            return $this->belongsTo('App\Models\Artist','artist_id');
        else 
            return $this->belongsTo('App\User','user_id')->where('status','Verified');
    }

    public function user() {
        return $this->belongsTo('App\User')->first();
    }
    public function artists() {
        return $this->belongsTo('App\Models\Artist','artist_id')->first();
    }

    // public function category() {
    //     return $this->belongsTo('App\Models\Category')->first();
    // }
    
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function volumes() {
        return $this->hasMany('App\Models\MangaVolume');
    }

    public function chapters() {
        return $this->hasMany('App\Models\MangaChapter');
    }
    public function chapterRead() {
        return $this->hasMany('App\Models\MangaChapter','manga_id')->has('pages')->orderBy('manga_volume_id')->orderBy('number');
    }
    public function latestchapters() {
        return $this->hasMany('App\Models\MangaChapter','manga_id')->has('pages')->latest();
    }

    public function pages() {
        return $this->hasMany('App\Models\MangaPage');
    }

    public function mangaViews() {
        return $this->hasMany('App\Models\MangaView');
    }
   
    public function latestpage() {
        return $this->hasOne('App\Models\MangaPage','manga_id')->latest();
    }

    public function save(array $options = [])
    {
        $slugHelper=new SlugHelper(\App\Models\Manga::class);
        if(empty($this->id)) {
            $this->slug = $slugHelper->createSlug($this->title,0);
            if (!$this->user_id && Auth::user()) {
                $this->user_id = Auth::user()->id;
            }
            $this->status_change = date('Y-m-d H:i:s');
        }
        else{
            $ratingsValue=\App\Models\MangaRating::where('manga_id',$this->id);
            $this->slug = $slugHelper->createSlug($this->title,$this->id);
            $this->views=count($this->mangaViews);
            $this->ratings=($ratingsValue->count()>0)?($ratingsValue->sum('rating')/$ratingsValue->count()):'0';
        }
        
        if (count(explode('-', $this->release_year))>1) {
            $this->release_year = date('Y',strtotime($this->release_year));
        }
        
        parent::save();
    }
    
    public function mangaRatings() {
        return $this->hasMany('App\Models\MangaRating');
    }


    public function mangaRatingCalculate() {
        return $this->mangaRatings->count()==0?0:$this->mangaRatings->sum('rating')/$this->mangaRatings->count();
    }

    public function mangaRequests() {
        return $this->hasMany('App\Models\MangaRequest');
    }

    public function getArtistAttribute($value){
        if($this->artist_id>0)
            return $this->artists()?$this->artists()->name:'';
        else
            return $this->user()->name;
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();

        unset($array['manga_views']);
        $array['artist'] = $this->artist;
        $array['manga_views_count'] = count($this->mangaViews);
        return $array;
    }
 
}
