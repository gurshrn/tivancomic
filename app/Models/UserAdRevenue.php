<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class UserAdRevenue extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date', 'user_id','manga_id','ad_clicks','revenue'
    ];

    public function manga() {
        return $this->belongsTo('App\Models\Manga','manga_id');
    }

    public function publisher() {
        return $this->belongsTo('App\User','user_id')->where('status','Verified');
    }

   
    public function user() {
        return $this->belongsTo('App\User')->where('status','Verified');
    }

    public function save(array $options = [])
    {
        $adRevenue=AdRevenue::where('date',$this->date)->first();
        $this->revenue = (double)($this->ad_clicks*($adRevenue->user_part/$adRevenue->ad_clicks));
           
        parent::save();
    }

    public function monthlyStat()
    {
        $stat=$this->select(DB::raw('year(date) as year,month(date) as month,sum(ad_clicks) as total_ad_clicks,sum(revenue) as total_revenue'))
                 ->groupBy(DB::raw('year(date)'),DB::raw('month(date)'))->where(DB::raw('year(date)'),date('Y'));
        if(!isAdmin()){
            $stat=$stat->where('user_id',\Auth::user()->id);
        }
        return $stat;
    }
}
