<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MangaPage extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'manga_id', 'manga_chapter_id', 'page_image', 'page_no'
    ];


    public function chapter() {
        return $this->belongsTo('App\Models\MangaChapter','manga_chapter_id')->first();
    }
	
	public function manga() {
        return $this->belongsTo('App\Models\Manga','manga_id')->first();
    }


   
}
