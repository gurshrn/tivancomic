<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;


class FollowManga extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'artist_id', 'user_id', 'manga_id'
    ];

    public function user() {
        return $this->belongsTo('App\User')->first();
    }

    public function manga() {
        return $this->belongsTo('App\Models\Manga')->first();
    }
    
    public function artist() {
        return $this->belongsTo('App\User','artist_id','id');
    }
}
