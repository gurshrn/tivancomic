<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MangaRating extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','manga_id','rating','review'
    ];


    public function manga() {
        return $this->belongsTo('App\Models\Manga');
    }
	
	public function user() {
        return $this->belongsTo('App\User');
    }
}
