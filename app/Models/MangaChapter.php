<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\SlugHelper;

class MangaChapter extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','manga_id','slug',
        'manga_volume_id','number','release_date'
    ];


    public function manga() {
        return $this->belongsTo('App\Models\Manga')->first();
    }
    
    public function volume() {
        return $this->belongsTo('App\Models\MangaVolume','manga_volume_id')->first();
    }
    
    public function pages() {
        return $this->hasMany('App\Models\MangaPage');
    }

    public function latestpage() {
        return $this->hasOne('App\Models\MangaPage')->latest();
    }

    public function save(array $options = array())
    {
        $slugHelper=new SlugHelper(\App\Models\MangaChapter::class);
        if(empty($this->id)) {
            $this->slug = $slugHelper->createSlug($this->manga()->slug.' '.$this->name,0);
        }
        else
            $this->slug = $slugHelper->createSlug($this->manga()->slug.' '.$this->name,$this->id);
            
        return parent::save($options);
    }
  
}
