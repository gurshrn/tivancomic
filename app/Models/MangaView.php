<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

Use App\Models\Manga;

class MangaView extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','manga_id','ip','slug','url','session_id','agent'
        ];


    public function manga() {
        return $this->belongsTo('App\Models\Manga');
    }
	
	public function user() {
        return $this->belongsTo('App\User');
    }

    public static function createViewLog($manga) {
        $mangaViews = MangaView::where('slug', $manga->slug)->where('session_id', \Request::getSession()->getId())->first();
        if(!$mangaViews)
            $mangaViews= new MangaView();
        $mangaViews->manga_id = $manga->id;
        $mangaViews->slug = $manga->slug;
        $mangaViews->url = \Request::url();
        $mangaViews->session_id = \Request::getSession()->getId();
        $mangaViews->user_id = \Auth::check()?\Auth::user()->id:null;
        $mangaViews->ip = \Request::getClientIp();
        $mangaViews->agent = \Request::header('User-Agent');
        $mangaViews->save();

        Manga::where('id', $manga->id)->update(['views'=>count($manga->mangaViews)]);
    }



}
