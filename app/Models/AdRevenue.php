<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdRevenue extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date', 'ad_clicks','total_revenue','admin_part','user_part'
    ];

    public function save(array $options = [])
    {
        $this->admin_part =(double)($this->total_revenue*(100-setting('site.revenue_user_part'))/100);
        $this->user_part =(double)($this->total_revenue*setting('site.revenue_user_part')/100);
               
        parent::save();
    }
}
