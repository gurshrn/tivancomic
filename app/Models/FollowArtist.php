<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;


class FollowArtist extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'artist_id', 'user_id','type'
    ];

    public function user() {
        return $this->belongsTo('App\User')->first();
    }

    public function artist() {
        if($this->type=='User')
            return $this->belongsTo('App\User','artist_id','id');
        else
            return $this->belongsTo('App\Models\Artist','artist_id','id');
        
    }
}
