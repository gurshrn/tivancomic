<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\SlugHelper;
use Laravel\Scout\Searchable;

class Artist extends Model
{
    use Searchable;
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','dob','gender','bio','avatar','slug','user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function mangas() {
        return $this->hasMany('App\Models\Manga')->with('chapters','mangaRatings')->withCount('mangaViews')->where('status','Published')->has('pages')->latest();
    }

    public function publisher() {
        return $this->belongsTo('App\User','user_id')->where('status','Verified');
    }

   
    public function user() {
        return $this->belongsTo('App\User')->where('status','Verified')->first();
    }

    public function save(array $options = [])
    {
        $slugHelper=new SlugHelper(\App\Models\Artist::class);
        $user=$this->user();
        if(empty($this->id)) {
            $this->slug = $slugHelper->createSlug($user->slug.' '.$this->name,0);
        }
        else
            $this->slug = $slugHelper->createSlug($user->slug.' '.$this->name,$this->id);
            
        parent::save();
    }
}
