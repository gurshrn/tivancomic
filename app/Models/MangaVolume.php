<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MangaVolume extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','manga_id','total_copies'
    ];


    public function manga() {
        return $this->belongsTo('App\Models\Manga');
    }
	
	public function chapters() {
        return $this->hasMany('App\Models\MangaChapter');
    }

    public function mangaRequests() {
        return $this->hasMany('App\Models\MangaRequest');
    }
    public function chapter_all() {
        return $this->hasMany('App\Models\MangaChapter','manga_volume_id','id')->orderBy('number')->get();
    }

    public static function artistCheck($user=null) {
        if($user)
            return  self::whereHas('manga', function($q)use($user) {
                        $q->where('user_id',$user->id);
                    });
        else
            return  self::whereHas('manga', function($q) {
                        $q->where('user_id',\Auth::user()->id);
                    });        
    }

}
