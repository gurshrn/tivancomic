<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SeriesUpdate extends Model
{
   
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'user_id', 'manga_id', 'manga_chapter_id', 'is_seen'
    ];


    public function user() {
        return $this->belongsTo('App\User')->first();
    }

    public function chapter() {
        return $this->belongsTo('App\Models\MangaChapter','manga_chapter_id')->first();
    }
	
	public function manga() {
        return $this->belongsTo('App\Models\Manga','manga_id')->first();
    } 

    public static function createSeriesUpdate($manga_id,$chapter_id) {
        $manga=Manga::where('id',$manga_id)->first();
        $manga_users=collect(FollowManga::where('manga_id',$manga_id)->select('user_id')->get()->toArray());
        $artist_users=FollowArtist::where('artist_id',$manga->user_id)->where('type','User')->select('user_id')->get()->toArray();
        $artist_artists=FollowArtist::where('artist_id',$manga->artist_id)->where('type','Artist')->select('user_id')->get()->toArray();
        $users=$manga_users->merge($artist_users)->merge($artist_artists)->unique();
        
        foreach ($users as $user) {
            $update = SeriesUpdate::where('manga_id', $manga_id)//->where('manga_chapter_id', $chapter_id)
            ->where('user_id',$user['user_id'])->first();
            if(!$update)
                $update= new SeriesUpdate();
            elseif($update->manga_chapter_id!=$chapter_id && $update->is_seen)
                $update= new SeriesUpdate();
            else
                continue;
            $update->manga_id = $manga_id;
            $update->manga_chapter_id = $chapter_id;
            $update->user_id = $user['user_id'];
            $update->is_seen = 0;
            $update->save();
        }
    }  
}
