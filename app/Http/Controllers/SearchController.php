<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SearchController extends Controller
{
   protected $searchableModels = [];

    public function __construct()
    {
        $this->searchableModels = self::getSearchableModels();
    }

    public function index(Request $request)
    {
        $searchString = $request->input('keywords');

        if (empty($this->searchableModels) || is_null($this->searchableModels)) {
            return view('search.listing', [
                'resultCollections' => [],
            ]);
        }
        $searchResults= array();
        foreach ($this->searchableModels as $model) {
            $result = $model::search($searchString)->get();
            $modelPath = explode('\\', strtolower($model));
            if(end($modelPath)=='artist'){
               $users=\App\User::has('mangas')->where('role_id','2')->where('status','Verified')->where('is_artist','1')->where('name',$searchString)->get(); 
               $result=$result->merge($users);    
            }
            if(count($result)>0){
                // Add Model Slug Prefix
                // foreach ($result as $item) {
                //     if (!empty($item->slug) && !empty($item->getTable())) {
                //     	if($item->getTable()=='mangas')
                //     	{
                //     		$item->slug=route('cms_manga_detail',$item->slug);
                //     	}
                //         elseif($item->getTable()=='posts')
                //     	{
                //     		$item->slug=route('blog-detail',$item->slug);
                //     	}
                //     	else{
                //     		$item->slug=route('artist-profile',$item->slug);
                //     	}
                //     }
                // }

                $searchResults[end($modelPath)] = $result;
            }
        }
        return view('search.listing', [
            'resultCollections' => $searchResults,
        ]);
    }

    /**
     * Filters our duplicates and retrieves an array of
     * searchable models from our configuration file
     * @return array
     */
    public static function getSearchableModels()
    {
        $searchableModels = [];

        foreach (config('scout.tntsearch.searchableModels') as $model) {
            $modelName = substr($model, strrpos($model, '\\') + 1);

            if (count(preg_grep("/$modelName/", $searchableModels)) > 0) {
                continue;
            }

            $searchableModels[] = $model;
        }

        return $searchableModels;
    }

    

}
