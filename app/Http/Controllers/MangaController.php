<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManager  as Image;
use Auth;
use App\User;
use App\Models\Category;
use App\Models\Manga;
use App\Models\MangaVolume;
use App\Models\MangaChapter;
use App\Models\MangaPage;
use App\Models\MangaRequest;
use App\Models\SeriesUpdate;
use App\Models\MangaView;
use App\Helpers\SlugHelper;
use App\Models\Artist;
use Excel;
use DB;

class MangaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $options;
    public function __construct(Request $request,$options = null) {
        $this->request = $request;
        $this->options = array(
            'param_name' => 'files',
            // Set the following option to 'POST', if your server does not support
            // DELETE requests. This is a parameter sent to the client:
           
            'print_response' => true
        );
        if ($options) {
            $this->options = $options + $this->options;
        }
      
    }

    /**
     * Manga Dierctory.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $mangas= \DB::table('mangas as ma')->where('ma.status', '=', 'Published')
                 ->whereNull('ma.deleted_at')
               ->leftJoin('users as u', function($join){
                 $join->on('u.id', '=', 'ma.user_id')
                 ->where('u.status', '=', 'Verified');
               })->join('manga_pages as mp', function($join){
                 $join->on('mp.manga_id', '=', 'ma.id');
               })->select('ma.id as id',DB::raw('max(ma.title) as title'),DB::raw('max(ma.created_at) as ma_created_at'), DB::raw('max(ma.cover) as cover'), DB::raw('max(ma.slug) as slug'),DB::raw("(SELECT count(*) FROM `manga_chapters` WHERE `manga_chapters`.`manga_id`=ma.id) as chapter_count"),DB::raw("(SELECT sum(rating)/count(*) FROM `manga_ratings` WHERE `manga_ratings`.`manga_id`=ma.id) as ratings"),DB::raw("(SELECT count(*) FROM `manga_views` WHERE `ma`.`id` = `manga_views`.`manga_id`) as `manga_views_count`"));
        

        //$mangas=Manga::with('chapters','mangaRatings')->withCount('mangaViews')->has('author')->has('pages');
         if($request->has('keywords')){
            if(!empty($request->get('keywords')))
           $mangas =$mangas->where('title',$request->get('keywords'));
        }
            
        if($request->has('release_year')){
            if($request->get('release_year')=='older')
                $mangas = $mangas->where('release_year','<=',date('Y')-20);
            elseif($request->get('release_year')!="")
                $mangas = $mangas->where('release_year',$request->get('release_year'));
        }
        if($request->has('release_status'))
            $mangas = $mangas->whereIn('release_status',$request->get('release_status'));
        if($request->has('category_id')){
             $mangas = $mangas->whereHas('categories', function ($q) use ($request) {
                $q->whereIn('category_id', $request->get('category_id'));
            });
        }
        if($request->has('sort')){
            if($request->get('sort')=='title')
                $mangas = $mangas->orderBy('title');
            elseif($request->get('sort')=='review')
                $mangas = $mangas->orderBy('ratings','desc');
            elseif($request->get('sort')=='date')
                $mangas = $mangas->orderBy('ma_created_at','desc');
            else //if($request->get('sort')=='views')
                $mangas = $mangas->orderBy('manga_views_count', 'desc');
            
        }
        else
            $mangas = $mangas->orderBy('manga_views_count', 'desc');
        
        $mangas=$mangas
               ->groupBy('ma.id')->paginate(20);
        //$mangas=$mangas->where('status','Published')->paginate(20);
        if($request->ajax())
        {
             return response()->json([
                'success' => true,
                'results'=>view('manga.partials.list',compact('mangas'))->render()
            ]);  
        }
        return view('manga.index',compact('mangas'));
    }

    /**
     * Manga Detail.
     *
     * @return \Illuminate\Http\Response
     */
    public function detail($slug,Request $request)
    {
        $manga=Manga::with('chapters')->withCount('mangaViews')->where('slug',$slug)->has('author')->first();
        if(!$manga)
        {
            return redirect()->back()->with('error','Wrong Url');
        }
        if(!$request->has('page')){
            MangaView::createViewLog($manga);
        }
        $volumes=$manga->volumes()->latest()->paginate(2);
        return view('manga.detail',compact(['manga','volumes']));
    }

    /**
     * Manga reading.
     *
     * @return \Illuminate\Http\Response
     */
    public function reading($slug,Request $request)
    {
        $parameter=$request->all();
        $manga=Manga::with('chapterRead')->where('slug',$slug)->has('author')->first();
        if(!$manga)
        {
            return redirect()->back()->with('error','Wrong Url');
        }
        return view('manga.reading',compact(['manga','parameter']));
    }



    /**
     * Upload Manga View.
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadManga($slug=null)
    {
        if(!isArtist() && !isAdmin() && !isPublisher())
        {
            return redirect()->back()->with('error','Oops! you are not authorize to current action.');
        }       
      
        $manga=Manga::where('slug',$slug)->first();
        if(!$manga && $slug)
             return redirect()->back()->withInput()->with('error','Sorry! Selected Comic does not exist.');
        if($slug){
            if($manga->user_id!=Auth::user()->id && !isAdmin())
            {
                return redirect()->back()->with('error','Oops! you are not authorize to current action.');
            }  
        }     
      
        return view('manga.upload.index',compact('manga'));
    }


    //update profile

    public function saveManga(Request $request)
    {
        if(isUser())
            return redirect()->back()->with('error','Oops! you are not authorize to current action.');

        $validator=Validator::make($request->all(), [
            'title' => 'required|string|max:100',
            'release_status' => 'required|string',
            'release_year'=>'required|integer',
            'agreement'=>'required'
        ]);
        if($validator->fails()){
            return redirect()->back()->withInput()->with('error','Kindly see input errors.');
        }
        $slugHelper=new SlugHelper(\App\Models\Manga::class);
        
        if($request->has('id')){
            $manga=Manga::where('id',$request->get('id'))->first();
            if(!$manga){
                return redirect()->back()->withInput()->with('error','Sorry! Selected Comic Series does not exist.');
            }
            $manga->slug=$slugHelper->createSlug($request->get('slug'),$manga->id);
        }
        else{
            $manga=new Manga;
            $manga->status_change=date('Y-m-d H:i:s');
            $manga->user_id=Auth::user()->id;
            $manga->cover="";
            $manga->slug=$slugHelper->createSlug($request->get('slug'),0);
            if(isAdmin()){
                $manga->status="Published";
            }
        }
        if(isPublisher() || isAdmin()){
           $manga->artist_id=$request->get('artist_id');         
        }
        if(isAdmin()){
            $manga->is_featured=$request->get('is_featured'); 
        }
        $manga->title=ucfirst($request->get('title'));
        $manga->category_id=implode(',',$request->get('category_id'));
        $manga->release_year=$request->get('release_year');
        $manga->release_status=$request->get('release_status');
        $manga->description=$request->get('description');
        $manga->artist='';
        if($request->hasFile('cover')){
            $fileName   = time() . '.jpg'; //. $image->getClientOriginalExtension();
            
            $image = (new Image)->make($request->file('cover'));
            $image->resize(300,500, function ($constraint) {
                $constraint->aspectRatio();
            })->encode('jpg');

            $path=\Storage::disk('local')->put('public/covers/'.$fileName, (string) $image );

            //$image->resizeWidth(300)->storeAs('public/covers1/', $fileName);
            $manga->cover=$fileName;
        }
            
        $manga->save();
        if($request->has('id'))
            $manga->categories()->sync($request->get('category_id'));
        else
            $manga->categories()->attach($request->get('category_id'));
        
        return redirect()->route('artist-manga-listing')->with('message','Comic Series uploaded successfully.This may be take couple of days to publish.');

    }

    public function deleteManga($slug, Request $request)
    {
        $manga=Manga::where('slug',$slug)->first();
        if(!$manga)
             return redirect()->back()->withInput()->with('error','Sorry! Selected Comic does not exist.');
        if($manga->user_id!=Auth::user()->id && !isAdmin())
        {
            return redirect()->back()->with('error','Oops! you are not authorize to current action.');
        }       
      
        $manga->delete();
        if($request->ajax()){
            return response()->json([
                'success' => true
            ]); 
        }
        return redirect()->back()->with('message','Comic deleted successfully.');
    }


    /**
     * Upload Manga Volume View.
     *
     * @return \Illuminate\Http\Response
     */
    public function mangaVolume($action)
    {
        if(isUser())
            return redirect()->back()->with('error','Oops! you are not authorize to current action.');
        return view('manga.upload.volume',['action'=>$action]);
    }

     /**
     * Add New Manga Volume
     *
     * @return \Illuminate\Http\Response
     */
    public function mangaVolumeUpdate($action,Request $request)
    {
        if(isUser())
            return redirect()->back()->with('error','Oops! you are not authorize to current action.');

        if($action=='add')
        {
            $validator=Validator::make($request->all(), [
                'name' => 'required|string|max:100',
                'manga_id' => 'required|integer'
            ]);
            if($validator->fails()){
                return redirect()->back()->withInput()->with('error','Kindly see input errors.');
            }
            $volume=new MangaVolume;
            $volume->manga_id=$request->get('manga_id');
            $volume->total_copies=$request->get('total_copies');
            $volume->name=ucfirst($request->get('name'));
            $volume->save();
            return redirect()->back()->with('message','Comic Volume uploaded successfully.');
        }
        elseif($action=='edit'){
            $validator=Validator::make($request->all(), [
                'name' => 'required|string|max:100',
                'manga_id' => 'required|integer',
                'manga_volume_id' => 'required|integer',                 
            ]);
            if($validator->fails()){
                return redirect()->back()->withInput()->with('error','Kindly see input errors.');
            }
            $volume=MangaVolume::where('id',$request->get('manga_volume_id'))->where('manga_id',$request->get('manga_id'))->first();
            if(!$volume)
                 return redirect()->back()->withInput()->with('error','Sorry! Selected Comic Volume does not exist.');
            $volume->manga_id=$request->get('manga_id');
            $volume->name=ucfirst($request->get('name'));
            $volume->total_copies=$request->get('total_copies');
            $volume->save();
            return redirect()->back()->with('message','Comic Volume updated successfully.');
        }
        else
        {
            $validator=Validator::make($request->all(), [
                'manga_id' => 'required|integer',
                'manga_volume_id' => 'required|integer',                 
            ]);
            if($validator->fails()){
                return redirect()->back()->withInput()->with('error','Kindly see input errors.');
            }
            $volume=MangaVolume::where('id',$request->get('manga_volume_id'))->where('manga_id',$request->get('manga_id'))->first();
            if(!$volume)
                 return redirect()->back()->withInput()->with('error','Sorry! Selected Comic Volume does not exist.');
            $volume->delete();
            return redirect()->back()->with('message','Comic Volume deleted successfully.');
        }
    }

    public function mangaVolumeList($manga_id,Request $request)
    {
        if($request->ajax())
            return getMangaVolumes($manga_id);
        
        return;       
    }


    /**
     * Upload Manga Chapter View.
     *
     * @return \Illuminate\Http\Response
     */
    public function mangaChapter($action)
    {
        if(isUser())
            return redirect()->back()->with('error','Oops! you are not authorize to current action.');
        return view('manga.upload.chapter',['action'=>$action]);
    }

        /**
     * Add New Manga Volume
     *
     * @return \Illuminate\Http\Response
     */
    public function mangaChapterUpdate($action,Request $request)
    {
        if(isUser())
            return redirect()->back()->with('error','Oops! you are not authorize to current action.');

        $slugHelper=new SlugHelper(\App\Models\MangaChapter::class);
        if($action=='add')
        {
            $validator=Validator::make($request->all(), [
                'name' => 'required|string|max:100',
                'number' => 'required|string|max:100',
                'release_date' => 'required|date',
                'manga_id' => 'required|integer',
                'manga_volume_id' => 'required|integer'
            ]);
            if($validator->fails()){
                return redirect()->back()->withInput()->with('error','Kindly see input errors.');
            }
            $chapter=new MangaChapter;
            $chapter->slug=$request->get('name');
            $chapter->manga_id=$request->get('manga_id');
            $chapter->manga_volume_id=$request->get('manga_volume_id');
            $chapter->number=$request->get('number');
            $chapter->release_date=date('Y-m-d',strtotime($request->get('release_date')));
            $chapter->name=ucfirst($request->get('name'));
            $chapter->save();
            return redirect()->back()->with('message','Comic chapter uploaded successfully.');
        }
        elseif($action=='edit'){
            $validator=Validator::make($request->all(), [
                'name' => 'required|string|max:100',
                'number' => 'required|integer|max:100',
                'release_date' => 'required|date',
                'manga_id' => 'required|integer',
                'manga_chapter_id' => 'required|integer',
                'manga_volume_id' => 'required|integer'
             ]);
            if($validator->fails()){
                return redirect()->back()->withInput()->with('error','Kindly see input errors.');
            }
            $chapter=MangaChapter::where('id',$request->get('manga_chapter_id'))->where('manga_volume_id',$request->get('manga_volume_id'))->where('manga_id',$request->get('manga_id'))->first();
            if(!$chapter)
                 return redirect()->back()->withInput()->with('error','Sorry! Selected Comic Chapter does not exist.');
            $chapter->number=$request->get('number');
            $chapter->release_date=date('Y-m-d',strtotime($request->get('release_date')));
            $chapter->name=ucfirst($request->get('name'));
            $chapter->save();
            if($request->has('page_id')){
                foreach ($request->get('page_id') as $key => $value) {
                    $page=MangaPage::where('id',$value)->where('manga_chapter_id',$request->get('manga_chapter_id'))->where('manga_id',$request->get('manga_id'))->first();
                    if($page)
                    {
                        $page->page_no=$request->get('page_no')[$key];
                        $page->save();
                    }
                }
                SeriesUpdate::createSeriesUpdate($chapter->manga_id,$chapter->id); 
            }
            return redirect()->back()->with('message','Comic Volume updated successfully.');
        }
        else
        {
            $validator=Validator::make($request->all(), [
               'manga_id' => 'required|integer',
                'manga_chapter_id' => 'required|integer',
                'manga_volume_id' => 'required|integer'
            ]);
            if($validator->fails()){
                return redirect()->back()->withInput()->with('error','Kindly see input errors.');
            }
            $chapter=MangaChapter::where('id',$request->get('manga_chapter_id'))->where('manga_volume_id',$request->get('manga_volume_id'))->where('manga_id',$request->get('manga_id'))->first();
            if(!$chapter)
                 return redirect()->back()->withInput()->with('error','Sorry! Selected Comic Chapter does not exist.');
          
            $chapter->delete();
            return redirect()->back()->with('message','Comic Chapter deleted successfully.');
        }
    }

    public function mangaChapterList($manga_id,$manga_volume_id=null,Request $request)
    {
        if($request->ajax())
            return getMangaChapters($manga_id,$manga_volume_id);
        
        return;       
    }

    public function mangaChapterDetail($manga_chapter_id,Request $request)
    {
        if($request->ajax())
            return MangaChapter::where('id',$manga_chapter_id)->first();
        
        return;       
    }
    

    /**
     * Upload Manga Chapter View.
     *
     * @return \Illuminate\Http\Response
     */
    public function mangaPage()
    {
        if(isUser())
            return redirect()->back()->with('error','Oops! you are not authorize to current action.');
        return view('manga.upload.pages');
    }

    public function mangaPageList($manga_id,$manga_chapter_id,Request $request)
    {
        if($request->ajax()){
            if($request->get('view'))
            {
                $pages=getMangaPages($manga_id,$manga_chapter_id);
                return view($request->get('view'),compact('pages'));
            }
            return getMangaPages($manga_id,$manga_chapter_id);
        }
        
        return;       
    }

    public function mangaPageUpdate(Request $request)
    {
        if(isUser())
        {
            if($request->ajax())
                return response()->json([
                    'session_null' => true,
                    'success' => false,
                ]);
            return redirect()->back()->with('error','Oops! you are not authorize to current action.');
        }
        $validator=Validator::make($request->all(), [
            'manga_id' => 'required|integer',
            'manga_chapter_id' => 'required|integer'
        ]);
        if($validator->fails()){
            if($request->ajax())
                return response()->json([
                    'success' => false,
                    'errors' => $validator->getMessageBag()->toArray(),
                ]);
            return redirect()->back()->withInput()->with('error','Kindly see input errors.');
        }
        if($request->hasFile('files')){
            foreach($request->file('files') as $file){
                $files[] =$this->uploadPage($file,$request);
            }
            $response = array($this->options['param_name'] => $files);
            SeriesUpdate::createSeriesUpdate($request->get('manga_id'),$request->get('manga_chapter_id')); 

            return response()->json($response);
        }
        else{
            return response()->json([
                    'success' => false,
                    'errors' => ['Please select a correct page to upload.'],
                ]);
            
        }
    
    }

    public function uploadPage($file,Request $request)
    {
        $data= array('file' => $file);
        $validator=$this->imagevalidator($data);
        $file_std = new \stdClass();
        if($validator->fails())
        {
            $error=[];
            foreach ($validator->messages()->getMessages() as $field_name => $messages)
            {
                foreach($messages AS $message) {
                    $error[]=$message; 
                }
                
            }
            $file_std->error=implode(',', $error); 
            return $file_std;
        }
        
        $path=storage_path('app/public/mangas/').$request->get('manga_id')."/".$request->get('manga_volume_id')."/".$request->get('manga_chapter_id');
        if(!\File::exists($path)) {
            \File::isDirectory($path) or \File::makeDirectory($path, 0777, true, true);
        }
        $page=new MangaPage;
        $page->manga_id=$request->get('manga_id');
        $page->manga_chapter_id=$request->get('manga_chapter_id');

        $image      = $request->file('files');
        $type_mime_img=\File::mimeType($file);
        $pages=MangaPage::where('manga_chapter_id',$request->get('manga_chapter_id'))->where('manga_id',$request->get('manga_id'))->orderBy('page_no','desc')->first();
        
        if($type_mime_img!= "image/png" && $type_mime_img!="image/x-png")
            $fileName   = time().'_'. ($pages?$pages->page_no+1:'1') . '.png';
        else
            $fileName   = time().'_'. ($pages?$pages->page_no+1:'1') . '.jpg';
          
        $file->storeAs('public/mangas/'.$request->get('manga_id')."/".$request->get('manga_volume_id')."/".$request->get('manga_chapter_id'), $fileName);
        $page->page_image=$fileName;
        $page->page_no=$pages?$pages->page_no+1:'1';
        $page->save();
        $sizeFile= \File::size($file);
        
        $path=asset('storage/mangas/').'/'.$request->get('manga_id')."/".$request->get('manga_volume_id')."/".$request->get('manga_chapter_id');

        $file_std->deleteUrl = route('manag-page-delete',$page->id);
        $file_std->deleteType='get';
        $file_std->id = $page->id;
        $file_std->name =$page->page_no;
        $file_std->size = (int)$sizeFile;
        $file_std->type = $type_mime_img;
        $file_std->url =  $path.'/'.$page->page_image;
        $file_std->get_full_url =  $path.'/'.$page->page_image;
        return $file_std;
    }

    protected function imagevalidator(array $data) {

        $messages = array (
        "file.mimes"   => 'Only PNG or JPG allowed.',
        
        );
        $rule='mimes:jpeg,png,jpg';
        // Create Rules
        return Validator::make($data, [
             'file' =>$rule,
        ], $messages);
            //image_size:>='.$dimensions[0].',>='.$dimensions[1].'
        // Update Rules
    }

    public function mangaPageDelete($page_id, Request $request)
    {
        if(isUser())
        {
            return redirect()->back()->with('error','Oops! you are not authorize to current action.');
        }
        $page=MangaPage::where('id',$page_id)->first();
        if(!$page)
             return redirect()->back()->withInput()->with('error','Sorry! Selected Comic Page does not exist.');
      
        $page->delete();
        if($request->ajax()){
            return response()->json([
                'success' => true
            ]); 
        }
        return redirect()->back()->with('message','Comic page deleted successfully.');
    }


    public function requestManga(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'manga_id' => 'required|integer',
            'manga_volume_id' => 'required|integer',
            'address' => 'required'
        ]);
        if($validator->fails()){
            if($request->ajax())
                return response()->json([
                    'success' => false,
                    'errors' => $validator->getMessageBag()->toArray(),
                ]);
            return redirect()->back()->withInput()->with('error','Kindly see input errors.');
        }
        $manga=Manga::where('id',$request->get('manga_id'))->has('author')->first();
        if($manga->user_id==Auth::user()->id)
        {
            if($request->ajax())
                return response()->json([
                    'success' => false,
                    'errors' => 'Oops! you cannot order your own Manga.',
                ]);
            return redirect()->back()->with('error','Oops! you cannot order your own Manga.');
        }
        $mangaRequest=MangaRequest::where('manga_id',$request->get('manga_id'))->where('manga_volume_id',$request->get('manga_volume_id'))->where('user_id',Auth::user()->id)->first();
        
        if($mangaRequest)
        {
            if($request->ajax())
                return response()->json([
                    'success' => false,
                    'errors' => 'Oops! you cannot order your own Manga.',
                ]);
            return redirect()->back()->with('error','Oops! you cannot order your own Manga.');
        }
        $mangaRequest=new \App\Models\MangaRequest;    
        $mangaRequest->manga_id=$request->get('manga_id');
        $mangaRequest->manga_volume_id=$request->get('manga_volume_id');
        $mangaRequest->address=$request->get('address');
        $mangaRequest->message=$request->get('message');
        $mangaRequest->user_id=\Auth::user()->id;
        $mangaRequest->save();
        
        if($request->ajax()){
            return response()->json([
                'success' => true,
                'message'=>'Comic order successfully.'
            ]); 
        }
        return redirect()->back()->with('message','Comic order successfully.');
    }

    public function artistMangaList()
    {
        if(!isArtist() && !isAdmin() && !isPublisher())
        {
            return redirect()->back()->with('error','Oops! you are not authorize to current action.');
        }
        $mangas=Manga::latest()->where('user_id',\Auth::user()->id);

        $mangas=$mangas->paginate(24);
        return view('manga.artist.index',compact('mangas'));
    }

    public function artistMangaRequests()
    {
        if(!isArtist() && !isAdmin() && !isPublisher())
        {
            return redirect()->back()->with('error','Oops! you are not authorize to current action.');
        }
        $mangaRequests=MangaVolume::artistCheck()->has('mangaRequests')->latest()->paginate(20);
        return view('manga.artist.requests',compact('mangaRequests'));
    }

    public function artistMangaRequestDetail($volume_id)
    {
        if(!isArtist() && !isAdmin() && !isPublisher())
        {
            return redirect()->back()->with('error','Oops! you are not authorize to current action.');
        }
        $mangaRequests=MangaRequest::artistCheck()->where('manga_volume_id',$volume_id)->latest()->paginate(20);
        return view('manga.artist.requestDetail',compact('mangaRequests'));
    }

    public function userMangaRequests()
    {
        $mangaRequests=MangaRequest::latest()->where('user_id',\Auth::user()->id)->paginate(20);
        return view('manga.users.requests',compact('mangaRequests'));
    }

    public function seriesUpdates ()
    {
        $seriesUpdates=SeriesUpdate::latest()->where('user_id',\Auth::user()->id)->paginate(20);
        return view('manga.users.updates',compact('seriesUpdates'));
    }

    public function import(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);
        if($validator->fails()){
            return redirect()->back()->withInput()->with('error','Attach a valid file.');
        }
        $path = $request->file('file')->getPathName();
        $data = Excel::load($path, function($reader) {})->get();
       
        $slugHelper=new SlugHelper(\App\Models\Manga::class);
        $path=storage_path('app/public/');
                        
        if(!empty($data) && $data->count())
        {
            foreach ($data as $key => $value) {
               
                if(!empty($value)){
                    if(Manga::where('title',$value['title'])->count()==0){
                        $user=(User::where('email',$value['user'])->count()>0)?User::where('email',$value['user'])->first():Auth::user();
                        $manga=new Manga;
                        $manga->status_change=date('Y-m-d H:i:s');
                        $manga->user_id=$user->id;
                        $manga->slug=$slugHelper->createSlug($value['title'],0);
           
                        if(!empty($value['artist'])){
                            if(Artist::where('name',$value['artist'])->where('user_id',$user->id)->count()>0)
                                $manga->artist_id=Artist::where('name',$value['artist'])->where('user_id',$user->id)->first()->id;
                            else
                            {
                                $artist=new Artist;
                                $artist->name=$value['artist'];
                                $artist->user_id=$user->id;
                                $artist->save();
                                $manga->artist_id=$artist->id;
                            }       
                        }
                        
                        $manga->title=ucfirst($value['title']);
                        $manga->category_id=(Category::where('name',$value['category'])->count()>0)?Category::where('name',$value['category'])->first()->id:Category::first()->id;
                        $manga->release_year=$value['year'];
                        $manga->release_status=$value['status'];
                        $manga->description=$value['description'];
                        $manga->artist='';
                        $fileName   = time() . '.jpg'; //. $image->getClientOriginalExtension();
                        
                        \File::move($path."import/".$value['folder']."/cover.jpg",$path. "covers/".$fileName);
                       
                        $manga->cover=$fileName;
                            
                        $manga->save();
                        foreach (\File::directories($path."import/".$value['folder']) as $volFolder) 
                        {
                            $vol=explode("\\", $volFolder);
                            $vol=end($vol);
                            $volume=new MangaVolume;
                            $volume->manga_id=$manga->id;
                            $volume->total_copies=explode('-',$vol)[2];
                            $volume->name=ucfirst(explode('-',$vol)[1]);
                            $volume->save();
                            foreach (\File::directories($volFolder) as $chapterFolder)
                            {
                                $ch=explode("\\", $chapterFolder);
                                $ch=end($ch);
                              
                                $chapter=new MangaChapter;
                                $chapter->slug=explode('-',$ch)[1];
                                $chapter->manga_id=$manga->id;
                                $chapter->manga_volume_id=$volume->id;
                                $chapter->number=explode('-',$ch)[0];
                                $chapter->release_date=date('Y-m-d');
                                $chapter->name=ucfirst(explode('-',$ch)[1]);
                                $chapter->save();
                                foreach (\File::files($chapterFolder) as $pageImage)
                                {
                                   $pagePath=storage_path('app/public/mangas/').$manga->id."/".$volume->id."/".$chapter->id;
                                    if(!\File::exists($pagePath)) {
                                        \File::isDirectory($pagePath) or \File::makeDirectory($pagePath, 0777, true, true);
                                    }
                                    $page=new MangaPage;
                                    $page->manga_id=$manga->id;
                                    $page->manga_chapter_id=$chapter->id;

                                    $pages=MangaPage::where('manga_chapter_id',$chapter->id)->where('manga_id',$manga->id)->orderBy('page_no','desc')->first();
                                    
                                    \File::move($chapterFolder."/".$pageImage->getRelativePathname(),$pagePath."/".$pageImage->getRelativePathname());  
                                    $page->page_image=$pageImage->getRelativePathname();
                                    $page->page_no=$pages?$pages->page_no+1:'1';
                                    
                                    $page->save();
                                }
                            }
                        }
                    }
                }
           }
        }
    }


    public function importNew(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);
        if($validator->fails()){
            return redirect()->back()->withInput()->with('error','Attach a valid file.');
        }
        $path = $request->file('file')->getPathName();
        $data = Excel::load($path, function($reader) {})->get();
       
        $slugHelper=new SlugHelper(\App\Models\Manga::class);
        $path=storage_path('app/public/');
                        
        if(!empty($data) && $data->count())
        {
            foreach ($data as $key => $value) {
               
                if(!empty($value['title'])){
                    $manga=Manga::where('title',$value['title'])->first();
                    $user=(User::where('email',$value['user'])->count()>0)?User::where('email',$value['user'])->first():Auth::user();
                    if(!$manga){
                        $manga=new Manga;
                        $manga->status_change=date('Y-m-d H:i:s');
                        $manga->slug=$slugHelper->createSlug($value['title'],0);
                        if(!empty($value['artist'])){
                            if(Artist::where('name',$value['artist'])->where('user_id',$user->id)->count()>0)
                                $manga->artist_id=Artist::where('name',$value['artist'])->where('user_id',$user->id)->first()->id;
                            else
                            {
                                $artist=new Artist;
                                $artist->name=$value['artist'];
                                $artist->user_id=$user->id;
                                $artist->save();
                                $manga->artist_id=$artist->id;
                            }       
                        }
                        $manga->user_id=$user->id;
                         

                        $categories=explode(',', $value['category']);
                        $cats=[];
                        foreach ($categories as $k => $cat) {
                            if(Category::where('name',$cat)->count()>0)
                                $cats[]=Category::where('name',$cat)->first()->id;
                        }
                        
                        $manga->title=ucfirst($value['title']);
                        $manga->category_id=(count($cats)>0?implode(',',$cats):Category::first()->id);
                        $manga->release_year=$value['year'];
                        $manga->release_status=$value['status'];
                        $manga->description=$value['description'];
                        $manga->artist='';
                        $fileName   = time() . '.jpg'; //. $image->getClientOriginalExtension();
                        
                        //\File::copy($path."import/".$value['folder']."/cover.jpg",$path. "covers/".$fileName);
                        \File::copy(storage_path('app/public/imports/').$user->slug."/".$value['pageimage'],storage_path('app/public/covers/').$fileName);  
                        $manga->cover=$fileName;
                            
                        $manga->save();
                    }
                    $volume=MangaVolume::where('name',$value['volume'])->where('manga_id',$manga->id)->first();
                    if(!$volume){
                        $volume=new MangaVolume;
                        $volume->manga_id=$manga->id;
                        $volume->total_copies=$value['copies'];
                        $volume->name=ucfirst($value['volume']);
                        $volume->save();
                    }
                    $chapter=MangaChapter::where('number',$value['chapter_no'])->where('manga_volume_id',$volume->id)->where('manga_id',$manga->id)->first();
                    if(!$chapter){
                        $chapter=new MangaChapter;
                        $chapter->slug=$value['chapter_name'];
                        $chapter->manga_id=$manga->id;
                        $chapter->manga_volume_id=$volume->id;
                        $chapter->number=$value['chapter_no'];
                        $chapter->release_date=date('Y-m-d');
                        $chapter->name=ucfirst($value['chapter_name']);
                        $chapter->save();
                    }
                    $pagePath=storage_path('app/public/mangas/').$manga->id."/".$volume->id."/".$chapter->id;
                    if(!\File::exists($pagePath)) {
                        \File::isDirectory($pagePath) or \File::makeDirectory($pagePath, 0777, true, true);
                    }
                    $page=MangaPage::where('page_no',$value['page_no'])->where('manga_chapter_id',$chapter->id)->where('manga_id',$manga->id)->first();
                    if(!$page){
                        $page=new MangaPage;
                        $page->manga_id=$manga->id;
                        $page->manga_chapter_id=$chapter->id;
                        $page->page_no=$value['page_no'];
                    }
                    \File::move(storage_path('app/public/imports/').$user->slug."/".$value['pageimage'],$pagePath."/".$value['pageimage']);  
                    $page->page_image=$value['pageimage'];
                    
                    $page->save();
                    
                }
           }
        }
        return redirect()->back()->with('message','Process Completed successfully.');
    }

    public function massUploadSave(Request $request){
        $files=[];
        foreach($request->file('files') as $file){
            $data= array('file' => $file);
            $validator=$this->imagevalidator($data);
            $file_std = new \stdClass();
            if($validator->fails())
            {
                $error=[];
                foreach ($validator->messages()->getMessages() as $field_name => $messages)
                {
                    foreach($messages AS $message) {
                        $error[]=$message; 
                    }
                    
                }
                $file_std->error=implode(',', $error); 
                
            }else{
                $type_mime_img=\File::mimeType($file);
                $path=storage_path('app/public/imports/').Auth::user()->slug;
                if(!\File::exists($path)) {
                    \File::isDirectory($path) or \File::makeDirectory($path, 0777, true, true);
                }
                $fileName=$file->getClientOriginalName();
                $file->storeAs('public/imports/'.Auth::user()->slug, $fileName);
                $sizeFile= \File::size($file);
                
                $path=asset('storage/imports/').'/'.Auth::user()->slug;

                $file_std->name = $fileName;
                $file_std->size = (int)$sizeFile;
                $file_std->type = $type_mime_img;
                $file_std->url =  $path.'/'.$fileName;
                $file_std->get_full_url =  $path.'/'.$fileName;
            }
            $files[]= $file_std;
        }
        $response = array($this->options['param_name'] => $files);
        return response()->json($response);
    }

    public function massUpload(Request $request)
    {
        if(isUser())
        {
            if($request->ajax())
                return response()->json([
                    'session_null' => true,
                    'success' => false,
                ]);
            return redirect()->back()->with('error','Oops! you are not authorize to current action.');
        }
        return view('manga.upload.mass');
    }

}
