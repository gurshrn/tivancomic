<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Manga;
use Illuminate\Support\Facades\View;
use App\Models\Artist;
use Analytics;
use Spatie\Analytics\Period;
use Carbon\Carbon;
use App\Models\AdRevenue;
use App\Models\UserAdRevenue;

class AdRevenueController extends Controller
{
   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }
    
    public function add()
    {
    	$period=Period::days(1);
        $analyticsData =  Analytics::performQuery(
            $period,
            'ga:users,ga:uniquePageviews,ga:adsenseRevenue',
            ['dimensions' => 'ga:date,ga:pagePath,ga:pagePathLevel2',
            'sort' => 'ga:pagePath',
            'filters'=>'ga:adsenseRevenue>0']
        );

     //    $data= collect($analyticsData['rows'] ?? [])->map(function (array $dateRow) {
	    //     return [
	    //         'date' => Carbon::createFromFormat('Ymd', $dateRow[0]),
	    //         'pagePath' => $dateRow[1],
	    //         'pagePathLevel2' =>  $dateRow[2],
	    //         'users' =>  (int)$dateRow[3],
	    //         'views' => (int)$dateRow[4],
	    //         'revenue' =>  (float)$dateRow[5],
	    //     ];
	    // });

        $adRevenue=AdRevenue::where('date',$period->endDate->toDateString())->first();
        if(!$adRevenue)
        	$adRevenue=new AdRevenue;
        $adRevenue->date=$period->endDate;
        $adRevenue->ad_clicks=(int)($analyticsData['totalsForAllResults']['ga:uniquePageviews']);
        $adRevenue->total_revenue=(double)($analyticsData['totalsForAllResults']['ga:adsenseRevenue']);
        $adRevenue->save();

        $manga_slug="";
        $userAdRevenue=null;
        foreach ($analyticsData['rows'] as $key => $value) {
        	if (strpos( $value[1], '/comic-directory/') === 0 || strpos( $value[1], '/comic-reading/')===0){
        		if(explode('?', $value[2])[0]!=$manga_slug){
        			if(!empty($userAdRevenue))
	        			$userAdRevenue->save();
		        	$manga_slug=explode('?', $value[2])[0];
        			$manga=\App\Models\Manga::where('slug',trim($manga_slug,'/'))->first();
        			if($manga){
        				$userAdRevenue=UserAdRevenue::where('manga_id',$manga->id)->whereDate('date',$period->endDate)->first();
        				if(!$userAdRevenue)
        					$userAdRevenue=new UserAdRevenue;
			        	$userAdRevenue->date=$period->endDate;
			        	$userAdRevenue->user_id=$manga->user_id;
			        	$userAdRevenue->manga_id=$manga->id;
			        	$userAdRevenue->ad_clicks=0;	
		        	}	        	
	        	}
	        	if(!empty($userAdRevenue))
	        		$userAdRevenue->ad_clicks=((int)$userAdRevenue->ad_clicks)+((int)$value[4]);
     		}		
        }
        if(!empty($userAdRevenue)){
        	$userAdRevenue->save();  
    	}
        
	    dd('done');
    }
           
}
