<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use Config;
use DB;
use App\Helpers\SlugHelper;
use App\User;
use App\Models\FollowArtist;
use App\Models\FollowManga;
use App\Models\Manga;
use App\Models\MangaRating;
use App\Models\SeriesUpdate;
use App\Models\AdRevenue;
use App\Models\UserAdRevenue;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('profile.index');
    }


    //Edit profile View

    public function editProfile()
    {
        return view('profile.edit');
    }


    //update profile

    public function updateProfile(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191',
            'dob'=>'date',
        ]);
        if($validator->fails()){
            return redirect()->back()->withInput()->with('error','Kindly see input errors.');
        }
        $slugHelper=new SlugHelper(\App\User::class);
        $user=Auth::user();
        $user->name=$request->get('name');
        $user->gender=$request->get('gender');
        $user->bio=$request->get('bio');
        $user->dob=date('Y-m-d',strtotime($request->get('dob')));
        $user->save();
        return redirect()->back()->with('message','Profile update successfully.');

    }

    public function followArtist(Request $request)
    {

        $validator=Validator::make($request->all(), [
            'artist_id' => 'required|integer',
            'type'=>'required'
        ]);
        if($validator->fails()){
            if($request->ajax())
                return response()->json([
                    'success' => false,
                    'errors' => $validator->getMessageBag()->toArray(),
                ]);
            return redirect()->back()->withInput()->with('error','Kindly see input errors.');
        }
        $artist=Artist::where('id',$request->get('artist_id'))->first();
        if($type=='User')
            $artist=User::where('id',$request->get('artist_id'))->first();
            
        if(!$artist){
            if($request->ajax())
                return response()->json([
                    'success' => false,
                    'errors' => 'Oops! selected user is not exists.',
                ]);
            return redirect()->back()->with('error','Oops! selected user is not exists.');
        }
        if($artist->id==Auth::user()->id)
        {
            if($request->ajax())
                return response()->json([
                    'success' => false,
                    'errors' => 'Oops! you cannot follow your own profile.',
                ]);
            return redirect()->back()->with('error','Oops! you cannot follow your own profile.');
        }
        $followArtist=FollowArtist::where('artist_id',$request->get('artist_id'))->where('type',$request->get('type'))->where('user_id',Auth::user()->id)->first();
        
        if($followArtist)
        {
            if($request->ajax())
                return response()->json([
                    'success' => false,
                    'errors' => 'Oops! you are already following profile.',
                ]);
            return redirect()->back()->with('error','Oops! you are already following profile.');
        }
        $followArtist=new \App\Models\FollowArtist;    
        $followArtist->artist_id=$request->get('artist_id');
        $followArtist->type=$request->get('type');
        $followArtist->user_id=\Auth::user()->id;
        $followArtist->save();
        
        if($request->ajax()){
            return response()->json([
                'success' => true,
                'reload' => true,
                'message'=>'Follow artist successfully.'
            ]); 
        }
        return redirect()->back()->with('message','Follow artist successfully.');
    }

    public function followManga(Request $request)
    {

        $validator=Validator::make($request->all(), [
            'manga_id' => 'required|integer',
            'artist_id' => 'integer'
        ]);
        if($validator->fails()){
            if($request->ajax())
                return response()->json([
                    'success' => false,
                    'errors' => $validator->getMessageBag()->toArray(),
                ]);
            return redirect()->back()->withInput()->with('error','Kindly see input errors.');
        }
        $manga=Manga::where('id',$request->get('manga_id'))->first();
        if(!$manga){
            if($request->ajax())
                return response()->json([
                    'success' => false,
                    'errors' => 'Oops! selected manga does not exists.',
                ]);
            return redirect()->back()->with('error','Oops! selected manga does not exists.');
        }
        if($manga->user_id==Auth::user()->id)
        {
            if($request->ajax())
                return response()->json([
                    'success' => false,
                    'errors' => 'Oops! you cannot follow your own Manga.',
                ]);
            return redirect()->back()->with('error','Oops! you cannot follow your own Manga.');
        }
        $followManga=FollowManga::where('manga_id',$request->get('manga_id'))->where('user_id',Auth::user()->id)->first();
        
        if($followManga)
        {
            if($request->ajax())
                return response()->json([
                    'success' => false,
                    'errors' => 'Oops! you are already following manga.',
                ]);
            return redirect()->back()->with('error','Oops! you are already following manga.');
        }
        $followManga=new \App\Models\FollowManga;    
        $followManga->manga_id=$request->get('manga_id');
        $followManga->artist_id=$manga->user_id;
        $followManga->user_id=\Auth::user()->id;
        $followManga->save();
        
        if($request->ajax()){
            return response()->json([
                'success' => true,
                'reload' => true,
                'message'=>'Follow manga successfully.'
            ]); 
        }
        return redirect()->back()->with('message','Follow manga successfully.');
    }


    public function unfollowArtist(Request $request)
    {

        $validator=Validator::make($request->all(), [
            'artist_id' => 'required|integer'
        ]);
        if($validator->fails()){
            if($request->ajax())
                return response()->json([
                    'success' => false,
                    'errors' => $validator->getMessageBag()->toArray(),
                ]);
            return redirect()->back()->withInput()->with('error','Kindly see input errors.');
        }
        $followArtist=FollowArtist::where('id',$request->get('artist_id'))->where('user_id',Auth::user()->id)->first();
        
        if(!$followArtist)
        {
            if($request->ajax())
                return response()->json([
                    'success' => false,
                    'errors' => 'Oops! you are not following profile.',
                ]);
            return redirect()->back()->with('error','Oops! you are not following profile.');
        }
        $followArtist->delete();
        
        if($request->ajax()){
            return response()->json([
                'success' => true,
                'reload' => true,
                'message'=>'Unfollow artist successfully.'
            ]); 
        }
        return redirect()->back()->with('message','Unfollow artist successfully.');
    }

    public function unfollowManga(Request $request)
    {

        $validator=Validator::make($request->all(), [
            'manga_id' => 'required|integer',
            'artist_id' => 'integer'
        ]);
        if($validator->fails()){
            if($request->ajax())
                return response()->json([
                    'success' => false,
                    'errors' => $validator->getMessageBag()->toArray(),
                ]);
            return redirect()->back()->withInput()->with('error','Kindly see input errors.');
        }
        $followManga=FollowManga::where('manga_id',$request->get('manga_id'))->where('user_id',Auth::user()->id)->first();
        
        if(!$followManga)
        {
            if($request->ajax())
                return response()->json([
                    'success' => false,
                    'errors' => 'Oops! you are not following manga.',
                ]);
            return redirect()->back()->with('error','Oops! you are not following manga.');
        }
        $followManga->delete();
        
        if($request->ajax()){
            return response()->json([
                'success' => true,
                'reload' => true,
                'message'=>'Unfollow manga successfully.'
            ]); 
        }
        return redirect()->back()->with('message','Unfollow manga successfully.');
    }

    public function userMangaFollowList()
    {
        $followMangas=FollowManga::where('user_id',\Auth::user()->id)->paginate(20);
        return view('userProfile.followingMangas',compact('followMangas'));
    }
    public function userArtistFollowList()
    {
        $followArtists=FollowArtist::where('user_id',\Auth::user()->id)->paginate(20);
        return view('userProfile.followingArtists',compact('followArtists'));
    }

    public function rateManga(Request $request)
    {

        $validator=Validator::make($request->all(), [
            'manga_id' => 'required|integer',
            'rating' => 'required|integer'
        ]);
        if($validator->fails()){
            if($request->ajax())
                return response()->json([
                    'success' => false,
                    'errors' => $validator->getMessageBag()->toArray(),
                ]);
            return redirect()->back()->withInput()->with('error','Kindly see input errors.');
        }
        $manga=Manga::where('id',$request->get('manga_id'))->first();
        if(!$manga){
            if($request->ajax())
                return response()->json([
                    'success' => false,
                    'errors' => 'Oops! selected manga does not exists.',
                ]);
            return redirect()->back()->with('error','Oops! selected manga does not exists.');
        }
        if($manga->user_id==Auth::user()->id)
        {
            if($request->ajax())
                return response()->json([
                    'success' => false,
                    'errors' => 'Oops! you cannot rate your own Manga.',
                ]);
            return redirect()->back()->with('error','Oops! you cannot rate your own Manga.');
        }
        $mangaRating=MangaRating::where('manga_id',$request->get('manga_id'))->where('user_id',Auth::user()->id)->first();
        
        if($mangaRating)
        {
            if($request->ajax())
                return response()->json([
                    'success' => false,
                    'errors' => 'Oops! you have already rate selected manga.',
                ]);
            return redirect()->back()->with('error','Oops! you have already rate selected manga.');
        }
        $mangaRating=new \App\Models\MangaRating;    
        $mangaRating->manga_id=$request->get('manga_id');
        $mangaRating->rating=$request->get('rating');
        $mangaRating->review=$request->has('review')?$request->get('review'):'';
        $mangaRating->user_id=\Auth::user()->id;
        $mangaRating->save();
        
        $rating=(MangaRating::where('manga_id',$manga->id)->sum('rating'))/(MangaRating::where('manga_id',$manga->id)->count());
        Manga::where('id', $manga->id)->update(['ratings'=>$rating]);
        
        if($request->ajax()){
            return response()->json([
                'success' => true,
                'reload' => true,
                'message'=>'Rate manga successfully.'
            ]); 
        }
        return redirect()->back()->with('message','Rate manga successfully.');
    }

    public function seriesUpdates()
    {
        $seriesUpdates=SeriesUpdate::where('user_id',\Auth::user()->id)->latest()->paginate(20);
        return view('manga.users.updates',compact('seriesUpdates'));
    }
  
    public function markUpdateRead(Request $request)
    {   
        if($request->has('id')){
           $seriesUpdate=SeriesUpdate::where('user_id',\Auth::user()->id)->where('id',$request->get('id'))->first();
            if($seriesUpdate){
                $seriesUpdate->is_seen=1;
                $seriesUpdate->save();
            }
        }
        return response()->json([
                'success' => true
            ]); 
    }
  
    
    
    public function settings()
    {
        return view('profile.settings');
    }

    public function upload_avatar(Request $request){
        
       $id = Auth::user()->id;
           
        $validator = Validator::make($request->all(), [
        'photo'       => 'required|mimes:jpg,gif,png,jpe,jpeg',
            ]);
                   
           if ($validator->fails()) {
                return response()->json([
                        'success' => false,
                        'errors' => $validator->getMessageBag()->toArray(),
                    ]);
            }
        
        // PATHS
        $path    = 'storage/'; 
        $imgOld   = $path.Auth::user()->avatar;
        
         //<--- HASFILE PHOTO
        if( $request->hasFile('photo') )    {
            
            $image      = $request->file('photo');
            $fileName   = Auth::user()->slug . '.' . $image->getClientOriginalExtension();
            $avatar=    'users/'.$fileName;
            $image->storeAs('public/users/', $fileName);
                
            // Update Database
            User::where( 'id', Auth::user()->id )->update( array( 'avatar' => $avatar ) );
            
            return response()->json([
                    'success' => true,
                    'avatar' => url($path.$avatar).'?q='.rand(),
                ]);
                    
        }//<--- HASFILE PHOTO       
    }//<--- End Method Avatar
    
    public function passwordUpdate(Request $request)
    {
        $input = $request->all();
        $id = Auth::user()->id;
       
        $validator = Validator::make($input, [
            'old_password' => 'required|min:6',
            'password'     => 'required|confirmed|min:6',  
        ]);
       
        if($validator->fails()){
            if($request->ajax())
                return response()->json([
                    'success' => false,
                    'errors' => $validator->getMessageBag()->toArray(),
                ]);
            return redirect()->back()->withErrors($validator);
        }
       
       if (!\Hash::check($input['old_password'], Auth::user()->password) ) {
            return redirect()->back()->with('error','Incorrect old password.');
        }
           
       $user = User::find($id);
       $user->password  = \Hash::make($input[ "password"] );
       $user->save();
       
       
       return redirect()->back()->with('message','Password changed successfully.');
    }

    public function earnings()
    {
        $mangaAdRevenue=UserAdRevenue::select('manga_id', DB::raw('year(date) as year,month(date) as month,sum(ad_clicks) as total_ad_clicks,sum(revenue) as total_revenue'))
                 ->groupBy(DB::raw('year(date)'),DB::raw('month(date)'),'manga_id')->where(DB::raw('year(date)'),date('Y'));
        
       
        if(!isAdmin()){
            $mangaAdRevenue=$mangaAdRevenue->where('user_id',Auth::user()->id);
        }
        $currentMonth=(new UserAdRevenue)->monthlyStat()->where(DB::raw('month(date)'),(int)date('m'))->first();
        $mangaAdRevenue=$mangaAdRevenue->paginate(20);
        $ad_revenue=[];
       
        for($i=1;$i<=date('m');$i++)
        {
            $monthRevenue=(new UserAdRevenue)->monthlyStat()->where(DB::raw('month(date)'),$i)->first();
            if(!$monthRevenue){
                $monthRevenue['year']=date('Y');
                $monthRevenue['month']=$i;
                $monthRevenue['total_ad_clicks']=0;
                $monthRevenue['total_revenue']=0;
            }
            else
            {
                $monthRevenue=$monthRevenue->toArray();
            }
            $ad_revenue[]=$monthRevenue;
        }
        return view('earnings.index',compact('mangaAdRevenue','currentMonth','ad_revenue'));
    }


}
