<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use Config;
use App\Helpers\SlugHelper;
use App\User;
use App\Models\Artist;
use App\Models\FollowManga;
use App\Models\Manga;
use App\Models\MangaRating;
use App\Models\SeriesUpdate;

class PublisherController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function listArtists()
    {
        if(!isPublisher() && !isAdmin())
        {
            return redirect()->back()->with('error','Oops! you are not authorize to current action.');
        }
        $artists=Artist::latest()->where('user_id',\Auth::user()->id)->paginate(12);

        return view('publisher.artist_index',compact('artists'));
    }

    /**
     * Add Artist View.
     *
     * @return \Illuminate\Http\Response
     */
    public function addArtist($slug=null)
    {
        if(!isPublisher() && !isAdmin())
        {
            return redirect()->back()->with('error','Oops! you are not authorize to current action.');
        }       
      
        $artist=Artist::where('slug',$slug)->first();
        if(!$artist && $slug)
             return redirect()->back()->withInput()->with('error','Sorry! Selected Artist does not exist.');
        if($slug){
            if((!isPublisher() || $artist->user_id!=Auth::user()->id) && !isAdmin())
            {
                return redirect()->back()->with('error','Oops! you are not authorize to current action.');
            }  
        }     
      
        return view('publisher.artist_add',compact('artist'));
    }


    //update profile

    public function saveArtist(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'name' => 'required|string|max:191',
            'email' => 'nullable|email|max:191',
            'dob'=>'nullable|date',
        ]);
        if($validator->fails()){
            return redirect()->back()->withInput()->with('error','Kindly see input errors.');
        }
        if($request->has('id')){
            $artist=Artist::where('id',$request->get('id'))->first();
            if(!$artist){
                return redirect()->back()->withInput()->with('error','Sorry! Selected Artist does not exist.');
            }
       }
        else{
            $artist=new Artist;
            $artist->user_id=Auth::user()->id;
        }
        if($request->hasFile('avatar')){
            $image      = $request->file('avatar');
            $fileName   = time() . '.' . $image->getClientOriginalExtension();
                
            $image->storeAs('public/artists/', $fileName);
            $artist->avatar='artists/'.$fileName;
        }
        $artist->email=$request->get('email');
        $artist->name=$request->get('name');
        $artist->gender=$request->get('gender');
        $artist->bio=$request->get('bio');
        $artist->dob=date('Y-m-d',strtotime($request->get('dob')));
        $artist->save();
        return redirect()->back()->with('message','Artist Profile update successfully.');

    }

    public function deleteArtist($slug, Request $request)
    {
        $artist=Artist::where('slug',$slug)->first();
        if(!$artist)
             return redirect()->back()->withInput()->with('error','Sorry! Selected Artist does not exist.');
        if((!isPublisher() || $artist->user_id!=Auth::user()->id) && !isAdmin())
        {
            return redirect()->back()->with('error','Oops! you are not authorize to current action.');
        }       
      
        $artist->delete();
        if($request->ajax()){
            return response()->json([
                'success' => true
            ]); 
        }
        return redirect()->back()->with('message','Artist deleted successfully.');
    }



}
