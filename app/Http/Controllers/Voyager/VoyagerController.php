<?php

namespace App\Http\Controllers\Voyager;

use TCG\Voyager\Http\Controllers\VoyagerController as BaseVoyagerController;
use TCG\Voyager\Facades\Voyager;
use Illuminate\Http\Request;
use TCG\Voyager\Events\BreadDataUpdated;
use Auth;
use App\Helpers\SlugHelper;
use App\Models\Manga;

class VoyagerController extends BaseVoyagerController
{
    //

    public function profile()
    {
        return Voyager::view('voyager::profile.profile');
    }



    public function profileEdit()
    {
        return Voyager::view('voyager::profile.edit');
    }


        // POST BR(E)AD
    public function profileUpdate(Request $request)
    {
        $slug = 'users';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
       
        // Compatibility with Model binding.
        $id =Auth::user()->id;

        $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->ajax()) {
            $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

            event(new BreadDataUpdated($dataType, $data));

            return redirect()
                ->route("voyager.profile")
                ->with([
                    'message'    => __('voyager::generic.successfully_updated')." Profile",
                    'alert-type' => 'success',
                ]);
        }
    }

    public function mangasPublish(Request $request)
    {
        foreach (explode(',',$request->get('ids')) as $key => $value) {
            $manga=Manga::where('id',$value)->first();
            if($manga){
                Manga::where('id',$value)->update(['status'=>"Published",
                'status_change'=>date('Y-m-d H:i:s')]);
            }
        }
        if($request->ajax()){
            return response()->json([
                'success' => true
            ]); 
        }
        return redirect()->back()->with(['message'=>'Manga published successfully.',
                'alert-type' => 'success',
                ]);
    }

    public function mangaPublish(Request $request)
    {
        $manga=Manga::where('id',$request->get('manga_id'))->first();
        if(!$manga)
             return redirect()->back()->withInput()->with(['message'=>'Sorry! Selected Manga does not exist.',
                'alert-type' => 'error',
                ]);
        Manga::where('id',$request->get('manga_id'))->update(['status'=>"Published",
        'status_change'=>date('Y-m-d H:i:s')]);
        // $manga->status="Published";
        // $manga->status_change=date('Y-m-d H:i:s');
        // $manga->save();
        if($request->ajax()){
            return response()->json([
                'success' => true
            ]); 
        }
        return redirect()->back()->with(['message'=>'Manga published successfully.',
                'alert-type' => 'success',
                ]);
    }

    public function mangaReject(Request $request)
    {
        $manga=Manga::where('id',$request->get('manga_id'))->first();
        if(!$manga)
             return redirect()->back()->withInput()->with(['message'=>'Sorry! Selected Manga does not exist.',
                'alert-type' => 'error',
                ]);
        Manga::where('id',$request->get('manga_id'))->update(['status'=>"Rejected",
        'status_change'=>date('Y-m-d H:i:s')]);
        // $manga->status="Rejected";
        // $manga->status_change=date('Y-m-d H:i:s');
        // $manga->save();
        if($request->ajax()){
            return response()->json([
                'success' => true
            ]); 
        }
        return redirect()->back()->with(['message'=>'Manga rejected successfully.',
                'alert-type' => 'success',
                ]);
    }


    public function mangasFeatured(Request $request)
    {
        foreach (explode(',',$request->get('ids')) as $key => $value) {
            $manga=Manga::where('id',$value)->first();
            if($manga){
                Manga::where('id',$value)->update(['is_featured'=>1]);
            }
        }
        if($request->ajax()){
            return response()->json([
                'success' => true
            ]); 
        }
        return redirect()->back()->with(['message'=>'Manga mark featured successfully.',
                'alert-type' => 'success',
                ]);
    }

    public function mangaFeatured(Request $request)
    {
        $manga=Manga::where('id',$request->get('manga_id'))->first();
        if(!$manga)
             return redirect()->back()->withInput()->with(['message'=>'Sorry! Selected Manga does not exist.',
                'alert-type' => 'error',
                ]);
        Manga::where('id',$request->get('manga_id'))->update(['is_featured'=>1]);
       if($request->ajax()){
            return response()->json([
                'success' => true
            ]); 
        }
        return redirect()->back()->with(['message'=>'Manga mark featured successfully.',
                'alert-type' => 'success',
                ]);
    }

    public function mangaUnFeatured(Request $request)
    {
        $manga=Manga::where('id',$request->get('manga_id'))->first();
        if(!$manga)
             return redirect()->back()->withInput()->with(['message'=>'Sorry! Selected Manga does not exist.',
                'alert-type' => 'error',
                ]);
        Manga::where('id',$request->get('manga_id'))->update(['is_featured'=>0]);
        if($request->ajax()){
            return response()->json([
                'success' => true
            ]); 
        }
        return redirect()->back()->with(['message'=>'Manga remove from featured successfully.',
                'alert-type' => 'success',
                ]);
    }
}
