<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Post;
use App\Models\Manga;
use App\Models\Page;
use App\Traits\Blocks;
use Illuminate\Support\Facades\View;
use App\Models\FollowManga;
use App\Models\FollowArtist;
use App\Models\Artist;
use Analytics;
use Spatie\Analytics\Period;
use Carbon\Carbon;
use DB;

class HomeController extends Controller
{
    use Blocks;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = Page::where('slug', '=','home')->firstOrFail();
        if($page){
            $blocks = $page->blocks()
                ->where('is_hidden', '=', '0')
                ->orderBy('order', 'asc')
                ->get()
                ->map(function ($block) {
                    return (object)[
                        'id' => $block->id,
                        'page_id' => $block->page_id,
                        'updated_at' => $block->updated_at,
                        'cache_ttl' => $block->cache_ttl,
                        'template' => $block->template()->template,
                        'data' => $block->cachedData,
                        'path' => $block->path,
                        'type' => $block->type,
                    ];
                });

            // Override standard body content, with page block content
            $page['block_content'] = $this->prepareEachBlock($blocks);
            
            // Check that the page Layout and its View exists
            if (empty($page->layout)) {
                $page->layout = 'index';
            }
            if (!View::exists("pages.{$page->layout}")) {
                $page->layout = 'index';
            }

            // Return the full page
            return view('pages.'.$page->layout, [
                'page' => $page,
                'layout' => $page->layout,
            ]);
        }
        return view('home.index');
    }

    public function latestChapters()
    {
        //$mangas=\App\Models\Manga::where('status','Published')->has('author')->has('pages')->has('chapters')->select('mangas.*', \DB::raw('(SELECT max(created_at) FROM manga_chapters WHERE manga_chapters.manga_id = mangas.id and manga_chapters.deleted_at is null and manga_chapters.id in (Select manga_pages.manga_chapter_id from manga_pages where manga_pages.deleted_at is null and manga_pages.manga_id=mangas.id) ) as chapter_created_at'))->orderBy('chapter_created_at','desc')->paginate(12);
        $subquery = '(SELECT MAX(mp.created_at) as created, mp.manga_id FROM manga_pages as mp INNER JOIN manga_chapters as mc ON mp.manga_chapter_id = mc.id AND mc.deleted_at is null Group By mp.manga_id ) as ch_date'; 
        $mangas= DB::table('mangas as ma')
               ->leftJoin('users as u', function($join){
                 $join->on('u.id', '=', 'ma.user_id')
                 ->where('u.status', '=', 'Verified');
               })
               ->leftjoin(DB::raw($subquery), 'ch_date.manga_id', 'ma.id')
               ->join('manga_pages as mp', function($join){
                 $join->on('mp.manga_id', '=', 'ma.id')
                 ->where('ma.status', '=', 'Published')
                 ->whereNull('ma.deleted_at');
               })
               ->select('ma.id as manga_id',DB::raw('max(ma.title) as title'), DB::raw('max(ma.cover) as cover'), DB::raw('max(ma.slug) as slug'),DB::raw('MAX(mp.created_at) as created'))
               ->groupBy('ma.id')
               ->orderBy('created', 'DESC')
               ->paginate(12);

        // SELECT ma.id as manga_id, mp.id as page_id, ma.title, ma.`cover`, MAX(mp.created_at) as created FROM `mangas` ma left join users u on u.id = ma.user_id AND u.status = 'Verified' LEFTJOIN ( SELECT MAX(mp.created_at) as created, mp.id as mangaID, mp.manga_id FROM manga_pages mp INNER JOIN manga_chapters as mc ON mp.manga_chapter_id = mc.id AND mc.deleted_at is null Group By mp.manga_id ) abc ON abc.manga_id = ma.id INNER JOIN manga_pages mp on mp.manga_id = ma.id WHERE ma.`status` = 'Published' AND ma.`deleted_at` is null GROUP By ma.id Order By created DESC

        return view('manga.latestchapters',compact('mangas'));
    }


     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function artists()
    {
        $artistslist=Artist::latest()->has('publisher')->has('mangas')->get();
        $artists=User::latest()->has('mangas')->where('status','Verified')->where('is_artist','1')->where('is_publisher','0')->where('role_id','2')->get();


        $artists = paginate(collect($artistslist)->merge($artists)->sortByDesc('created_at'), 12); //Filter the page var
        //->paginate(12);
        return view('artist.index',compact('artists'));
    }

    public function artistProfile($slug)
    {
        $artist=User::where('slug',$slug)->first();
        if(!$artist)
            $artist=Artist::where('slug',$slug)->first();
        else if($artist->status!='Verified')
            return redirect()->back()->with('error','Artist is not exists.');
          
        return view('artist.profile',compact('artist'));
    }

    public function artistDetail($slug)
    {
        $artist=User::where('slug',$slug)->first();
        if(!$artist){
            $artist=Artist::where('slug',$slug)->first();
            $mangas=Manga::where('artist_id',$artist->id)->where('status','Published')->has('pages')->latest()->paginate(10);
        }
        else if($artist->status!='Verified')
            return redirect()->back()->with('error','Artist is not exists.');       
        else
            $mangas=Manga::where('user_id',$artist->id)->where('status','Published')->has('pages')->latest()->paginate(10);
        return view('artist.detail',compact(['artist','mangas']));
    }


    public function blogs()
    {
        $blogs=Post::latest()->paginate(6);
        return view('blog.index',compact('blogs'));
    }

    public function blogDetail($slug)
    {
        $blog=Post::where('slug',$slug)->first();
        return view('blog.detail',compact('blog'));
    }
   
    public function contact(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'name' => 'required|max:40',
            'phone' => 'required|max:25',
            'category' => 'required',
            'email' => 'required|email'
        ]);
        if($validator->fails()){
            if($request->ajax())
                return response()->json([
                    'success' => false,
                    'errors' => $validator->getMessageBag()->toArray(),
                ]);
            return redirect()->back()->withInput()->with('error','Kindly see input errors.');
        }
        $contact= new \App\Models\Contact();
        $contact->name = $request->get('name');
        $contact->email =$request->get('email');
        $contact->phone =$request->get('phone');
        $contact->category = $request->get('category');
        $contact->message =$request->get('message');
        $contact->ip = \Request::getClientIp();
        $contact->agent = \Request::header('User-Agent');
        $contact->save();
        \Mail::send('emails.contact', $contact, function ($message) {
            $message->subject('Contact Query')->to(\Config::get('mail.from.address'));
        });
        if($request->ajax())
            return response()->json([
                'success' => true,
                'message' => 'Your query sent sccuessfully. we will contact you soon.',
            ]);
        return redirect()->back()->with('message','Your query sent sccuessfully. we will contact you soon.');
    }

    public function mailTest()
    {
        \Mail::send('welcome', array(), function ($message){
            //
            $message->subject('Test Mail By Manga');
            $message->to('neetu.rani@imarkinfotech.com');
        });
    }

    public function homeContent()
    {
        return response()->json([
                'success' => true,
                'latestChapter'=>view('widgets.featuredComics')->render(),
                'mostPopular'=>view('widgets.mostPopular')->render(),
                //'recentMangas'=>view('widgets.recentMangas')->render()
            ]);  
    }

    
}
