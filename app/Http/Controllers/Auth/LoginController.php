<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Contracts\Auth\Guard;
use Validator;
use App\Models\Page;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
        $this->middleware('guest', ['except' => 'logout']);
    }


    public function index() {
        if(!\Auth::check() ){
            if(Page::where('slug',substr(parse_url(\URL::previous())['path'],1))->count()>0)
                return redirect()->back()->with('login','1');
            else
                 return redirect('/')->with('login','1');
            //return view('auth.login');
        } else {
            return redirect('/');
        }

     }

    public function login(Request $request) {
        // get our login input
        $login = $request->input('email');

        // check login field
        $login_type = filter_var( $login, FILTER_VALIDATE_EMAIL ) ? 'email' : 'username';

        // merge our login field into the request with either email or username as key
        $request->merge([ $login_type => $login ]);

        // let's validate and set our credentials
        if ( $login_type == 'email' ) {

            $this->validate($request, [
                'email'    => 'required|email',
                'password' => 'required',
            ]);

            $credentials = $request->only( 'email', 'password' );

        } else {

            $this->validate($request, [
                'email' => 'required',
                'password' => 'required',
            ]);

            $credentials = $request->only( 'username', 'password' );

        }

          
        if( $this->auth->attempt($credentials, $request->has('remember')) ){
                
            if($this->auth->User()->status != 'Blocked') {
                   
                    return response()->json(['success' => 'true','reload'=>'true',
                        'message' => 'Logged in successfully.']);
                     
                    //return redirect()->intended('/');
                    
            } else {
                
                $this->auth->logout();
                return response()->json([ 'success' => false,
                    'errors' => 'Sorry! Your account is blocked. Please contact administrator.']);
            } 
            
        }

         return response()->json([ 'success' => false,
                    'errors' => 'These credentials do not match our records.']);
    }
}
