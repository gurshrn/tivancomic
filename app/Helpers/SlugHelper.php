<?php

namespace App\Helpers;

class SlugHelper
{
    private $entity;
    private $slugAttr;

    public function __construct($entity = \App\User::class, $slugAttr = 'slug')
    {
        $this->entity = $entity;
        $this->slugAttr = $slugAttr;
    }

    /**
     * @param $title
     * @param int $id
     * @return string
     * @throws \Exception
     */
    public function createSlug($title, $id = 0)
    {
        // Normalize the title
        $slug = str_slug($title);
        // Get any that could possibly be related.
        // This cuts the queries down by doing it once.
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        // If we haven't used it before then we are all good.
        if (!$allSlugs->contains($this->slugAttr, $slug)) {
            return $slug;
        }
        // Just append numbers like a savage until we find not used.
        for ($i = 1; $i <= 100; $i++) {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains($this->slugAttr, $newSlug)) {
                return $newSlug;
            }
        }
        throw new \Exception('Can not create a unique slug');
    }

    protected function getRelatedSlugs($slug, $id = 0)
    {
        if(in_array('Illuminate\Database\Eloquent\SoftDeletes', class_uses($this->entity)))
            return call_user_func(array($this->entity, 'select'), $this->slugAttr)->withTrashed()->where($this->slugAttr, 'like', $slug . '%')
            ->where('id', '<>', $id)
            ->get();
        else
            return call_user_func(array($this->entity, 'select'), $this->slugAttr)->where($this->slugAttr, 'like', $slug . '%')
            ->where('id', '<>', $id)
            ->get();
    }
}