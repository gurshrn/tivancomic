<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Helpers\SlugHelper;

class User extends \TCG\Voyager\Models\User implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','is_artist','dob','gender','bio','avatar','slug','is_publisher'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function mangas() {
        return $this->hasMany('App\Models\Manga')->with('chapters','mangaRatings')->withCount('mangaViews')->where('status','Published')->has('pages')->latest();
    }
   
    public function save(array $options = [])
    {
        $slugHelper=new SlugHelper(\App\User::class);
        if(empty($this->id)) {
            $this->slug = $slugHelper->createSlug($this->name,0);
        }
        else
            $this->slug = $slugHelper->createSlug($this->name,$this->id);
            
        parent::save();
    }
}
